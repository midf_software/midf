'''
Matplotlib Grid plotter

plot_data_longitudinal: PLotea la propiedad en un mesh rectangular de todas las capas vs todos los radios en un tiempo especifico. Vista longitudinal

plot_data_axial: Plotea la propiedad en un mesh radial de una capa especifica en un tiempo especifico. Vista axial.
'''

import numpy as np
import matplotlib.pyplot as plt
import csv
import re
import StringIO
import sys
import time
import datetime

my_dpi = 96

def plot_data_longitudinal(fname, title, radiuses, prop_values, vmin, vmax, log=False):
    numl = prop_values.shape[1]
    numr = len(radiuses)
    if log:
        radiuses = np.log10(radiuses)
    # Perparar datos como un meshgrid
    layers = np.linspace(0, numl, numl+1)
    rr, ll = np.meshgrid(radiuses, layers)
    z = prop_values.T
    #-- Plot... ------------------------------------------------
    fig = plt.figure(figsize=(465/my_dpi, 350/my_dpi), dpi=my_dpi)
    ax = fig.add_subplot(111) 

    cax = plt.pcolormesh(rr, ll, z, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
    cb = fig.colorbar(cax)

    # Ticks y texto del plot
    layers_ticks = np.arange(0, numl+1, 1)
    layers_labels = [v for x,v in enumerate(layers_ticks)]
    ax.set_yticks(layers_ticks)
    ax.set_yticklabels(layers_labels)

    ax.set_xticks(radiuses)
    rad_labels = ['%.2f'%(x) for x in radiuses]
    rad_labels[1:-1] = [' '] * (len(rad_labels) - 2) 
    ax.set_xticklabels(rad_labels); #rad labels solo muestra texto para primer y ultimo radio
    plt.ylabel('Layer')
    plt.xlabel('Radius')
    plt.title(title)

    ax.set_xlim([0, max(radiuses)])
    plt.gca().invert_yaxis()
    ax.grid(True, linewidth=1.5)
    plt.savefig(fname, bbox_inches='tight')
    fig.clf()
    plt.close()
   

def plot_data_axial(fname, title, radiuses, prop_values, vmin, vmax, log=False):
    ti = time.time()
    zeniths = radiuses
    if log:
        zeniths = np.log10(zeniths)
    azimuths = np.radians(np.linspace(0, 360, 50))
    r, theta = np.meshgrid(zeniths, azimuths)
    zdata = np.zeros_like(r)
    zdata[:, ...] = prop_values
    values = zdata
    #-- Plot... ------------------------------------------------
    fig = plt.figure(figsize=(465/my_dpi, 350/my_dpi), dpi=my_dpi)
    ax = fig.add_subplot(111, polar=True) 
    #cax = ax.contourf(theta, r, values, cmap = plt.get_cmap('rainbow'))
    cax = plt.pcolormesh(theta, r, values, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
    cb = fig.colorbar(cax) 
    #ax.set_rscale('log')
    ax.set_title(title)
    ax.set_yticks(zeniths);
    ax.set_yticklabels([]);
    ax.set_xticklabels([]);
    ax.set_ylim([min(zeniths), max(zeniths)])
    ax.grid(True)
    print "ET grafico", time.time() - ti
    #plt.show()
    plt.savefig(fname, bbox_inches='tight')
    fig.clf()
    plt.close()

def prepare_data(filename):
    f = open(filename, 'r')
    fdata  = f.read()
    fdata = re.sub( ' +', ' ', fdata ).strip() + ' '
    cdata = csv.reader(StringIO.StringIO(fdata), delimiter=' ');
    j = 0
    radiuses, values = [], []
    tmp = 0
    time_layers_data = []
    time_data = []
    vmin, vmax = 100000, 0

    for i, row in enumerate(cdata):
        if i > 1:
            if len(row) <= 3:
                # Time starts
                j += 1
                time_data.append([float(x) for x in row if x != ''][0])
                if j > 1:
                    # data for this time is complete, start saving
                    value_data = np.array(values)
                    value_min, value_max = value_data.min(), value_data.max()

                    vmin = vmin if vmin < value_min else value_min
                    vmax = vmax if vmax > value_max else value_max
                    
                    time_layers_data.append(value_data)
                radiuses, values = [], []
            else:
                # Data value
                rdata = row[1:-1]
                radiuses.append(float(rdata[0]))
                values.append([float(x) for x in rdata[1:]])
    #Append last one
    value_data = np.array(values)
    value_min, value_max = value_data.min(), value_data.max()
    vmin = vmin if vmin < value_min else value_min
    vmax = vmax if vmax > value_max else value_max
    time_layers_data.append(value_data)
    return time_data, time_layers_data, radiuses, vmin, vmax


def get_gridplot( layer_num, time_num, prop, sc_id, base_tstamp):
    #root_dir_data = './res/solver/Results/'
    root_dir_data = './Results/'
    output_dir = './res/props/'
    prop_dirs = {'gas_pressure': root_dir_data + '%s_Gas Pressure.txt'%(sc_id), 'oil_pressure': root_dir_data + '%s_Oil Pressure.txt'%(sc_id), 'water_pressure': root_dir_data + '%s_Water Pressure.txt'%(sc_id), 'gas_saturation': root_dir_data + '%s_Gas Saturation.txt'%(sc_id), 'oil_saturation': root_dir_data + '%s_Oil Saturation.txt'%(sc_id), 'water_saturation': root_dir_data + '%s_Water Saturation.txt'%(sc_id)}
    data_fname = prop_dirs[prop]


    time_data, time_layers_data,radiuses, vmin, vmax = prepare_data(data_fname)
    
    
    if layer_num != ALL_LAYERS:
        # For one specific layer, specific time
        value_data = time_layers_data[time_num-1]
        l = layer_num - 1
        i = time_num - 1
        layer_values = value_data[:, l]
        fname = '%s/render.png'%(output_dir)
        days = int(time_data[i])
        time_str = datetime.datetime.fromtimestamp(int(base_tstamp/1000.) + days * 86400).strftime('%d/%m/%Y')
        #time_str = days        
        title = 'Time: %s Layer: %s'%(time_str, l+1)
        print title
        print fname
        plot_data_axial(fname, title, radiuses, layer_values, vmin=vmin, vmax=vmax, log=False)
    else:
        # All layers for specific time
        i = time_num - 1
        value_data = time_layers_data[time_num-1]
        layers_values = value_data
        days = int(time_data[i])
        time_str = datetime.datetime.fromtimestamp(int(base_tstamp/1000.) + days * 86400).strftime('%d/%m/%Y')
        fname = '%s/render.png'%(output_dir)
        title = 'Time: %s '%(time_str)
        plot_data_longitudinal(fname, title, radiuses, layers_values, vmin, vmax)

    # For all data
    '''
    for i,  value_data in enumerate(time_layers_data):
        
        for l in range(value_data.shape[1]):
            layer_values = value_data[:, l]
            fname = '%s/c%s/%s.png'%(output_dir, l+1, i+1)
            title = 'Time: %s (days) Layer: %s'%(int(time_data[i]), l+1)
            print title
            plot_data(fname, title, radiuses, layer_values, vmin=vmin, vmax=vmax, log=False)
            print fname
    '''

if __name__ == '__main__':
    ALL_LAYERS = -1
    prop = sys.argv[1] # Keys gas_pressure, oil_pressure, water_pressure
    layer_num = int(sys.argv[2])
    time_num = int(sys.argv[3])
    sc_id = int(sys.argv[4])
    base_tstamp = long(sys.argv[5])

    print "Property", prop, " Layer ", layer_num, " Time ", time_num, " Scenario id", sc_id
    get_gridplot(layer_num, time_num, prop, sc_id, base_tstamp)
