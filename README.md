
MIDF : Modelo integrado de daño de formacion.

Configuracion de desarrollo
===========================

- Agregar la carpeta lib al JAVAPATH o agregar los .JAR de ese directorio como librerias externas.

- Configurar base de datos en MySQL. Nombre base de datos: "midf", login: "root", "root". (ver application.controllers.DBClient)

- Agregar siguiente parametro de ejecucion a la maquina virtual (VM arguments):

 "-javaagent:/ruta/activejdbc-instrumentation/lib/activejdbc-instrumentation-1.4.10-20140719.225608-42.jar"

Se debe establecer la ruta correcta al JAR. En Eclipse es en run -> Run configurations -> Arguments -> VM Arguments.

- Si la base de datos esta vacia se pueden crear las tablas ejecutando el script docs/dev/database/create_default_schema.sql en MySQL.

- Adicionalmente se pueden insertar datos de ejemplo (Proyecto  + escenario simple) ejecutando el script  docs/dev/database/insert_samples.sql en MySQL.


Contacto
========
midf.software@gmail.com
