INSERT INTO `midf`.`projects` (`name`, `ref`, `description`, `date`) VALUES ('Test project', 'ref', 'description', '2014-04-18');

INSERT INTO `midf`.`scenarios` (`project_id`, `name`, `ref`, `type`, `date`, `description`) VALUES ('1', 'Test scenario', 'scn1', 'Basic Simulation', '2014-04-18', 'Scenario description');

INSERT INTO `midf`.`simulations` (`scenario_id`) VALUES ('1');

INSERT INTO `midf`.`base_datasets` (`scenario_id`) VALUES ('1');

INSERT INTO `midf`.`reservoir_properties` (`base_dataset_id`, `formation_thickness`, `external_radius`, `wellbore_radius`, `formation_depthtop`, `rock_density`) VALUES ('1', '200', '2000', '0.35', '9000', '10');


INSERT INTO `midf`.`grids` (`base_dataset_id`, `radial_numblocks`, `angular_numblocks`, `vertical_numblocks`) VALUES ('1', '10', '1', '4');


INSERT INTO `midf`.`geological_units` (`grid_id`, `name`, `thickness`, `num_layers`) VALUES ('1', 'UG1', '100', '2');
INSERT INTO `midf`.`geological_units` (`grid_id`, `name`, `thickness`, `num_layers`) VALUES ('1', 'UG2', '50', '1');
INSERT INTO `midf`.`geological_units` (`grid_id`, `name`, `thickness`, `num_layers`) VALUES ('1', 'UG3', '50', '1');


INSERT INTO `midf`.`layers` (`geological_unit_id`, `grid_id`, `layernum`, `thickness`) VALUES ('1', '1', '1', '50');
INSERT INTO `midf`.`layers` (`geological_unit_id`, `grid_id`, `layernum`, `thickness`) VALUES ('1', '1', '2', '50');
INSERT INTO `midf`.`layers` (`geological_unit_id`, `grid_id`, `layernum`, `thickness`) VALUES ('2', '1', '3', '50');
INSERT INTO `midf`.`layers` (`geological_unit_id`, `grid_id`, `layernum`, `thickness`) VALUES ('3', '1', '4', '50');


INSERT INTO `midf`.`formation_conditions` (`rule`, `layer_id`, `porosity`, `permeability_x`, `permeability_y`, `permeability_z`) VALUES ('*', '1', '0.04', '10', '10', '10');
INSERT INTO `midf`.`formation_conditions` (`rule`, `layer_id`, `porosity`, `permeability_x`, `permeability_y`, `permeability_z`) VALUES ('*', '2', '0.04', '20', '20', '20');
INSERT INTO `midf`.`formation_conditions` (`rule`, `layer_id`, `porosity`, `permeability_x`, `permeability_y`, `permeability_z`) VALUES ('*', '3', '0.05', '15', '15', '15');
INSERT INTO `midf`.`formation_conditions` (`rule`, `layer_id`, `porosity`, `permeability_x`, `permeability_y`, `permeability_z`) VALUES ('*', '4', '0.06', '10', '10', '10');


INSERT INTO `midf`.`initial_conditions` (`rule`, `layer_id`, `reservoir_pressure`, `gas_saturation`, `water_saturation`) VALUES ('*', '1', '2000', '0.5', '0.2');
INSERT INTO `midf`.`initial_conditions` (`rule`, `layer_id`, `reservoir_pressure`, `gas_saturation`, `water_saturation`) VALUES ('*', '2', '3000', '0.4', '0.4');
INSERT INTO `midf`.`initial_conditions` (`rule`, `layer_id`, `reservoir_pressure`, `gas_saturation`, `water_saturation`) VALUES ('*', '3', '3000', '0.3', '0.4');
INSERT INTO `midf`.`initial_conditions` (`rule`, `layer_id`, `reservoir_pressure`, `gas_saturation`, `water_saturation`) VALUES ('*', '4', '3000', '0.6', '0.1');

INSERT INTO `midf`.`simulation_conditions` (`simulation_id`, `max_timestep`, `min_timestep`, `total_time`, `max_pressuredelta`, `max_saturationdelta`, `max_rsdelta`, `dt_accelfactor`, `relative_error`, `epsilon`, `num_iterations`) VALUES ('1', '1', '0.000001', '365', '20', '0.1', '10', '2', '0.0000001', '0.0000001', '20');

INSERT INTO `midf`.`perforation_conditions` (`layer_id`, `perforated`) VALUES ('1', 'YES');
INSERT INTO `midf`.`perforation_conditions` (`layer_id`, `perforated`) VALUES ('2', 'YES');
INSERT INTO `midf`.`perforation_conditions` (`layer_id`, `perforated`) VALUES ('3', 'YES');
INSERT INTO `midf`.`perforation_conditions` (`layer_id`, `perforated`) VALUES ('4', 'YES');
