-- MySQL dump 10.14  Distrib 5.5.39-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: midf
-- ------------------------------------------------------
-- Server version	5.5.39-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `midf`
--

/*!40000 DROP DATABASE IF EXISTS `midf`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `midf` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `midf`;

--
-- Table structure for table `base_datasets`
--

DROP TABLE IF EXISTS `base_datasets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_datasets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenario_id` int(11) DEFAULT NULL,
  `scenario_result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_base_datasets_scenarios1_idx` (`scenario_id`),
  KEY `fk_base_datasets_scenario_results1_idx` (`scenario_result_id`),
  CONSTRAINT `fk_base_datasets_scenarios1` FOREIGN KEY (`scenario_id`) REFERENCES `scenarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_base_datasets_scenario_results1` FOREIGN KEY (`scenario_result_id`) REFERENCES `scenario_results` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_datasets`
--

LOCK TABLES `base_datasets` WRITE;
/*!40000 ALTER TABLE `base_datasets` DISABLE KEYS */;
INSERT INTO `base_datasets` VALUES (1,1,NULL),(2,2,NULL),(3,3,NULL),(4,4,NULL),(5,5,NULL),(7,7,NULL),(8,9,NULL),(9,10,NULL),(10,11,NULL),(11,12,NULL);
/*!40000 ALTER TABLE `base_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservoir_id` int(11) DEFAULT NULL,
  `name` varchar(145) DEFAULT NULL,
  `poly_points` varchar(700) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_field_reservoir_idx` (`reservoir_id`),
  CONSTRAINT `fk_field_reservoir` FOREIGN KEY (`reservoir_id`) REFERENCES `reservoirs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (1,NULL,'FIELD-R719','4.209786,-71.807327;4.334408,-71.777115;4.315237,-71.466751;4.164588,-71.667252'),(2,NULL,'FIELD-R722','4.947622,-72.168503;4.925731,-72.135544;4.908265,-71.948090;5.032444,-71.948776;5.069378,-72.069626');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formation_conditions`
--

DROP TABLE IF EXISTS `formation_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formation_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` varchar(100) DEFAULT NULL,
  `layer_id` int(11) NOT NULL,
  `porosity` double DEFAULT NULL,
  `permeability_x` double DEFAULT NULL,
  `permeability_y` double DEFAULT NULL,
  `permeability_z` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_formation_conditions_layers1_idx` (`layer_id`),
  CONSTRAINT `fk_formation_conditions_layers1` FOREIGN KEY (`layer_id`) REFERENCES `layers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formation_conditions`
--

LOCK TABLES `formation_conditions` WRITE;
/*!40000 ALTER TABLE `formation_conditions` DISABLE KEYS */;
INSERT INTO `formation_conditions` VALUES (1,'*',1,0.04,10,10,10),(2,'*',2,0.04,10,10,10),(3,'*',3,0.04,10,10,10),(4,'*',4,0.04,10,10,10),(5,'*',5,0.04,10,10,10),(6,'*',6,0.04,10,10,10),(7,'*',7,0.04,10,10,10),(8,'*',8,0.04,10,10,0),(9,'*',9,0.04,10,10,10),(10,'*',10,0.04,10,10,10),(11,'*',11,0.04,10,10,10),(12,'*',12,0.04,10,10,10),(13,'*',13,0.04,10,10,0),(14,'*',14,0.04,10,10,10),(15,'*',15,0.04,10,10,10),(16,'*',16,0.04,10,10,10);
/*!40000 ALTER TABLE `formation_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geological_units`
--

DROP TABLE IF EXISTS `geological_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geological_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grid_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `thickness` double DEFAULT NULL,
  `num_layers` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_geological_units_grid1_idx` (`grid_id`),
  CONSTRAINT `fk_geological_units_grid1` FOREIGN KEY (`grid_id`) REFERENCES `grids` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geological_units`
--

LOCK TABLES `geological_units` WRITE;
/*!40000 ALTER TABLE `geological_units` DISABLE KEYS */;
INSERT INTO `geological_units` VALUES (1,1,'UG1',75,1),(2,1,'UG2',75,1),(3,1,'UG3',75,1),(4,2,'Geological unit',300,4),(5,3,'Geological unit',300,4),(6,1,'UG4',75,1),(7,4,'Geological unit',200,2),(8,4,'Geological unit 2',100,2);
/*!40000 ALTER TABLE `geological_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grids`
--

DROP TABLE IF EXISTS `grids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `radial_numblocks` int(11) DEFAULT NULL,
  `angular_numblocks` int(11) DEFAULT NULL,
  `vertical_numblocks` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grid_base_data1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_grid_base_data1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grids`
--

LOCK TABLES `grids` WRITE;
/*!40000 ALTER TABLE `grids` DISABLE KEYS */;
INSERT INTO `grids` VALUES (1,1,10,1,4),(2,5,10,1,4),(3,8,10,1,4),(4,10,15,1,4);
/*!40000 ALTER TABLE `grids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `initial_conditions`
--

DROP TABLE IF EXISTS `initial_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `initial_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` varchar(45) DEFAULT NULL,
  `layer_id` int(11) NOT NULL,
  `reservoir_pressure` double DEFAULT NULL,
  `gas_saturation` double DEFAULT NULL,
  `water_saturation` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_initial_conditions_layers1_idx` (`layer_id`),
  CONSTRAINT `fk_initial_conditions_layers1` FOREIGN KEY (`layer_id`) REFERENCES `layers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `initial_conditions`
--

LOCK TABLES `initial_conditions` WRITE;
/*!40000 ALTER TABLE `initial_conditions` DISABLE KEYS */;
INSERT INTO `initial_conditions` VALUES (1,'*',1,4000,0.6,0.2),(2,'*',2,4000,0.6,0.4),(3,'*',3,4000,0.6,0.4),(4,'*',4,4000,0.6,0.1),(5,'*',5,4000,0.6,0),(6,'*',6,4000,0.6,0),(7,'*',7,4000,0.6,0),(8,'*',8,4000,0.6,0),(9,'*',9,4000,0.6,0),(10,'*',10,4000,0.6,0),(11,'*',11,4000,0.6,0),(12,'*',12,4000,0.6,0),(13,'*',13,3200,0.6,0),(14,'*',14,3200,0.6,0),(15,'*',15,3000,0.6,0),(16,'*',16,3000,0.6,0);
/*!40000 ALTER TABLE `initial_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipr`
--

DROP TABLE IF EXISTS `ipr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pr` double DEFAULT NULL,
  `pb` double DEFAULT NULL,
  `pwfpf` double DEFAULT NULL,
  `q` double DEFAULT NULL,
  `f2` double DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `skin` double DEFAULT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ipr_scenarios1_idx` (`scenario_id`),
  CONSTRAINT `fk_ipr_scenarios1` FOREIGN KEY (`scenario_id`) REFERENCES `scenarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipr`
--

LOCK TABLES `ipr` WRITE;
/*!40000 ALTER TABLE `ipr` DISABLE KEYS */;
INSERT INTO `ipr` VALUES (1,4000,3000,1500,800,0.363,NULL,NULL,2),(2,4000,3000,1500,800,0.583,NULL,NULL,10),(3,4000,3000,1500,800,0.5,NULL,NULL,12);
/*!40000 ALTER TABLE `ipr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `krs_gas_values`
--

DROP TABLE IF EXISTS `krs_gas_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `krs_gas_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `sg` double DEFAULT NULL,
  `pcog` double DEFAULT NULL,
  `krg` double DEFAULT NULL,
  `kro` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_krs_gas_values_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_krs_gas_values_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `krs_gas_values`
--

LOCK TABLES `krs_gas_values` WRITE;
/*!40000 ALTER TABLE `krs_gas_values` DISABLE KEYS */;
INSERT INTO `krs_gas_values` VALUES (1,1,0,0.81,0,1),(2,1,0.0014,0.91,0,0.9995),(3,1,0.0029,1.03,0.0001,0.996),(4,1,0.0712,1.36,0.0073,0.7189),(5,1,0.1781,1.95,0.0367,0.4972),(6,1,0.3562,3.1,0.1033,0.2527),(7,1,0.5699,5.7,0.2133,0.0588),(8,1,0.6767,9.4,0.2503,0.0065),(9,1,0.7095,9.9,0.2596,0.0003),(10,1,0.8619,10,0.2598,0),(11,5,0,0.81,0,1),(12,5,0.0014,0.91,0,0.9995),(13,5,0.0029,1.03,0.0001,0.996),(14,5,0.0712,1.36,0.0073,0.7189),(15,5,0.1781,1.95,0.0367,0.4972),(16,5,0.3562,3.1,0.1033,0.2527),(17,5,0.5699,5.7,0.2133,0.0588),(18,5,0.6767,9.4,0.2503,0.0065),(19,5,0.7095,9.9,0.2596,0.0003),(20,5,0.8619,10,0.2598,0),(21,8,0,0.81,0,1),(22,8,0.0014,0.91,0,0.9995),(23,8,0.0029,1.03,0.0001,0.996),(24,8,0.0712,1.36,0.0073,0.7189),(25,8,0.1781,1.95,0.0367,0.4972),(26,8,0.3562,3.1,0.1033,0.2527),(27,8,0.5699,5.7,0.2133,0.0588),(28,8,0.6767,9.4,0.2503,0.0065),(29,8,0.7095,9.9,0.2596,0.0003),(30,8,0.8619,10,0.2598,0),(31,10,0,0.81,0,1),(32,10,0.0014,0.91,0,0.9995),(33,10,0.0029,1.03,0.0001,0.996),(34,10,0.0712,1.36,0.0073,0.7189),(35,10,0.1781,1.95,0.0367,0.4972),(36,10,0.3562,3.1,0.1033,0.2527),(37,10,0.5699,5.7,0.2133,0.0588),(38,10,0.6767,9.4,0.2503,0.0065),(39,10,0.7095,9.9,0.2596,0.0003),(40,10,0.8619,10,0.2598,0);
/*!40000 ALTER TABLE `krs_gas_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `krs_water_values`
--

DROP TABLE IF EXISTS `krs_water_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `krs_water_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `sw` double DEFAULT NULL,
  `pcwo` double DEFAULT NULL,
  `krw` double DEFAULT NULL,
  `kro` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_krs_water_values_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_krs_water_values_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `krs_water_values`
--

LOCK TABLES `krs_water_values` WRITE;
/*!40000 ALTER TABLE `krs_water_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `krs_water_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layers`
--

DROP TABLE IF EXISTS `layers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `geological_unit_id` int(11) DEFAULT NULL,
  `grid_id` int(11) NOT NULL,
  `layernum` int(11) DEFAULT NULL,
  `thickness` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_layers_geological_units1_idx` (`geological_unit_id`),
  KEY `fk_layers_grid1_idx` (`grid_id`),
  CONSTRAINT `fk_layers_geological_units1` FOREIGN KEY (`geological_unit_id`) REFERENCES `geological_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_layers_grid1` FOREIGN KEY (`grid_id`) REFERENCES `grids` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layers`
--

LOCK TABLES `layers` WRITE;
/*!40000 ALTER TABLE `layers` DISABLE KEYS */;
INSERT INTO `layers` VALUES (1,1,1,1,75),(2,2,1,2,75),(3,3,1,3,75),(4,6,1,4,75),(5,4,2,1,75),(6,4,2,2,75),(7,4,2,3,75),(8,4,2,4,75),(9,5,3,1,75),(10,5,3,2,75),(11,5,3,3,75),(12,5,3,4,75),(13,7,4,1,100),(14,7,4,2,100),(15,8,4,3,50),(16,8,4,4,50);
/*!40000 ALTER TABLE `layers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_types`
--

DROP TABLE IF EXISTS `module_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_types`
--

LOCK TABLES `module_types` WRITE;
/*!40000 ALTER TABLE `module_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_type_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_module_module_type1_idx` (`module_type_id`),
  CONSTRAINT `fk_module_module_type1` FOREIGN KEY (`module_type_id`) REFERENCES `module_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multiparametric`
--

DROP TABLE IF EXISTS `multiparametric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multiparametric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `well_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_multiparametric_well1_idx` (`well_id`),
  CONSTRAINT `fk_multiparametric_well1` FOREIGN KEY (`well_id`) REFERENCES `wells` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multiparametric`
--

LOCK TABLES `multiparametric` WRITE;
/*!40000 ALTER TABLE `multiparametric` DISABLE KEYS */;
/*!40000 ALTER TABLE `multiparametric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operation_conditions`
--

DROP TABLE IF EXISTS `operation_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` double DEFAULT NULL,
  `qg` double DEFAULT NULL,
  `qo` double DEFAULT NULL,
  `qw` double DEFAULT NULL,
  `bhfp` double DEFAULT NULL,
  `dbhfp` double DEFAULT NULL,
  `base_dataset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operation_conditions_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_operation_conditions_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation_conditions`
--

LOCK TABLES `operation_conditions` WRITE;
/*!40000 ALTER TABLE `operation_conditions` DISABLE KEYS */;
INSERT INTO `operation_conditions` VALUES (1,0,0,0,0,2600,0,1),(2,365,0,0,0,2600,0,1),(3,0,0,0,0,2600,0,5),(4,365,0,0,0,2600,0,5),(5,0,0,0,0,2600,0,8),(6,151,0,0,0,2600,0,8),(7,152,0,0,0,3755,0,8),(8,155,0,0,0,3755,0,8),(9,156,0,0,0,2600,0,8),(10,365,0,0,0,2600,0,8),(11,0,0,0,0,2600,0,10),(12,365,0,0,0,2600,0,10);
/*!40000 ALTER TABLE `operation_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perforation_conditions`
--

DROP TABLE IF EXISTS `perforation_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perforation_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layer_id` int(11) DEFAULT NULL,
  `skin` double DEFAULT NULL,
  `perforated` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_perforation_conditions_layers1_idx` (`layer_id`),
  CONSTRAINT `fk_perforation_conditions_layers1` FOREIGN KEY (`layer_id`) REFERENCES `layers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perforation_conditions`
--

LOCK TABLES `perforation_conditions` WRITE;
/*!40000 ALTER TABLE `perforation_conditions` DISABLE KEYS */;
INSERT INTO `perforation_conditions` VALUES (1,1,NULL,'YES'),(2,2,NULL,'YES'),(3,3,NULL,'YES'),(4,4,NULL,'YES'),(5,5,0,'YES'),(6,6,0,'YES'),(7,7,0,'YES'),(8,8,0,'YES'),(9,9,0,'YES'),(10,10,0,'YES'),(11,11,0,'YES'),(12,12,0,'YES'),(13,13,0,'YES'),(14,14,0,'YES'),(15,15,0,'YES'),(16,16,0,'YES');
/*!40000 ALTER TABLE `perforation_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `production`
--

DROP TABLE IF EXISTS `production`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `well_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_production_well1_idx` (`well_id`),
  CONSTRAINT `fk_production_well1` FOREIGN KEY (`well_id`) REFERENCES `wells` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `production`
--

LOCK TABLES `production` WRITE;
/*!40000 ALTER TABLE `production` DISABLE KEYS */;
/*!40000 ALTER TABLE `production` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `ref` varchar(45) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `well_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_well1_idx` (`well_id`),
  CONSTRAINT `fk_project_well1` FOREIGN KEY (`well_id`) REFERENCES `wells` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'M3-100A - Damage Analysis','ref','Well M3-100A Damage and remediation analysis','28/08/2014',NULL),(2,'M3-100A - Black Oil Simulation',NULL,'Well M3-100A Black oil simulation ','26/11/2014',NULL);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvt_densities`
--

DROP TABLE IF EXISTS `pvt_densities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvt_densities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `gas_density` double DEFAULT NULL,
  `water_density` double DEFAULT NULL,
  `oil_density` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pvt_densities_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_pvt_densities_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvt_densities`
--

LOCK TABLES `pvt_densities` WRITE;
/*!40000 ALTER TABLE `pvt_densities` DISABLE KEYS */;
INSERT INTO `pvt_densities` VALUES (1,1,0.0664348,60.3094,57.49),(2,5,0.0664348,60.3094,57.49),(3,8,0.0664,60.30904,57.49),(4,10,0.0664348,60.30904,57.49);
/*!40000 ALTER TABLE `pvt_densities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvt_values`
--

DROP TABLE IF EXISTS `pvt_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvt_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `ug` double DEFAULT NULL,
  `uo` double DEFAULT NULL,
  `uw` double DEFAULT NULL,
  `bg` double DEFAULT NULL,
  `bo` double DEFAULT NULL,
  `bw` double DEFAULT NULL,
  `rs` double DEFAULT NULL,
  `rv` double DEFAULT NULL,
  `pressure` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pvt_values_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_pvt_values_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvt_values`
--

LOCK TABLES `pvt_values` WRITE;
/*!40000 ALTER TABLE `pvt_values` DISABLE KEYS */;
INSERT INTO `pvt_values` VALUES (1,1,0.01516,0.53092,0.30154514,0.00307,1.23892,1.03844572,299.98,0,1000),(2,1,0.01568,0.49541,0.30215546,0.002531,1.27292,1.03774735,363.11,0,1200),(3,1,0.01624,0.45881,0.30276826,0.002148,1.30714,1.03704992,427.64,0,1400),(4,1,0.01687,0.42583,0.30338355,0.001865,1.34182,1.03635343,493.91,0,1600),(5,1,0.01756,0.39728,0.30400135,0.001647,1.37718,1.03565788,562.24,0,1800),(6,1,0.01832,0.37213,0.30462167,0.001476,1.41338,1.03496326,632.89,0,2000),(7,1,0.01917,0.34951,0.30524452,0.001338,1.45059,1.03426956,706.14,0,2200),(8,1,0.02011,0.32892,0.30586993,0.001226,1.48899,1.0335768,782.31,0,2400),(9,1,0.02114,0.31002,0.3064979,0.001133,1.52875,1.03288497,861.72,0,2600),(10,1,0.02228,0.29257,0.30712846,0.001055,1.5701,1.03219406,944.76,0,2800),(11,1,0.02353,0.27637,0.30776162,0.00099,1.61327,1.03150407,1031.87,0,3000),(12,1,0.0249,0.26127,0.30839739,0.000935,1.65856,1.03081501,1123.58,0,3200),(13,1,0.02641,0.24712,0.3090358,0.000888,1.70629,1.03012687,1220.53,0,3400),(14,1,0.02807,0.2338,0.30967686,0.000848,1.7569,1.02943964,1323.51,0,3600),(15,1,0.02992,0.22117,0.31032058,0.000815,1.81092,1.02875333,1433.49,0,3800),(16,1,0.03199,0.20913,0.31096698,0.000786,1.86904,1.02806794,1551.76,0,4000),(17,1,0.03434,0.19756,0.31161608,0.000762,1.9322,1.02738345,1680.06,0,4200),(18,1,0.03704,0.18631,0.3122679,0.000743,2.00174,1.02669988,1820.83,0,4400),(19,1,0.04022,0.17523,0.31292245,0.000727,2.07972,1.02601722,1977.77,0,4600),(20,1,0.04376,0.16491,0.31353121,0.000717,2.1624,1.02538568,2142.84,0,4785.26),(21,5,0.01516,0.53092,0.30154514,0.00307,1.23892,1.03844572,299.98,0,1000),(22,5,0.01568,0.49541,0.30215546,0.002531,1.27292,1.03774735,363.11,0,1200),(23,5,0.01624,0.45881,0.30276826,0.002148,1.30714,1.03704992,427.64,0,1400),(24,5,0.01687,0.42583,0.30338355,0.001865,1.34182,1.03635343,493.91,0,1600),(25,5,0.01756,0.39728,0.30400135,0.001647,1.37718,1.03565788,562.24,0,1800),(26,5,0.01832,0.37213,0.30462167,0.001476,1.41338,1.03496326,632.89,0,2000),(27,5,0.01917,0.34951,0.30524452,0.001338,1.45059,1.03426956,706.14,0,2200),(28,5,0.02011,0.32892,0.30586993,0.001226,1.48899,1.0335768,782.31,0,2400),(29,5,0.02114,0.31002,0.3064979,0.001133,1.52875,1.03288497,861.72,0,2600),(30,5,0.02228,0.29257,0.30712846,0.001055,1.5701,1.03219406,944.76,0,2800),(31,5,0.02353,0.27637,0.30776162,0.00099,1.61327,1.03150407,1031.87,0,3000),(32,5,0.0249,0.26127,0.30839739,0.000935,1.65856,1.03081501,1123.58,0,3200),(33,5,0.02641,0.24712,0.3090358,0.000888,1.70629,1.03012687,1220.53,0,3400),(34,5,0.02807,0.2338,0.30967686,0.000848,1.7569,1.02943964,1323.51,0,3600),(35,5,0.02992,0.22117,0.31032058,0.000815,1.81092,1.02875333,1433.49,0,3800),(36,5,0.03199,0.20913,0.31096698,0.000786,1.86904,1.02806794,1551.76,0,4000),(37,5,0.03434,0.19756,0.31161608,0.000762,1.9322,1.02738345,1680.06,0,4200),(38,5,0.03704,0.18631,0.3122679,0.000743,2.00174,1.02669988,1820.83,0,4400),(39,5,0.04022,0.17523,0.31292245,0.000727,2.07972,1.02601722,1977.77,0,4600),(40,5,0.04376,0.16491,0.31353121,0.000717,2.1624,1.02538568,2142.84,0,4785.26),(41,8,0.01516,0.53092,0.30154514,0.00307,1.23892,1.03844572,299.98,0,1000),(42,8,0.01568,0.49541,0.30215546,0.002531,1.27292,1.03774735,363.11,0,1200),(43,8,0.01624,0.45881,0.30276826,0.002148,1.30714,1.03704992,427.64,0,1400),(44,8,0.01687,0.42583,0.30338355,0.001865,1.34182,1.03635343,493.91,0,1600),(45,8,0.01756,0.39728,0.30400135,0.001647,1.37718,1.03565788,562.24,0,1800),(46,8,0.01832,0.37213,0.30462167,0.001476,1.41338,1.03496326,632.89,0,2000),(47,8,0.01917,0.34951,0.30524452,0.001338,1.45059,1.03426956,706.14,0,2200),(48,8,0.02011,0.32892,0.30586993,0.001226,1.48899,1.0335768,782.31,0,2400),(49,8,0.02114,0.31002,0.3064979,0.001133,1.52875,1.03288497,861.72,0,2600),(50,8,0.02228,0.29257,0.30712846,0.001055,1.5701,1.03219406,944.76,0,2800),(51,8,0.02353,0.27637,0.30776162,0.00099,1.61327,1.03150407,1031.87,0,3000),(52,8,0.0249,0.26127,0.30839739,0.000935,1.65856,1.03081501,1123.58,0,3200),(53,8,0.02641,0.24712,0.3090358,0.000888,1.70629,1.03012687,1220.53,0,3400),(54,8,0.02807,0.2338,0.30967686,0.000848,1.7569,1.02943964,1323.51,0,3600),(55,8,0.02992,0.22117,0.31032058,0.000815,1.81092,1.02875333,1433.49,0,3800),(56,8,0.03199,0.20913,0.31096698,0.000786,1.86904,1.02806794,1551.76,0,4000),(57,8,0.03434,0.19756,0.31161608,0.000762,1.9322,1.02738345,1680.06,0,4200),(58,8,0.03704,0.18631,0.3122679,0.000743,2.00174,1.02669988,1820.83,0,4400),(59,8,0.04022,0.17523,0.31292245,0.000727,2.07972,1.02601722,1977.77,0,4600),(60,8,0.04376,0.16491,0.31353121,0.000717,2.1624,1.02538568,2142.84,0,4785.26),(61,10,0.01516,0.53092,0.30154514,0.00307,1.23892,1.03844572,299.98,0,1000),(62,10,0.01568,0.49541,0.30215546,0.002531,1.27292,1.03774735,363.11,0,1200),(63,10,0.01624,0.45881,0.30276826,0.002148,1.30714,1.03704992,427.64,0,1400),(64,10,0.01687,0.42583,0.30338355,0.001865,1.34182,1.03635343,493.91,0,1600),(65,10,0.01756,0.39728,0.30400135,0.001647,1.37718,1.03565788,562.24,0,1800),(66,10,0.01832,0.37213,0.30462167,0.001476,1.41338,1.03496326,632.89,0,2000),(67,10,0.01917,0.34951,0.30524452,0.001338,1.45059,1.03426956,706.14,0,2200),(68,10,0.02011,0.32892,0.30586993,0.001226,1.48899,1.0335768,782.31,0,2400),(69,10,0.02114,0.31002,0.3064979,0.001133,1.52875,1.03288497,861.72,0,2600),(70,10,0.02228,0.29257,0.30712846,0.001055,1.5701,1.03219406,944.76,0,2800),(71,10,0.02353,0.27637,0.30776162,0.00099,1.61327,1.03150407,1031.87,0,3000),(72,10,0.0249,0.26127,0.30839739,0.000935,1.65856,1.03081501,1123.58,0,3200),(73,10,0.02641,0.24712,0.3090358,0.000888,1.70629,1.03012687,1220.53,0,3400),(74,10,0.02807,0.2338,0.30967686,0.000848,1.7569,1.02943964,1323.51,0,3600),(75,10,0.02992,0.22117,0.31032058,0.000815,1.81092,1.02875333,1433.49,0,3800),(76,10,0.03199,0.20913,0.31096698,0.000786,1.86904,1.02806794,1551.76,0,4000),(77,10,0.03434,0.19756,0.31161608,0.000762,1.9322,1.02738345,1680.06,0,4200),(78,10,0.03704,0.18631,0.3122679,0.000743,2.00174,1.02669988,1820.83,0,4400),(79,10,0.04022,0.17523,0.31292245,0.000727,2.07972,1.02601722,1977.77,0,4600),(80,10,0.04376,0.16491,0.31353121,0.000717,2.1624,1.02538568,2142.84,0,4785.26);
/*!40000 ALTER TABLE `pvt_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservoir_properties`
--

DROP TABLE IF EXISTS `reservoir_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservoir_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  `formation_thickness` double DEFAULT NULL,
  `external_radius` double DEFAULT NULL,
  `wellbore_radius` double DEFAULT NULL,
  `formation_depthtop` double DEFAULT NULL,
  `rock_density` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reservoir_properties_base_data1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_reservoir_properties_base_data1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservoir_properties`
--

LOCK TABLES `reservoir_properties` WRITE;
/*!40000 ALTER TABLE `reservoir_properties` DISABLE KEYS */;
INSERT INTO `reservoir_properties` VALUES (1,1,300,6000,0.35,9000,0),(2,5,300,4000,0.35,9000,0),(3,8,300,6000,0.35,9000,0),(4,10,300,1500,0.35,9000,0);
/*!40000 ALTER TABLE `reservoir_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservoirs`
--

DROP TABLE IF EXISTS `reservoirs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservoirs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_dataset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reservoir_base_data1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_reservoir_base_data1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservoirs`
--

LOCK TABLES `reservoirs` WRITE;
/*!40000 ALTER TABLE `reservoirs` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservoirs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scenario_results`
--

DROP TABLE IF EXISTS `scenario_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_scenery_results_scenery1_idx` (`scenario_id`),
  CONSTRAINT `fk_scenery_results_scenery1` FOREIGN KEY (`scenario_id`) REFERENCES `scenarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scenario_results`
--

LOCK TABLES `scenario_results` WRITE;
/*!40000 ALTER TABLE `scenario_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `scenario_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scenarios`
--

DROP TABLE IF EXISTS `scenarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `ref` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `sorder` int(11) DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `fk_scenery_project1_idx` (`project_id`),
  CONSTRAINT `fk_scenery_project1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scenarios`
--

LOCK TABLES `scenarios` WRITE;
/*!40000 ALTER TABLE `scenarios` DISABLE KEYS */;
INSERT INTO `scenarios` VALUES (1,1,'M3-100A_Base case','m3100a_001','Basic Simulation',NULL,'01/01/2014','single well simulation',1),(2,1,'M3-100A_IPR Analysis','ipr1','IPR',NULL,'01/10/2014','.',2),(3,1,'M3-100A_Multiparametric Analysis','mult1','Multiparametric',NULL,'10/10/2014','.',3),(4,1,'M3-100A_Asphaltenes Diagnostic','asf001','Asphaltenes Diagnostic',NULL,'11/11/2014','.',4),(5,1,'M3-100A_Asphaltene Diagnostic Simulation','sim2','Basic Simulation',NULL,'01/01/2014','.',5),(7,1,'M3-100A_Asphaltenes Remediation','remasf1','Asphaltenes Remediation',NULL,'13/11/2014','.',6),(9,1,'M3-100A_Global Analysis','sim3','Basic Simulation',NULL,'01/01/2014','.',9),(10,1,'M3-100A_IPR Remediation','iprrem1','IPR',NULL,'20/11/2014',' .',7),(11,2,'M31-00A_Black Oil','sim-1','Basic Simulation',NULL,'26/11/2014','. ',NULL),(12,1,'M3-100A_IPRWellTest','ipr-welltest','IPR',NULL,'27/11/2014','.',8);
/*!40000 ALTER TABLE `scenarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simulation_conditions`
--

DROP TABLE IF EXISTS `simulation_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simulation_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `simulation_id` int(11) DEFAULT NULL,
  `max_timestep` double DEFAULT NULL,
  `min_timestep` double DEFAULT NULL,
  `total_time` double DEFAULT NULL,
  `max_pressuredelta` double DEFAULT NULL,
  `max_saturationdelta` double DEFAULT NULL,
  `max_rsdelta` double DEFAULT NULL,
  `dt_accelfactor` double DEFAULT NULL,
  `relative_error` double DEFAULT NULL,
  `epsilon` double DEFAULT NULL,
  `num_iterations` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simulation_conditions_simulation1_idx` (`simulation_id`),
  CONSTRAINT `fk_simulation_conditions_simulation1` FOREIGN KEY (`simulation_id`) REFERENCES `simulations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simulation_conditions`
--

LOCK TABLES `simulation_conditions` WRITE;
/*!40000 ALTER TABLE `simulation_conditions` DISABLE KEYS */;
INSERT INTO `simulation_conditions` VALUES (1,1,1,0.000001,365,20,0.1,10,2,0.0000001,0.0000001,20),(2,2,1,0.000001,365,10,0.05,100,2,0.00001,0.00001,20),(3,3,1,0.000001,365,10,0.05,100,2,0.00001,0.00001,20),(4,4,1,0.000001,365,10,0.05,100,2,0.00001,0.00001,20);
/*!40000 ALTER TABLE `simulation_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simulation_results`
--

DROP TABLE IF EXISTS `simulation_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simulation_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `simulation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simulation_results_simulation1_idx` (`simulation_id`),
  CONSTRAINT `fk_simulation_results_simulation1` FOREIGN KEY (`simulation_id`) REFERENCES `simulations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simulation_results`
--

LOCK TABLES `simulation_results` WRITE;
/*!40000 ALTER TABLE `simulation_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `simulation_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simulations`
--

DROP TABLE IF EXISTS `simulations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simulations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simulations_scenarios1_idx` (`scenario_id`),
  CONSTRAINT `fk_simulations_scenarios1` FOREIGN KEY (`scenario_id`) REFERENCES `scenarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simulations`
--

LOCK TABLES `simulations` WRITE;
/*!40000 ALTER TABLE `simulations` DISABLE KEYS */;
INSERT INTO `simulations` VALUES (1,1),(2,5),(3,9),(4,11);
/*!40000 ALTER TABLE `simulations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skin_conditions`
--

DROP TABLE IF EXISTS `skin_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skin_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` double DEFAULT NULL,
  `skin` double DEFAULT NULL,
  `skin_radius` double DEFAULT NULL,
  `base_dataset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_skin_conditions_base_datasets1_idx` (`base_dataset_id`),
  CONSTRAINT `fk_skin_conditions_base_datasets1` FOREIGN KEY (`base_dataset_id`) REFERENCES `base_datasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skin_conditions`
--

LOCK TABLES `skin_conditions` WRITE;
/*!40000 ALTER TABLE `skin_conditions` DISABLE KEYS */;
INSERT INTO `skin_conditions` VALUES (1,0,3,0.4,1),(2,365,3,0.4,1),(3,0,3,0.4,5),(4,31,5.3,0.4,5),(5,59,10.49,0.4,5),(6,90,11.82,0.4,5),(7,0,3,0.4,8),(8,31,5.3,0.4,8),(9,59,10.49,0.4,8),(10,90,11.82,0.4,8),(11,151,12.28,0.4,8),(12,155,6.8,0.4,8),(13,157,5,0.4,8),(14,151,12.28,0.4,5),(15,365,12.28,0.4,5),(16,0,2,0.4,10),(17,365,3,0.4,10),(18,250,8,0.4,8),(19,300,10,0.4,8),(20,360,13,0.4,8),(21,365,13,0.4,8);
/*!40000 ALTER TABLE `skin_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wells`
--

DROP TABLE IF EXISTS `wells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `long_` double DEFAULT NULL,
  `wellbore_radius` double DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_wells_fields1_idx` (`field_id`),
  CONSTRAINT `fk_wells_fields1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wells`
--

LOCK TABLES `wells` WRITE;
/*!40000 ALTER TABLE `wells` DISABLE KEYS */;
INSERT INTO `wells` VALUES (1,1,4.248133,-71.726303,0.35,'WELL-M3-100A'),(2,1,4.304281,-71.560135,0.5,'WELL-002'),(3,1,4.242655,-71.602707,0.6,'WELL-003'),(4,2,5.000979,-72.101212,0.4,'WELL-051'),(5,2,4.969513,-72.021561,0.2,'WELL-052');
/*!40000 ALTER TABLE `wells` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-27 12:17:15
