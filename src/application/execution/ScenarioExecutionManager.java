package application.execution;

import application.controllers.MainController;
import application.models.Scenario;
//TODO esto puede ser un workflow
public abstract class ScenarioExecutionManager {
	protected Scenario scenario;
	private MainController mc;
	
	public abstract boolean execute();
	
	public ScenarioExecutionManager(MainController mc, Scenario scenario) {
		this.mc = mc;
		this.scenario = scenario;
	}
	public Scenario getScenario() {
		return scenario;
	}
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}
	
}
