package application.controllers.results.asphaltenes_remediation;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.models.Scenario;

public class AsphaltenesRemediationResultsController  implements Initializable, ControlledScreen{
	private Workflow workflowController;
	@FXML
    private LineChart<Number, Number> damageChart;

    @FXML
    private LineChart<Number, Number> pressureChart;

    @FXML
    private Label skinText; 
    @FXML
    private Tab precipitadoChart;
    
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		this.skinText.setText(this.skinText.getText() + " " + 5.2);
		double [] data_prec = {8413.789761, 8430.169739, 8448.049186, 8465.925526, 8483.430915, 8500.920261, 8518.034157, 8534.768179, 8551.124327, 8567.100314, 8582.700315, 8598.281396, 8613.122598, 8627.944649, 8642.026573, 8656.089129, 8669.770689, 8682.711793, 8695.631049, 8707.809655, 8719.966228, 8731.384178, 8742.777736, 8753.430335, 8763.701254, 8773.949756, 8783.454925, 8792.578244, 8800.960375, 8809.317666, 8817.290773, 8824.52262, 8831.370255, 8837.83588, 8843.91504, 8849.612175, 8854.56588, 8859.494542, 8863.679828, 8867.119588, 8870.536575, 8873.208137, 8875.493435, 8877.396983, 8878.555362, 8879.327688, 8812.508743, 8708.587001, 8605.854873, 8504.312706, 8403.956506, 8305.14222, 8207.156472, 8110.35118, 8014.726558, 7920.282731, 7827.015763, 7734.92573, 7644.008741, 7553.908811, 7465.338129, 7377.580992, 7290.636099, 7205.214274, 7120.602994, 7036.800896, 6954.515614, 6873.037682, 6792.36395, 6712.848624, 6634.489643, 6556.931534, 6480.52609, 6404.918275, 6330.109929, 6256.447289, 6183.933125, 6111.859473, 6041.281269, 5971.140695, 5902.139926, 5833.928619, 5766.502277, 5700.210323, 5634.352334, 5569.626843, 5506.031754, 5442.864979, 5380.476877, 5319.215539, 5258.728117, 5198.664015, 5139.722994, 5081.552854, 5024.15213, 4967.171036, 4911.304982, 4856.203986, 4801.520141, 4747.947596, 4694.788446, 4642.390181, 4590.750147, 4539.869295, 4489.743799, 4440.027133, 4391.065695, 4342.855752, 4295.397039, 4248.343774, 4202.039291, 4156.482186, 4111.327257, 4066.572608, 4022.907473, 3979.640225, 3936.770081, 3894.641182, 3853.252134, 3812.257041, 3771.65712, 3731.792706, 3692.319531, 3653.581505, 3615.23279, 3577.273585, 3540.045285, 3503.204577, 3467.092487, 3431.366095, 3396.023749, 3361.066508, 3326.833757, 3292.64162, 3259.173008, 3226.425753, 3193.717976, 3161.729739, 3129.779905, 3098.547811, 3067.692631, 3037.213656, 3007.449397, 2977.721345, 2948.366602, 2919.724732, 2891.116726, 2862.879988, 2835.353523, 2807.859413, 2781.073106, 2754.318904, 2728.27008, 2702.251649, 2676.601292, 2651.316915, 2626.399297, 2601.846377, 2577.658916, 2553.835573, 2530.038949, 2506.606199, 2483.871758, 2461.162674, 2438.480158, 2416.49327, 2394.532038, 2372.929968, 2351.68774, 2330.803474, 2309.942776, 2289.774082, 2269.293975, 2249.504686, 2229.736958, 2210.324933, 2191.268029, 2172.232351, 2153.550957, 2135.222696, 2116.914739, 2098.959112, 2081.023858, 2063.772513, 2046.207381, 2029.325626, 2012.462388, 1995.617034, 1979.121646, 1962.975148, 1946.846221, 1931.065423, 1915.301211, 1899.884904, 1884.815975, 1869.431716, 1854.724824, 1840.033764, 1825.357458};
		double [] data_pres = {14.7, 24.7, 34.7, 44.7, 54.7, 64.7, 74.7, 84.7, 94.7, 104.7, 114.7, 124.7, 134.7, 144.7, 154.7, 164.7, 174.7, 184.7, 194.7, 204.7, 214.7, 224.7, 234.7, 244.7, 254.7, 264.7, 274.7, 284.7, 294.7, 304.7, 314.7, 324.7, 334.7, 344.7, 354.7, 364.7, 374.7, 384.7, 394.7, 404.7, 414.7, 424.7, 434.7, 444.7, 454.7, 464.7, 474.7, 484.7, 494.7, 504.7, 514.7, 524.7, 534.7, 544.7, 554.7, 564.7, 574.7, 584.7, 594.7, 604.7, 614.7, 624.7, 634.7, 644.7, 654.7, 664.7, 674.7, 684.7, 694.7, 704.7, 714.7, 724.7, 734.7, 744.7, 754.7, 764.7, 774.7, 784.7, 794.7, 804.7, 814.7, 824.7, 834.7, 844.7, 854.7, 864.7, 874.7, 884.7, 894.7, 904.7, 914.7, 924.7, 934.7, 944.7, 954.7, 964.7, 974.7, 984.7, 994.7, 1004.7, 1014.7, 1024.7, 1034.7, 1044.7, 1054.7, 1064.7, 1074.7, 1084.7, 1094.7, 1104.7, 1114.7, 1124.7, 1134.7, 1144.7, 1154.7, 1164.7, 1174.7, 1184.7, 1194.7, 1204.7, 1214.7, 1224.7, 1234.7, 1244.7, 1254.7, 1264.7, 1274.7, 1284.7, 1294.7, 1304.7, 1314.7, 1324.7, 1334.7, 1344.7, 1354.7, 1364.7, 1374.7, 1384.7, 1394.7, 1404.7, 1414.7, 1424.7, 1434.7, 1444.7, 1454.7, 1464.7, 1474.7, 1484.7, 1494.7, 1504.7, 1514.7, 1524.7, 1534.7, 1544.7, 1554.7, 1564.7, 1574.7, 1584.7, 1594.7, 1604.7, 1614.7, 1624.7, 1634.7, 1644.7, 1654.7, 1664.7, 1674.7, 1684.7, 1694.7, 1704.7, 1714.7, 1724.7, 1734.7, 1744.7, 1754.7, 1764.7, 1774.7, 1784.7, 1794.7, 1804.7, 1814.7, 1824.7, 1834.7, 1844.7, 1854.7, 1864.7, 1874.7, 1884.7, 1894.7, 1904.7, 1914.7, 1924.7, 1934.7, 1944.7, 1954.7, 1964.7, 1974.7, 1984.7, 1994.7};
		XYChart.Series series = new XYChart.Series();
		List list = series.getData();
		
		for (int i=0; i< data_pres.length; i++){
			list.add(new XYChart.Data(data_pres[i], data_prec[i]));
		}
		series.setData((ObservableList) list);
		series.setName("Precipitado de Asfaltenos");
		pressureChart.getData().add(series);
		pressureChart.setCreateSymbols(false);
	}
	
	@FXML
	protected void backToScenarioButtonAction(MouseEvent ev) {
		Scenario currentScenario = ((MainWorkflow) workflowController)
				.getProjectWorkflow().getProject().getCurrent();
		if (currentScenario != null) {
			ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
					workflowController.getMainController(),
					((MainWorkflow) workflowController).getProjectWorkflow()
							.getProject(), currentScenario);
			((MainWorkflow)workflowController).getProjectWorkflow().setScenaryWorkflow(scenaryWorkflow);
			scenaryWorkflow.initScenarioWorkflow();
		}
	}

}
