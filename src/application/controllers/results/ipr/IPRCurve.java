/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.controllers.results.ipr;

import javafx.embed.swing.SwingNode;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import application.utils.PlatformHelper;

/**
 *
 * @author EdwinH
 */
public class IPRCurve {
    //Datos Entrada
    double Pr;
    double Pb;
    double PwfPF;
    double Q;
    double Fpf1;
    double F1;
    double Fpf2;
    double F2;
    String model;
    double Pskin;
    double s_proximate;
    
   public double X1[];
   public double Y1[];
   public double X2[];
   public double Y2[];

	public double getPskin() {
		return Pskin;
	}

    public double getS_proximate() {
		return s_proximate;
	}
    
	//Variables intermedias
    double Gq;
    double GPwf;

    public void setPr(double Pr) {
        this.Pr = Pr;
    }

    public void setPb(double Pb) {
        this.Pb = Pb;
    }

    public void setPwfPF(double PwfPF) {
        this.PwfPF = PwfPF;
    }

    public void setQ(double Q) {
        this.Q = Q;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setFpf1(double Fpf1) {
        this.Fpf1 = Fpf1;
    }
    
    public void setFpf2(double Fpf2) {
        this.Fpf2 = Fpf2;
    }
    
    public void setF1(double F1) {
        this.F1 = F1;
    }
    
    public void setF2(double F2) {
        this.F2 = F2;
    }
    
    public void ipr_curves(SwingNode swingNode){
        if((this.PwfPF>=this.Pr )){
            JTextArea message = new JTextArea("NO PRODUCTION!!\nPwf(PF) >= Pr");
            JPanel panel = new JPanel();
            panel.add(message);
            swingNode.setContent(panel);
        } else {
            XYSeriesCollection dataset= new XYSeriesCollection();
            //Curva base
            XYSeries serie1 = new XYSeries("Ideal IPR");
             X1=new double[(int)this.Pr+100];
             Y1=new double[(int)this.Pr+100];
            
            this.Fpf1 = 1.0;
            this.F1 = 1.0;            
            for(int i=0;i<Pr;i++){
                CalcularQ ((double)i, this.Fpf1, this.F1);
                X1[i] = Gq;
                Y1[i] = GPwf;
            } 
            
             for(int i=0;i<Pr;i++){
                 serie1.add(X1[i], Y1[i]);
            }
             
            dataset.addSeries(serie1);
             
            //Curva actual
            XYSeries serie2 = new XYSeries("Current IPR");
             X2=new double[(int)this.Pr+100];
             Y2=new double[(int)this.Pr+100];
            
            this.Fpf2 = 1.0;
            for(int i=0;i<Pr;i++){
                CalcularQ ((double)i, this.Fpf2, this.F2);
                X2[i] = Gq;
                Y2[i] = GPwf;
            } 
            
             for(int i=0;i<Pr;i++){
                 serie2.add(X2[i], Y2[i]);
            }
             
            dataset.addSeries(serie2);
            JFreeChart chart = ChartFactory.createXYLineChart("IPR CURVE", "Rate (BPD)", "Pressure (PSI)", dataset, PlotOrientation.VERTICAL, true, true, false);        
            ChartPanel frame = new ChartPanel(chart);
            PlatformHelper.run( () -> {
            swingNode.setContent(frame);});
            
        }
        
        double Pwfi = Pr - F2*(Pr - PwfPF);
        Pskin = ((Pr - Pwfi)/F2) - Pr + Pwfi; 
        double PskinN = Pskin - (1-F2)*Pskin;
        double Fm = (Pr - Pwfi)/(Pr - Pwfi + PskinN);
        
        // S proximate calculation
        s_proximate = (7.0 / F2) - 7.0;
    }
        
    public void CalcularQ(double pwf, double Fpf, double F){
        if((this.PwfPF<this.Pr)){
            if((this.Pr>this.Pb)&&(this.PwfPF>this.Pb))
                Caso1q (pwf, Fpf, F);                             //PARTE RECTA Y CURVA                               
            if((this.Pr>this.Pb)&&(this.PwfPF<=this.Pb))
                Caso2q (pwf, Fpf, F);                              //PARTE RECTA-CURVA
            if((this.Pr<=this.Pb)&&(this.PwfPF<this.Pb))
                Caso3q (pwf, Fpf, F);                              //SOLO PARTE CURVA            
        }  
    }
    
     public void Caso1q (double Pwf, double FPF, double F){
        double JPF,Ji,J,qbi,qci,qb,R,q;         
        JPF= Q/(Pr-PwfPF); 
        Ji= JPF/FPF; 
        qbi= Ji*(Pr-Pb);
        qci= (qbi*Pb)/(1.8*(Pr-Pb));
        qb= F*qbi;
        if((Pwf<=Pb)&&(F<=1))                                                    //TRAMO CURVO STANDING - PATTON
        {   R= Pwf/Pb;
            q= F*(1-R)*(1.8-0.8*F*(1-R))*qci + qb;
            Imprimirq (F,Pwf,q);
        }  
        if((Pwf<=Pb)&&(F>1))                                                     //TRAMO CURVO LEKIA-EVANS
        {   R= Pwf/Pb;
            q= ( Math.pow(F,0.4) )*(1-R*R)*qci + qb;
            Imprimirq (F,Pwf,q);                    
        }   
        if((Pwf>Pb))                                                            //TRAMO RECTO
        {   J= F*Ji;
            q= J*(Pr-Pwf);
            Imprimirq (F,Pwf,q);                    
        }   
    }    
    public void Caso2q (double Pwf, double FPF, double F){
        double Pwfi,Ri,qci,qbi,qb,Ji,J,R,q,s;                                     //duda con volver a declarar R        
        
        Pwfi= Pr-FPF*(Pr-PwfPF);                                                
        if(Pwfi>Pb) { Caso1q (Pwf, FPF, F);}
            else 
            {   Ri= Pwfi/Pb;
                qci= Q/( 1-0.2*Ri-0.8*Ri*Ri+1.8*((Pr-Pb)/Pb) );
                qbi= (1.8*qci*(Pr-Pb))/Pb;
                qb= F*qbi;
                if((Pwf<=Pb)&&(F<=1))                                                    //TRAMO CURVO STANDING - PATTON
                {   R= Pwf/Pb;
                    q= F*(1-R)*(1.8-0.8*F*(1-R))*qci + qb;
                    Imprimirq (F,Pwf,q);
                }  
                if((Pwf<=Pb)&&(F>1))                                                     //TRAMO CURVO LEKIA-EVANS
                {   R= Pwf/Pb;
                    q= ( Math.pow(F,0.4) )*(1-R*R)*qci + qb;
                    Imprimirq (F,Pwf,q);                    
                }    
                if((Pwf>Pb))                                                            //TRAMO RECTO
                {   Ji= qbi/(Pr-Pb);
                    J= F*Ji;
                    q= J*(Pr-Pwf);
                    Imprimirq (F,Pwf,q);                    
                } 
            }
    }    
    public void Caso3q (double Pwf, double FPF, double F){
        double Aofi,RPF,Pwfi,Ri,R,q;
        RPF= PwfPF/Pb;
        if(FPF<=1)                                                              //duda con R(PF) ??
        { Aofi= Q/( FPF*(1-RPF)*(1.8-0.8*FPF*(1-RPF)) );}
            else
            {   Pwfi=Pr-FPF*(Pr-PwfPF);
                Ri= Pwfi/Pb;                                                    //duda con Ri
                Aofi= Q/(1-0.2*Ri-0.8*Ri*Ri);
            }
        if(F<=1)                                                                //STANDING
        {   R=Pwf/Pr;
            q= F*(1-R)*(1.8-0.8*F*(1-R))*Aofi;
            Imprimirq (F,Pwf,q); 
        }
        if(F>1)                                                                 //LEKIA-EVANS
        {   R=Pwf/Pr;
            q= Aofi*Math.pow(F,0.4)*(1-R*R);
            Imprimirq (F,Pwf,q);
        }
    }


    public void Imprimirq (double F, double Pwf, double q){
        Gq=q;   GPwf=Pwf;
    }    
}