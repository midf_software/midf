package application.controllers.results.ipr;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.MainWorkflow;
import application.controllers.project.scenary.preanalysis.ipr.IPRDataController;
import application.controllers.results.ScenarioResultsManager;
import application.models.Scenario;

public class IPRResultsManager extends ScenarioResultsManager{

	public IPRResultsManager(MainController mc, Scenario scenario) {
		super(mc, scenario);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void showResults() {
		// TODO Auto-generated method stub
		MainWorkflow mw = mc.getMainWorkflowController();
		ControlledScreen resultsController = mw
				.setInternalPanel(AppMain.getConfigProperties()
						.getProperty("ipr.results.view"));
		
		IPRResultsController ipc = (IPRResultsController) resultsController;
		
		ipc.setScenario(scenario);
		ipc.drawIPR();
	}

}
