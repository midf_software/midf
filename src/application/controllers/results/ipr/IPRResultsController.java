package application.controllers.results.ipr;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.models.IPR;
import application.models.Scenario;
import application.utils.PlatformHelper;

public class IPRResultsController implements Initializable, ControlledScreen {

	private Workflow workflowController;
	private Scenario sc;
	
	@FXML
	private SwingNode swingNode;
	@FXML
	private Label  lb_dp_skin;
	
	@FXML
	private Label  lb_skin_factor;
	private IPRCurve ipr; 
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	public void drawIPR() {
		
		       IPR iprModel = (IPR)this.getScenario().getModule();
				 ipr = new IPRCurve();

			        //Mandamos los datos al objeto IPRCurve
			        ipr.setPr(iprModel.getPr());
			        ipr.setPb(iprModel.getPb());
			        ipr.setPwfPF(iprModel.getPwfPF());
			        ipr.setQ(iprModel.getQ());
			        ipr.setF2(iprModel.getF2());
			        ipr.setModel("Vogel");
			      
			        ipr.ipr_curves(swingNode);
			        this.lb_dp_skin.setText(this.lb_dp_skin.getText() + Double.toString(ipr.getPskin()));
			        this.lb_skin_factor.setText(this.lb_skin_factor.getText() +String.format("%.2f", ipr.getS_proximate()));
			        
	}
	@Override
	public void setWorkflowController(Workflow workflowController) {
		// TODO Auto-generated method stub
		this.workflowController = workflowController;
	}
	
	@Override
	public Workflow getWorkflowController() {
		// TODO Auto-generated method stub
		return this.workflowController;
	}
	
	@FXML
	protected void backToScenarioButtoAction(MouseEvent ev) {
		Scenario currentScenario = ((MainWorkflow) workflowController)
				.getProjectWorkflow().getProject().getCurrent();
		if (currentScenario != null) {
			ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
					workflowController.getMainController(),
					((MainWorkflow) workflowController).getProjectWorkflow()
							.getProject(), currentScenario);
			((MainWorkflow)workflowController).getProjectWorkflow().setScenaryWorkflow(scenaryWorkflow);
			scenaryWorkflow.initScenarioWorkflow();
		}
	}
	
	public void cleanButtonAction(MouseEvent ev) {
		
	}

	public Scenario getScenario() {
		return sc;
	}

	public void setScenario(Scenario sc) {
		this.sc = sc;
	}
	
    @FXML
    private void exportEvent(ActionEvent event) {    
        String cadena = "IPR Results \r\n \r\n";
        cadena += "Curve IPR Base \r\n";
        cadena += "X , Y \r\n";
        for(int i=0; i<ipr.X1.length; i++){
            cadena += ipr.X1[i] + " , " + ipr.Y1[i] + "\r\n";        
        }
        cadena += " \r\n \r\n";
        cadena += "Curve IPR Real \r\n";
        cadena += "X , Y \r\n";
        for(int i=0; i<ipr.X2.length; i++){
            cadena += ipr.X2[i] + " , " + ipr.Y2[i] + "\r\n";        
        }
        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("todos los archivos *.csv", "csv","CSV"));//filtro para ver solo archivos .csv
        int seleccion = fileChooser.showSaveDialog(null);
        try{
            if (seleccion == JFileChooser.APPROVE_OPTION){//comprueba si ha presionado el boton de aceptar
                File JFC = fileChooser.getSelectedFile();
                String PATH = JFC.getAbsolutePath();//obtenemos el path del archivo a guardar
                PrintWriter printwriter = new PrintWriter(JFC);
                printwriter.print(cadena);//escribe en el archivo todo lo que se encuentre en el JTextArea
                printwriter.close();//cierra el archivo

                //comprobamos si a la hora de guardar obtuvo la extension y si no se la asignamos
                if(!(PATH.endsWith(".csv"))){
                    File temp = new File(PATH+".csv");
                    JFC.renameTo(temp);//renombramos el archivo
                }

                JOptionPane.showMessageDialog(null,"Guardado exitoso!", "Guardado exitoso!", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (Exception e){//por alguna excepcion salta un mensaje de error
            JOptionPane.showMessageDialog(null,"Error al guardar el archivo!", "Oops! Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
	
}