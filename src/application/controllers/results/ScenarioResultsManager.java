package application.controllers.results;

import application.controllers.MainController;
import application.models.Scenario;
//TODO esto puede ser un workflow
public abstract class ScenarioResultsManager {
	protected Scenario scenario;
	protected MainController mc;
	
	public abstract void showResults();
	
	public ScenarioResultsManager(MainController mc, Scenario scenario) {
		this.mc = mc;
		this.scenario = scenario;
	}
	
	public Scenario getScenario() {
		return scenario;
	}
	
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}
}
