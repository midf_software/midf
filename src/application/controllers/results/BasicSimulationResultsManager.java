package application.controllers.results;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.MainWorkflow;
import application.controllers.simulation.results.ResultsController;
import application.models.Scenario;

public class BasicSimulationResultsManager extends ScenarioResultsManager{

	public BasicSimulationResultsManager(MainController mc, Scenario scenario) {
		super(mc, scenario);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void showResults() {
		MainWorkflow mw = mc.getMainWorkflowController();
		ControlledScreen resultsController = mw
				.setInternalPanel(AppMain.getConfigProperties()
						.getProperty("results.view"));
		ResultsController rc = ((ResultsController) resultsController);
		
		rc.setScenario(mw.getProjectWorkflow()
						.getProject().getCurrent());
	    rc.setLayers(mw.getProjectWorkflow()
						.getProject().getCurrent().getInputData().getGrid().getVerticalNumblocks());
	    rc.loadProductionCharts(false);		
	}

}
