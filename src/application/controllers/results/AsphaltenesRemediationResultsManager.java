package application.controllers.results;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.MainWorkflow;
import application.controllers.results.asphaltenes.AsphaltenesResultsController;
import application.controllers.results.asphaltenes_remediation.AsphaltenesRemediationResultsController;
import application.models.Scenario;

public class AsphaltenesRemediationResultsManager extends ScenarioResultsManager{
	public AsphaltenesRemediationResultsManager(MainController mc, Scenario scenario) {
		super(mc, scenario);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void showResults() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				MainWorkflow mw = mc.getMainWorkflowController();
				ControlledScreen resultsController = mw
						.setInternalPanel(AppMain.getConfigProperties()
								.getProperty("asphaltenes_remediation.results.view"));
				
				AsphaltenesRemediationResultsController ac = (AsphaltenesRemediationResultsController) resultsController;
	}
}
