package application.controllers.results.multiparametric;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.models.IPR;
import application.models.Multiparametric;
import application.models.Scenario;
import application.utils.PlatformHelper;

public class MultiparametricResultsController implements Initializable, ControlledScreen {

	private Workflow workflowController;
	private Scenario sc;
	
	 @FXML
     private Tab tab_SkinDiagram;
     @FXML
     private Tab tab_SkinParameters;
     @FXML
     private SwingNode swingNode1;
     @FXML
     private SwingNode swingNode2;
     @FXML
     private SwingNode swingNode3;
     @FXML
     private SwingNode swingNode4;
     @FXML
     private SwingNode swingNode5;
     @FXML
     private SwingNode swingNode6;
     @FXML
     private SwingNode swingNode7;
     
     
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@Override
	public void setWorkflowController(Workflow workflowController) {
		// TODO Auto-generated method stub
		this.workflowController = workflowController;
	}
	
	@Override
	public Workflow getWorkflowController() {
		// TODO Auto-generated method stub
		return this.workflowController;
	}
	
	@FXML
	protected void backToScenarioButtonAction(MouseEvent ev) {
		Scenario currentScenario = ((MainWorkflow) workflowController)
				.getProjectWorkflow().getProject().getCurrent();
		if (currentScenario != null) {
			ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
					workflowController.getMainController(),
					((MainWorkflow) workflowController).getProjectWorkflow()
							.getProject(), currentScenario);
			((MainWorkflow)workflowController).getProjectWorkflow().setScenaryWorkflow(scenaryWorkflow);
			scenaryWorkflow.initScenarioWorkflow();
		}
	}
	public void draw_results() {
		//serialize
		//this.serialize_mp(); //enable to update
		Multiparametric mp  = this.load_mp();
		 PlatformHelper.run(() -> {
		        mp.spiderchart(swingNode1, tab_SkinDiagram);
		        mp.spiderchart_parameter1(swingNode2, tab_SkinParameters);
		        mp.spiderchart_parameter2(swingNode3, tab_SkinParameters);
		        mp.spiderchart_parameter3(swingNode4, tab_SkinParameters);
		        mp.spiderchart_parameter4(swingNode5, tab_SkinParameters);
		        mp.spiderchart_parameter5(swingNode6, tab_SkinParameters);
		        mp.spiderchart_parameter6(swingNode7, tab_SkinParameters);});
	}
	public void cleanButtonAction(MouseEvent ev) {
		
	}

	public Scenario getScenario() {
		return sc;
	}

	public void setScenario(Scenario sc) {
		this.sc = sc;
	}
	
	
	private Multiparametric load_mp() {
		Multiparametric mp  = (Multiparametric)this.sc.getModule();
		return mp;
	}
	 @FXML
	    private void exportEvent(ActionEvent event) {
			Multiparametric mp  = this.load_mp();
	        String cadena = "Multiparametric Results \r\n \r\n";
	        cadena += "Statistical Parameters Diagram: \r\n";
	        cadena += "Statistical Mineral Scales \r\n";
	        cadena += "MSP1:, " + mp.getMsp1() + " \r\n";
	        cadena += "MSP2:, " + mp.getMsp2() + " \r\n";
	        cadena += "MSP3:, " + mp.getMsp3() + " \r\n";
	        cadena += "MSP4:, " + mp.getMsp4() + " \r\n";
	        cadena += "MSP5:, " + mp.getMsp5() + " \r\n \r\n";
	        
	        cadena += "Statistical Fines Blockage \r\n";
	        cadena += "FBP1:, " + mp.getFbp1() + " \r\n";
	        cadena += "FBP2:, " + mp.getFbp2() + " \r\n";
	        cadena += "FBP3:, " + mp.getFbp3() + " \r\n";
	        cadena += "FBP4:, " + mp.getFbp4() + " \r\n";
	        cadena += "FBP5:, " + mp.getFbp5() + " \r\n \r\n";

	        cadena += "Statistical Organic Scales \r\n";
	        cadena += "OSP1:, " + mp.getOsp1() + " \r\n";
	        cadena += "OSP2:, " + mp.getOsp2() + " \r\n";
	        cadena += "OSP3:, " + mp.getOsp3() + " \r\n";
	        cadena += "OSP4:, " + mp.getOsp4() + " \r\n";
	        cadena += "OSP5:, " + mp.getOsp5() + " \r\n \r\n";
	        
	        cadena += "Statistical Relative Permeability \r\n";
	        cadena += "KRP1:, " + mp.getKrp1() + " \r\n";
	        cadena += "KRP2:, " + mp.getKrp2() + " \r\n";
	        cadena += "KRP3:, " + mp.getKrp3() + " \r\n";
	        cadena += "KRP4:, " + mp.getKrp4() + " \r\n";
	        cadena += "KRP5:, " + mp.getKrp5() + " \r\n \r\n";
	        
	        cadena += "Statistical Induced Damage \r\n";
	        cadena += "IDP1:, " + mp.getIdp1() + " \r\n";
	        cadena += "IDP2:, " + mp.getIdp2() + " \r\n";
	        cadena += "IDP3:, " + mp.getIdp3() + " \r\n";
	        cadena += "IDP4:, " + mp.getIdp4() + " \r\n \r\n";
	        
	        cadena += "Statistical Geomechanical Damage \r\n";
	        cadena += "GDP1:, " + mp.getGdp1() + " \r\n";
	        cadena += "GDP2:, " + mp.getGdp2() + " \r\n";
	        cadena += "GDP3:, " + mp.getGdp3() + " \r\n";
	        cadena += "GDP4:, " + mp.getGdp4() + " \r\n \r\n \r\n";
	        
	        cadena += "Multiparametric Characterization Diagram \r\n";
	        cadena += "MSP:, " + mp.getMineral_scales_avg() + " \r\n";
	        cadena += "FBP:, " + mp.getFines_blockage_avg() + " \r\n";
	        cadena += "OSP:, " + mp.getOrganic_scales_avg() + " \r\n";
	        cadena += "KrP:, " + mp.getRelative_permeability_avg() + " \r\n";
	        cadena += "IDP:, " + mp.getInduced_damage_avg() + " \r\n";
	        cadena += "GDP:, " + mp.getGeomechanical_damage_avg() + " \r\n";
	        
	        JFileChooser fileChooser = new JFileChooser();
	        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("todos los archivos *.csv", "csv","CSV"));//filtro para ver solo archivos .csv
	        int seleccion = fileChooser.showSaveDialog(null);
	        try{
	            if (seleccion == JFileChooser.APPROVE_OPTION){//comprueba si ha presionado el boton de aceptar
	                File JFC = fileChooser.getSelectedFile();
	                String PATH = JFC.getAbsolutePath();//obtenemos el path del archivo a guardar
	                PrintWriter printwriter = new PrintWriter(JFC);
	                printwriter.print(cadena);//escribe en el archivo todo lo que se encuentre en el JTextArea
	                printwriter.close();//cierra el archivo

	                //comprobamos si a la hora de guardar obtuvo la extension y si no se la asignamos
	                if(!(PATH.endsWith(".csv"))){
	                    File temp = new File(PATH+".csv");
	                    JFC.renameTo(temp);//renombramos el archivo
	                }

	                JOptionPane.showMessageDialog(null,"Guardado exitoso!", "Guardado exitoso!", JOptionPane.INFORMATION_MESSAGE);
	            }
	        }catch (Exception e){//por alguna excepcion salta un mensaje de error
	            JOptionPane.showMessageDialog(null,"Error al guardar el archivo!", "Oops! Error", JOptionPane.ERROR_MESSAGE);
	        }
	    }
	 
}