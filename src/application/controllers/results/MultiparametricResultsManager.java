package application.controllers.results;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.MainWorkflow;
import application.controllers.results.ipr.IPRResultsController;
import application.controllers.results.multiparametric.MultiparametricResultsController;
import application.models.Scenario;

public class MultiparametricResultsManager extends ScenarioResultsManager {

	public MultiparametricResultsManager(MainController mc, Scenario scenario) {
		super(mc, scenario);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void showResults() {
		// TODO Auto-generated method stub
		MainWorkflow mw = mc.getMainWorkflowController();
		ControlledScreen resultsController = mw
				.setInternalPanel(AppMain.getConfigProperties()
						.getProperty("multiparametric.results"));
		
		MultiparametricResultsController ipc = (MultiparametricResultsController) resultsController;
		ipc.setScenario(this.scenario);
		ipc.draw_results();
	}

}
