package application.controllers.dashboard;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;

/**
 * Dashboard screen controller
 * 
 * @author CV
 *
 */
public class DashboardController implements Initializable, ControlledScreen {

	MainWorkflow workflowController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO: Show options for specific user/roles?
	}

	/**
	 * Action called on project button action, calls the project
	 * dashboard screen.
	 * 
	 * @param event
	 */
	@FXML
	protected void projectButtonAction(MouseEvent event) {
		workflowController.setInternalPanel(AppMain.getConfigProperties().getProperty(
				"project.dashboard.view"));
	}

	/**
	 * Action called on fields button action, calls the fields screen.
	 * 
	 * @param event
	 */
	@FXML
	protected void fieldsButtonAction(MouseEvent event) {
		workflowController
				.setInternalPanel(AppMain.getConfigProperties().getProperty("fields.view"));
	}

	/**
	 * Sets the workflow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (MainWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}
}
