package application.controllers.dashboard;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.models.Project;

/**
 * Project dashboard screen controller
 * 
 * @author CV
 *
 */
public class ProjectDashboardController implements Initializable,
		ControlledScreen {

	MainWorkflow workflowController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Specific options for user/roles?
	}

	/**
	 * New project button action, calls the first project form
	 * General data.
	 * 
	 * @param event
	 */
	@FXML
	protected void newProjectAction(MouseEvent event) {
		workflowController.instanceNewProject();
	}

	/**
	 * Load project button action, inits the search project srceen.
	 * 
	 * @param event
	 */
	@FXML
	protected void loadProjectAction(MouseEvent event) {
		workflowController.setInternalPanel(AppMain.getConfigProperties().getProperty("load.project.view"));
	}

	/**
	 * Sets the workflow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (MainWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}
}
