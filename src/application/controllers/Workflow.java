package application.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.sun.javafx.stage.StageHelper;

import application.controllers.project.ProjectWorkflow;
import application.controllers.project.scenary.ScenaryGeneralDataController;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
/**
 * Class setting the workflow controllers behavior
 * 
 * @author CV
 *
 */
public class Workflow {

	private MainController mainController;

	/**
	 * Constructs
	 * 
	 * @param mainController
	 *            Main GUI controller
	 */
	public Workflow(MainController mainController) {
		this.mainController = mainController;
	}

	/**
	 * Sets an internal panel (screen) on the workspace.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setInternalPanel(final String resource) {

		closeCurrentModals();

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
			Parent loadScreen = (Parent) loader.load();

			ControlledScreen screenControler = ((ControlledScreen) loader
					.getController());
			screenControler.setWorkflowController(this);

			mainController.addInternalPanel(loadScreen);

			enableProjectToolbar(screenControler
					.getWorkflowController() instanceof ScenaryWorkflow||
					screenControler
					.getWorkflowController() instanceof ScenaryWorkflowBase);

			return screenControler;

		} catch (IOException e) {
			// TODO: manejo excepciones
			System.out.println(e);
			return null;
		}
	}

	/**
	 * Shows a modal, containing the screen passed as @resource.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setModal(final String resource, String title,
			boolean closeCurrent, boolean cleanWorkspace) {

		// Remove internal panels?
		if (cleanWorkspace) {
			mainController.removeInternalPanel();
		}

		// 1 modal at time?
		if (closeCurrent) {
			closeCurrentModals();
		}

		try {

			final Stage modalStage = new Stage();
			// Do not block owner window events
			modalStage.initModality(Modality.WINDOW_MODAL);

			FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
			Parent loadScreen = (Parent) loader.load();
			
			Scene scene = new Scene(loadScreen);
			scene.getStylesheets().add(
					getClass().getResource(
							AppMain.getConfigProperties().getProperty(
									"application.css")).toExternalForm());
			loadScreen.getStylesheets().add(
					getClass().getResource(
							AppMain.getConfigProperties().getProperty(
									"application.css")).toExternalForm());
			modalStage.setTitle(title);
			modalStage.setScene(scene);
			modalStage.setResizable(false);
			modalStage.show();

			ControlledScreen screenControler = ((ControlledScreen) loader
					.getController());
			screenControler.setWorkflowController(this);

			enableProjectToolbar(screenControler
					.getWorkflowController() instanceof ScenaryWorkflow ||
					screenControler
					.getWorkflowController() instanceof ScenaryWorkflowBase
					);

			return screenControler;

		} catch (IOException e) {
			// TODO: manejo excepciones
			System.out.println(e);
			return null;
		}
	}

	/**
	 * Iterates through application modal windows calling Stage close().
	 */
	public void closeCurrentModals() {
		List<Stage> list = StageHelper.getStages();
		for (int i = 0; i < list.size(); i++) {
			Stage stage = list.get(i);
			if (stage.getModality().equals(Modality.WINDOW_MODAL)) {
				stage.close();
			}
		}
	}
	
	/**
	 * Calls the MainController enableProjectToolbar.
	 */
	public void enableProjectToolbar(boolean enable) {
		mainController.enableProjectToolbar(enable);
	}

	/**
	 * Shows a dialog message
	 * 
	 * @param message
	 */
	public void showDialog(String message) {
		// TODO: create a custom dialog component
		final Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.APPLICATION_MODAL);

		Button diagButton = new Button("Ok");
		diagButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				dialogStage.close();
			}
		});

		dialogStage.setScene(new Scene(VBoxBuilder.create()
				.children(new Text(message), diagButton).alignment(Pos.CENTER)
				.padding(new Insets(20)).spacing(10).build()));
		dialogStage.show();
	}
	
	public MainController getMainController() {
		return mainController;
	}

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}
	
	public File showFileChooser(){
		final Stage dialogStage = new Stage();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File file = fileChooser.showOpenDialog(dialogStage);
		return file;
	}
}
