package application.controllers;

/**
 * Interface setting the internal panel (screen) behavior
 * 
 * @author CV
 *
 */
public interface ControlledScreen {

	/**
	 * Sets the workflow controller for the screen.
	 * 
	 * @param workflowController
	 */
	public void setWorkflowController(Workflow workflowController);
	
	/**
	 * Returns the workflow controller instance
	 * @return
	 */
	public Workflow getWorkflowController();
}
