package application.controllers.project;

import application.controllers.ControlledScreen;
import application.controllers.validation.FormValidator;
import application.models.Project;

/**
 * Interface showing projects GUIs behavior.
 * 
 * @author CV
 *
 */
public interface ProjectControlledScreen extends ControlledScreen {

	/**
	 * Method used to set the screen data into the current @Project.
	 * 
	 * @param project The current project
	 */
	public void setScreenData(Project project);

	/**
	 * Method used to show the @Project data in each screen.
	 * 
	 * @param project The current project
	 */
	public void loadScreenData(Project project);

	/**
	 * Validates the form
	 */
	public void isValidForm() throws Exception;
	
	/**
	 * Devuelve validador
	 * @return
	 */
	public FormValidator getValidator();
}
