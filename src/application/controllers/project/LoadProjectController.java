package application.controllers.project;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.models.Project;
import application.utils.CloneHelper;
import application.utils.TableHelper;

/**
 * Class controlling the Load Project GUI
 * 
 * @author CV
 *
 */
public class LoadProjectController implements Initializable, ControlledScreen {

	/** Workflow controller. */
	MainWorkflow workFlowController;

	@FXML
	private TableView<Project> projectsTable;

	@FXML
	private TableColumn<Project, String> projectCol;

	@FXML
	private TableColumn<Project, String> descriptionCol;

	@FXML
	private TableColumn<Project, String> dateCol;

	@FXML
	private TableColumn<Project, String> optionCol;

	@FXML
	private TextField nameText;
	@FXML
	private TextField dateFromText;
	@FXML
	private TextField dateToText;
	@FXML
	private TextField descriptionText;
	@FXML
	private TextField typeText;
	@FXML
	private TextField moduleText;

	ObservableList<Project> data = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	public void initializeTable() {

		List<TableColumn<Project, ?>> cols = new ArrayList<>();

		cols.add(projectCol);
		cols.add(descriptionCol);
		cols.add(dateCol);
		cols.add(optionCol);

		ObservableList<TableColumn<Project, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("name");
		atts.add("description");
		atts.add("date");
		atts.add("option");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(String.class);
		attsTypes.add(String.class);
		attsTypes.add(String.class);

		TableHelper<Project> helper = new TableHelper<Project>(projectsTable,
				colsList, atts, attsTypes);

		data.addAll(new ArrayList<Project>(Project.findAll()));
		projectsTable.setItems(data);
	}

	/**
	 * Creates a new project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void newProjectButtonAction(MouseEvent ev) {
		workFlowController.instanceNewProject();
	}

	/**
	 * Load an existing project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void loadProjectButtonAction(MouseEvent ev) {
		Project selectedProject = projectsTable.getSelectionModel()
				.getSelectedItem();
		if (selectedProject != null) {
			workFlowController.loadProject(selectedProject);
		}
	}

	/**
	 * Load an existing project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void copyProjectButtonAction(MouseEvent ev) {
		Project selectedProject = projectsTable.getSelectionModel()
				.getSelectedItem();
		if (selectedProject != null) {
			Project newProject = null;
			try {
				newProject = CloneHelper.clone(selectedProject, null);
				newProject.setName("Copy of " + newProject.getName());
				data.add(newProject);
			} catch (Exception e) {
				// TODO: delete fail register
				workFlowController.showDialog("The object wasnt cloned");
				e.printStackTrace();
			}

		}
	}

	/**
	 * Load an existing project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteProjectButtonAction(MouseEvent ev) {
		Project selectedProject = projectsTable.getSelectionModel()
				.getSelectedItem();
		if (selectedProject != null) {

			data.remove(selectedProject);
			selectedProject.deleteCascade();

		}
	}

	/**
	 * Load an existing project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void searchButtonAction(MouseEvent ev) {
		data.clear();

		String searchFields = "";

		if (nameText.getText() != null && !nameText.getText().equals("")) {
			searchFields += "name like '%" + nameText.getText() + "%' and ";
		}
		if (dateFromText.getText() != null
				&& !dateFromText.getText().equals("")) {
			searchFields += "date >= '" + dateFromText.getText() + "' and ";
		}
		if (dateToText.getText() != null && !dateToText.getText().equals("")) {
			searchFields += "date <= '" + dateToText.getText() + "' and ";
		}
		if (descriptionText.getText() != null
				&& !descriptionText.getText().equals("")) {
			searchFields += "description like '%" + descriptionText.getText() + "%' and ";
		}

		if (searchFields.equals("")) {
			data.addAll(new ArrayList<Project>(Project.findAll()));
		} else {
			// TODO: its magic
			searchFields = searchFields.substring(0, searchFields.length() - 5);
			data.addAll(new ArrayList<Project>(Project.where(searchFields)));
		}
		projectsTable.setItems(data);
	}

	/**
	 * Load an existing project, starting the project workflow
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		nameText.setText("");
		dateFromText.setText("");
		dateToText.setText("");
		descriptionText.setText("");
		moduleText.setText("");
		typeText.setText("");
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workFlowController = (MainWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workFlowController;
	}

}
