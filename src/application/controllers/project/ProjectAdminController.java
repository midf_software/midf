package application.controllers.project;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.controllers.validation.FormValidator;
import application.models.Project;
import application.models.Scenario;
import application.utils.CloneHelper;
import application.utils.TableHelper;

/**
 * @Project general data GUI controller.
 * 
 * @author CV
 *
 */
public class ProjectAdminController implements Initializable,
		ProjectControlledScreen {

	/** Workflow Controller. */
	ProjectWorkflow workflowController;

	@FXML
	private Label nameLabel;

	@FXML
	private Label wellLabel;

	@FXML
	private Label dateLabel;

	@FXML
	private Label descriptionLabel;

	@FXML
	private TableView<Scenario> scenaryTable;

	@FXML
	private TableColumn<Scenario, String> scenaryColumn;

	@FXML
	private TableColumn<Scenario, String> typeColumn;

	@FXML
	private TableColumn<Scenario, String> dateColumn;

	private ObservableList<Scenario> tableData;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	public void initializeTable() {
		List<TableColumn<Scenario, ?>> cols = new ArrayList<>();

		cols.add(scenaryColumn);
		cols.add(typeColumn);
		cols.add(dateColumn);

		ObservableList<TableColumn<Scenario, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("name");
		atts.add("type");
		atts.add("date");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(String.class);
		attsTypes.add(String.class);

		TableHelper<Scenario> helper = new TableHelper<Scenario>(scenaryTable,
				colsList, atts, attsTypes);

		tableData = FXCollections.observableArrayList(workflowController
				.getProject().getScenarios());

		scenaryTable.setItems(tableData);
	}

	/**
	 * delete button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void editProjectButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"project.general.data.view"), AppMain
						.getLabelsProperties()
						.getProperty("general.data.title"), true, false, false);
	}

	/**
	 * delete button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteButtonAction(MouseEvent ev) {
		Scenario selected = scenaryTable.getSelectionModel().getSelectedItem();
		if (selected != null) {
			//tableData.remove(selected);
			//selected.deleteCascade();
		}
	}

	/**
	 * delete button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void copyButtonAction(MouseEvent ev) {
		Scenario selected = scenaryTable.getSelectionModel().getSelectedItem();
		if (selected != null) {
			Scenario newScenario = null;
			try {
				newScenario = CloneHelper.clone(selected,
						workflowController.getProject());
				newScenario.setName("Copy of " + newScenario.getName());
				tableData.add(newScenario);
			} catch (Exception e) {
				// TODO: delete fail register
				workflowController.showDialog("The object wasnt cloned");
				e.printStackTrace();
			}
		}
	}

	/**
	 * new button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"scenario.select.view"), AppMain.getLabelsProperties()
						.getProperty("scenario.select.title"), true, false,
				false);
	}

	/**
	 * Load button action. Starts an scenario workflow with the loaded scnario.
	 * 
	 * @param ev
	 */
	@FXML
	protected void loadButtonAction(MouseEvent ev) {
		Scenario currentScenario = scenaryTable.getSelectionModel()
				.getSelectedItem();
		if (currentScenario != null) {
			workflowController.getProject().setCurrent(currentScenario);
			ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
					workflowController.getMainController(),
					workflowController.getProject(), currentScenario);
			workflowController.setScenaryWorkflow(scenaryWorkflow);
			scenaryWorkflow.initScenarioWorkflow();
		}
	}

	/**
	 * Validates the form
	 */
	@Override
	public void isValidForm() {

	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {

	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
		if (project != null) {
			nameLabel.setText(project.getName());
			wellLabel.setText("WELL-M3-100A");
			// dateLabel.setText(DateUtils.dateToString(project.getDate()));
			dateLabel.setText(project.getDate());
			descriptionLabel.setText(project.getDescription());
		}
	}

	/**
	 * Sets the workflfow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ProjectWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return null;
	}

}
