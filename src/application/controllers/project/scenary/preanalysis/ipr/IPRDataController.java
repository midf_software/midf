package application.controllers.project.scenary.preanalysis.ipr;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryControlledScreen;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.models.IPR;
import application.models.Project;
import application.models.Scenario;
import application.utils.FieldHelper;

public class IPRDataController implements Initializable, ScenaryControlledScreen {
	 @FXML    
     private ListView<String> lv_model;
     private final ObservableList<String> myListViewData = FXCollections.observableArrayList();
     String selectedModel = "";
     
     @FXML
     private TextField tf_prText;
     @FXML
     private TextField tf_pbText;
     @FXML
     private TextField tf_pwfText;
     @FXML
     private TextField tf_qText;
     @FXML
     private TextField tf_fText;
     
    private FormValidator fv;
	private IPRWorkflow workflowController;
	
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (IPRWorkflow) workflowController; 
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public void setScreenData(Project project) {
		// TODO Auto-generated method stub
		Scenario sc = project.getCurrent();
		IPR iprModel;
		if (sc.getModule() == null) {
			iprModel = new IPR();
		}
		else {
			iprModel = (IPR) sc.getModule();
		}
		
		iprModel.setPb(FieldHelper.getDouble(this.tf_pbText.getText()));
		iprModel.setPr(FieldHelper.getDouble(this.tf_prText.getText()));
		iprModel.setPwfPF(FieldHelper.getDouble(this.tf_pwfText.getText()));
		iprModel.setF2(FieldHelper.getDouble(this.tf_fText.getText()));
		iprModel.setQ(FieldHelper.getDouble(this.tf_qText.getText()));
		
		sc.setModule(iprModel);
	}

	@Override
	public void loadScreenData(Project project) {
		// TODO Auto-generated method stub
		Scenario sc = project.getCurrent();
		IPR iprModel; 
		if (sc.getModule() == null) {
			return;
		}
		
		else {
			iprModel = (IPR) sc.getModule();
		}
		
		this.tf_pbText.setText(FieldHelper.getText(iprModel.getPb()));
		this.tf_prText.setText(FieldHelper.getText(iprModel.getPr()));
		this.tf_pwfText.setText(FieldHelper.getText(iprModel.getPwfPF()));
		this.tf_qText.setText(FieldHelper.getText(iprModel.getQ()));
		this.tf_fText.setText(FieldHelper.getText(iprModel.getF2()));
		
		sc.setModule(iprModel);
	}

	@Override
	public void isValidForm() throws Exception {
		// TODO Auto-generated method stub
		fv.validate();
				}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return fv;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		  myListViewData.add("Vogel");
	      // myListViewData.add("Sukarno"); //TODO implementar
	      lv_model.setItems(myListViewData);
	      lv_model.getSelectionModel().select(0);
	      
	  	fv = new FormValidator(
	  			new ValidationClause("Pr", this.tf_prText, new DecimalNumberRule()),
	  			new ValidationClause("Pb", this.tf_pbText, new DecimalNumberRule()),
	  			new ValidationClause("Q", this.tf_qText, new DecimalNumberRule()),
	  			new ValidationClause("F2", this.tf_fText, new DecimalNumberRule()),
	  			new ValidationClause("Pwf", this.tf_pwfText, new DecimalNumberRule())
	  			);
	}

	
	/**
	 * Calls the next form, information is saved on setScreenData method
	 * 
	 * @param ev
	 */
	@FXML
	protected void saveButtonAction(MouseEvent ev) {
		workflowController
		.setModal(
				AppMain.getConfigProperties().getProperty(
						"ipr.view"),
				AppMain.getLabelsProperties().getProperty(
						"ipr.title"));
	}

	/**
	 * Cancel button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cancelButtonAction(MouseEvent ev) {
	
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		
	}

}
