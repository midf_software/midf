/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.controllers.project.scenary.preanalysis.multiparametric;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryControlledScreen;
import application.controllers.validation.FormValidator;
import application.models.IPR;
import application.models.Multiparametric;
import application.models.Project;
import application.models.Scenario;
import application.utils.FieldHelper;
import application.utils.PlatformHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author MIDF
 */
public class DatosNuevoMPController implements Initializable, ScenaryControlledScreen {

     private MultiparametricWorkflow workflowController;
     @FXML
     private ComboBox<String> cb_type_well;
     private final ObservableList<String> myComboBoxData = FXCollections.observableArrayList();
     String selectedTypeWell = "Oil";
     
  
     @FXML
     private TextField tf_TD;
     @FXML
     private TextField tf_rw;
     @FXML
     private TextField tf_re;
     @FXML
     private TextField tf_top;
     @FXML
     private TextField tf_netpay;   
     @FXML
     private TextField tf_porosity;
     @FXML
     private TextField tf_permeability;
     @FXML
     private TextField tf_kc;
     @FXML
     private TextField tf_reservoir_pressure;
     @FXML
     private TextField tf_saturation_pressure;
     @FXML
     private TextField tf_pc_mineral_scales;
     @FXML
     private TextField tf_pc_organic_scales;
     
     @FXML
     private TextField tf_qo;
     @FXML
     private TextField tf_qg;
     @FXML
     private TextField tf_bhp;
     @FXML
     private TextField tf_uo;
     @FXML
     private TextField tf_ug;
     @FXML
     private TextField tf_bo;
     @FXML
     private TextField tf_bg;
     @FXML
     private TextField tf_mineral_scales;
     @FXML
     private TextField tf_fines_blockage;
     @FXML
     private TextField tf_organic_scales;
     @FXML
     private TextField tf_relative_permeability;
     @FXML
     private TextField tf_induced_damage;
     @FXML
     private TextField tf_geomechanical_damage;
     @FXML
     private TextField tf_msp1_p10;
     @FXML
     private TextField tf_msp1_p90;
     @FXML
     private TextField tf_msp1_valor;
     @FXML
     private TextField tf_fbp1_p10;
     @FXML
     private TextField tf_fbp1_p90;
     @FXML
     private TextField tf_fbp1_valor;
     @FXML
     private TextField tf_osp1_p10;
     @FXML
     private TextField tf_osp1_p90;
     @FXML
     private TextField tf_osp1_valor;
     @FXML
     private TextField tf_krp1_p10;
     @FXML
     private TextField tf_krp1_p90;
     @FXML
     private TextField tf_krp1_valor;
     @FXML
     private TextField tf_idp1_p10;
     @FXML
     private TextField tf_idp1_p90;
     @FXML
     private TextField tf_idp1_valor;
     @FXML
     private TextField tf_gdp1_p10;
     @FXML
     private TextField tf_gdp1_p90;
     @FXML
     private TextField tf_gdp1_valor;
     @FXML
     private TextField tf_msp2_p10;
     @FXML
     private TextField tf_msp2_p90;
     @FXML
     private TextField tf_msp2_valor;
     @FXML
     private TextField tf_fbp2_p10;
     @FXML
     private TextField tf_fbp2_p90;
     @FXML
     private TextField tf_fbp2_valor;
     @FXML
     private TextField tf_osp2_p10;
     @FXML
     private TextField tf_osp2_p90;
     @FXML
     private TextField tf_osp2_valor;
     @FXML
     private TextField tf_krp2_p10;
     @FXML
     private TextField tf_krp2_p90;
     @FXML
     private TextField tf_krp2_valor;
     @FXML
     private TextField tf_idp2_p10;
     @FXML
     private TextField tf_idp2_p90;
     @FXML
     private TextField tf_idp2_valor;
     @FXML
     private TextField tf_gdp2_p10;
     @FXML
     private TextField tf_gdp2_p90;
     @FXML
     private TextField tf_gdp2_valor;
     
     @FXML
     private TextField tf_msp3_p10;
     @FXML
     private TextField tf_msp3_p90;
     @FXML
     private TextField tf_msp3_valor;
     @FXML
     private TextField tf_fbp3_p10;
     @FXML
     private TextField tf_fbp3_p90;
     @FXML
     private TextField tf_fbp3_valor;
     @FXML
     private TextField tf_osp3_p10;
     @FXML
     private TextField tf_osp3_p90;
     @FXML
     private TextField tf_osp3_valor;
     @FXML
     private TextField tf_krp3_p10;
     @FXML
     private TextField tf_krp3_p90;
     @FXML
     private TextField tf_krp3_valor;
     @FXML
     private TextField tf_idp3_p10;
     @FXML
     private TextField tf_idp3_p90;
     @FXML
     private TextField tf_idp3_valor;
     @FXML
     private TextField tf_gdp3_p10;
     @FXML
     private TextField tf_gdp3_p90;
     @FXML
     private TextField tf_gdp3_valor;

     @FXML
     private TextField tf_msp4_p10;
     @FXML
     private TextField tf_msp4_p90;
     @FXML
     private TextField tf_msp4_valor;
     @FXML
     private TextField tf_fbp4_p10;
     @FXML
     private TextField tf_fbp4_p90;
     @FXML
     private TextField tf_fbp4_valor;
     @FXML
     private TextField tf_osp4_p10;
     @FXML
     private TextField tf_osp4_p90;
     @FXML
     private TextField tf_osp4_valor;
     @FXML
     private TextField tf_krp4_p10;
     @FXML
     private TextField tf_krp4_p90;
     @FXML
     private TextField tf_krp4_valor;
     @FXML
     private TextField tf_idp4_p10;
     @FXML
     private TextField tf_idp4_p90;
     @FXML
     private TextField tf_idp4_valor;
     @FXML
     private TextField tf_gdp4_p10;
     @FXML
     private TextField tf_gdp4_p90;
     @FXML
     private TextField tf_gdp4_valor;
     
     @FXML
     private TextField tf_msp5_p10;
     @FXML
     private TextField tf_msp5_p90;
     @FXML
     private TextField tf_msp5_valor;
     @FXML
     private TextField tf_fbp5_p10;
     @FXML
     private TextField tf_fbp5_p90;
     @FXML
     private TextField tf_fbp5_valor;
     @FXML
     private TextField tf_osp5_p10;
     @FXML
     private TextField tf_osp5_p90;
     @FXML
     private TextField tf_osp5_valor;
     @FXML
     private TextField tf_krp5_p10;
     @FXML
     private TextField tf_krp5_p90;
     @FXML
     private TextField tf_krp5_valor;
     
     private Multiparametric mp;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        myComboBoxData.add("Oil");
        myComboBoxData.add("Gas");
                        
        cb_type_well.setItems(myComboBoxData);      
        cb_type_well.setValue("Oil");        
        // Handle ComboBox event.
        cb_type_well.setOnAction((event) -> {
            selectedTypeWell = cb_type_well.getSelectionModel().getSelectedItem();
            System.out.println(selectedTypeWell);
        });
        
       // dp_date.setValue(LocalDate.now());      
    }
    @FXML
    private void handleButtonAction(ActionEvent event) {
        
        //Datos Generales
        mp.setType_well(selectedTypeWell);
        mp.setTD(Double.parseDouble(tf_TD.getText()));
        mp.setRw(Double.parseDouble(tf_rw.getText()));
        mp.setRe(Double.parseDouble(tf_re.getText()));
        
        //Datos yacimiento
        mp.setTop(Double.parseDouble(tf_top.getText()));
        mp.setNetpay(Double.parseDouble(tf_netpay.getText()));
        mp.setPorosity(Double.parseDouble(tf_porosity.getText()));
        mp.setPermeability(Double.parseDouble(tf_permeability.getText()));
        mp.setKc(Double.parseDouble(tf_kc.getText()));
        mp.setReservoir_pressure(Double.parseDouble(tf_reservoir_pressure.getText()));
        mp.setSaturation_pressure(Double.parseDouble(tf_saturation_pressure.getText()));
        mp.setPc_mineral_scales(Double.parseDouble(tf_pc_mineral_scales.getText()));
        mp.setPc_organic_scales(Double.parseDouble(tf_pc_organic_scales.getText()));       
        
        //Datos producción
        mp.setQo(Double.parseDouble(tf_qo.getText()));
        mp.setQg(Double.parseDouble(tf_qg.getText()));
        mp.setBhp(Double.parseDouble(tf_bhp.getText()));
        
        //Datos PVT
        mp.setUo(Double.parseDouble(tf_uo.getText()));
        mp.setUg(Double.parseDouble(tf_ug.getText()));
        mp.setBo(Double.parseDouble(tf_bo.getText()));
        mp.setBg(Double.parseDouble(tf_bg.getText()));
        
        //Datos kd/ki
        mp.setMineral_scales(Double.parseDouble(tf_mineral_scales.getText()));
        mp.setFines_blockage(Double.parseDouble(tf_fines_blockage.getText()));
        mp.setOrganic_scales(Double.parseDouble(tf_organic_scales.getText()));
        mp.setRelative_permeability(Double.parseDouble(tf_relative_permeability.getText()));
        mp.setInduced_damage(Double.parseDouble(tf_induced_damage.getText()));
        mp.setGeomechanical_damage(Double.parseDouble(tf_geomechanical_damage.getText()));
        
        //Datos percentil
        //Parte 1
        mp.setMsp1_p10(Double.parseDouble(tf_msp1_p10.getText()));
        mp.setMsp1_p90(Double.parseDouble(tf_msp1_p90.getText()));
        mp.setMsp1_valor(Double.parseDouble(tf_msp1_valor.getText()));
        mp.setFbp1_p10(Double.parseDouble(tf_fbp1_p10.getText()));
        mp.setFbp1_p90(Double.parseDouble(tf_fbp1_p90.getText()));
        mp.setFbp1_valor(Double.parseDouble(tf_fbp1_valor.getText()));
        mp.setOsp1_p10(Double.parseDouble(tf_osp1_p10.getText()));
        mp.setOsp1_p90(Double.parseDouble(tf_osp1_p90.getText()));
        mp.setOsp1_valor(Double.parseDouble(tf_osp1_valor.getText()));
        mp.setKrp1_p10(Double.parseDouble(tf_krp1_p10.getText()));
        mp.setKrp1_p90(Double.parseDouble(tf_krp1_p90.getText()));
        mp.setKrp1_valor(Double.parseDouble(tf_krp1_valor.getText()));
        mp.setIdp1_p10(Double.parseDouble(tf_idp1_p10.getText()));
        mp.setIdp1_p90(Double.parseDouble(tf_idp1_p90.getText()));
        mp.setIdp1_valor(Double.parseDouble(tf_idp1_valor.getText()));
        mp.setGdp1_p10(Double.parseDouble(tf_gdp1_p10.getText()));
        mp.setGdp1_p90(Double.parseDouble(tf_gdp1_p90.getText()));
        mp.setGdp1_valor(Double.parseDouble(tf_gdp1_valor.getText()));
        
        //Parte 2
        mp.setMsp2_p10(Double.parseDouble(tf_msp2_p10.getText()));
        mp.setMsp2_p90(Double.parseDouble(tf_msp2_p90.getText()));
        mp.setMsp2_valor(Double.parseDouble(tf_msp2_valor.getText()));
        mp.setFbp2_p10(Double.parseDouble(tf_fbp2_p10.getText()));
        mp.setFbp2_p90(Double.parseDouble(tf_fbp2_p90.getText()));
        mp.setFbp2_valor(Double.parseDouble(tf_fbp2_valor.getText()));
        mp.setOsp2_p10(Double.parseDouble(tf_osp2_p10.getText()));
        mp.setOsp2_p90(Double.parseDouble(tf_osp2_p90.getText()));
        mp.setOsp2_valor(Double.parseDouble(tf_osp2_valor.getText()));
        mp.setKrp2_p10(Double.parseDouble(tf_krp2_p10.getText()));
        mp.setKrp2_p90(Double.parseDouble(tf_krp2_p90.getText()));
        mp.setKrp2_valor(Double.parseDouble(tf_krp2_valor.getText()));
        mp.setIdp2_p10(Double.parseDouble(tf_idp2_p10.getText()));
        mp.setIdp2_p90(Double.parseDouble(tf_idp2_p90.getText()));
        mp.setIdp2_valor(Double.parseDouble(tf_idp2_valor.getText()));
        mp.setGdp2_p10(Double.parseDouble(tf_gdp2_p10.getText()));
        mp.setGdp2_p90(Double.parseDouble(tf_gdp2_p90.getText()));
        mp.setGdp2_valor(Double.parseDouble(tf_gdp2_valor.getText()));

        //Parte 3
        mp.setMsp3_p10(Double.parseDouble(tf_msp3_p10.getText()));
        mp.setMsp3_p90(Double.parseDouble(tf_msp3_p90.getText()));
        mp.setMsp3_valor(Double.parseDouble(tf_msp3_valor.getText()));
        mp.setFbp3_p10(Double.parseDouble(tf_fbp3_p10.getText()));
        mp.setFbp3_p90(Double.parseDouble(tf_fbp3_p90.getText()));
        mp.setFbp3_valor(Double.parseDouble(tf_fbp3_valor.getText()));
        mp.setOsp3_p10(Double.parseDouble(tf_osp3_p10.getText()));
        mp.setOsp3_p90(Double.parseDouble(tf_osp3_p90.getText()));
        mp.setOsp3_valor(Double.parseDouble(tf_osp3_valor.getText()));
        mp.setKrp3_p10(Double.parseDouble(tf_krp3_p10.getText()));
        mp.setKrp3_p90(Double.parseDouble(tf_krp3_p90.getText()));
        mp.setKrp3_valor(Double.parseDouble(tf_krp3_valor.getText()));
        mp.setIdp3_p10(Double.parseDouble(tf_idp3_p10.getText()));
        mp.setIdp3_p90(Double.parseDouble(tf_idp3_p90.getText()));
        mp.setIdp3_valor(Double.parseDouble(tf_idp3_valor.getText()));
        mp.setGdp3_p10(Double.parseDouble(tf_gdp3_p10.getText()));
        mp.setGdp3_p90(Double.parseDouble(tf_gdp3_p90.getText()));
        mp.setGdp3_valor(Double.parseDouble(tf_gdp3_valor.getText()));
        
        //Parte 4
        mp.setMsp4_p10(Double.parseDouble(tf_msp4_p10.getText()));
        mp.setMsp4_p90(Double.parseDouble(tf_msp4_p90.getText()));
        mp.setMsp4_valor(Double.parseDouble(tf_msp4_valor.getText()));
        mp.setFbp4_p10(Double.parseDouble(tf_fbp4_p10.getText()));
        mp.setFbp4_p90(Double.parseDouble(tf_fbp4_p90.getText()));
        mp.setFbp4_valor(Double.parseDouble(tf_fbp4_valor.getText()));
        mp.setOsp4_p10(Double.parseDouble(tf_osp4_p10.getText()));
        mp.setOsp4_p90(Double.parseDouble(tf_osp4_p90.getText()));
        mp.setOsp4_valor(Double.parseDouble(tf_osp4_valor.getText()));
        mp.setKrp4_p10(Double.parseDouble(tf_krp4_p10.getText()));
        mp.setKrp4_p90(Double.parseDouble(tf_krp4_p90.getText()));
        mp.setKrp4_valor(Double.parseDouble(tf_krp4_valor.getText()));
        mp.setIdp4_p10(Double.parseDouble(tf_idp4_p10.getText()));
        mp.setIdp4_p90(Double.parseDouble(tf_idp4_p90.getText()));
        mp.setIdp4_valor(Double.parseDouble(tf_idp4_valor.getText()));
        mp.setGdp4_p10(Double.parseDouble(tf_gdp4_p10.getText()));
        mp.setGdp4_p90(Double.parseDouble(tf_gdp4_p90.getText()));
        mp.setGdp4_valor(Double.parseDouble(tf_gdp4_valor.getText()));
        
        //Parte 5
        mp.setMsp5_p10(Double.parseDouble(tf_msp5_p10.getText()));
        mp.setMsp5_p90(Double.parseDouble(tf_msp5_p90.getText()));
        mp.setMsp5_valor(Double.parseDouble(tf_msp5_valor.getText()));
        mp.setFbp5_p10(Double.parseDouble(tf_fbp5_p10.getText()));
        mp.setFbp5_p90(Double.parseDouble(tf_fbp5_p90.getText()));
        mp.setFbp5_valor(Double.parseDouble(tf_fbp5_valor.getText()));
        mp.setOsp5_p10(Double.parseDouble(tf_osp5_p10.getText()));
        mp.setOsp5_p90(Double.parseDouble(tf_osp5_p90.getText()));
        mp.setOsp5_valor(Double.parseDouble(tf_osp5_valor.getText()));
        mp.setKrp5_p10(Double.parseDouble(tf_krp5_p10.getText()));
        mp.setKrp5_p90(Double.parseDouble(tf_krp5_p90.getText()));
        mp.setKrp5_valor(Double.parseDouble(tf_krp5_valor.getText()));
        
        mp.statistical_mineral_scales();
        mp.statistical_fines_blockage();
        mp.statistical_organic_scales();
        mp.statistical_relative_permeability();
        mp.statistical_induced_damage();
        mp.statistical_geomechanical_damage();
        mp.statistical_averages();
        
        mp.skin_calculations();
        mp.skin_averages();
        mp.avegares();
        /*
        PlatformHelper.run(() -> {
        mp.spiderchart(swingNode1, tab_SkinDiagram);
        mp.spiderchart_parameter1(swingNode2, tab_SkinParameters);
        mp.spiderchart_parameter2(swingNode3, tab_SkinParameters);
        mp.spiderchart_parameter3(swingNode4, tab_SkinParameters);
        mp.spiderchart_parameter4(swingNode5, tab_SkinParameters);
        mp.spiderchart_parameter5(swingNode6, tab_SkinParameters);
        mp.spiderchart_parameter6(swingNode7, tab_SkinParameters);});*/
        
    }

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (MultiparametricWorkflow) workflowController; 
	}

	@Override
	public Workflow getWorkflowController() {
		return  this.workflowController;
	}

	@Override
	public void setScreenData(Project project) {
		// TODO Auto-generated method stub
		handleButtonAction(null);
		project.getCurrent().setModule(this.mp);
	}

	@Override
	public void loadScreenData(Project project) {
		
		Scenario sc = project.getCurrent();
		Multiparametric mp; 
		if (sc.getModule() == null) {
			return;
		}
		
		else {
			mp = (Multiparametric) sc.getModule();
			//Set fields according to mp data
			tf_TD.setText(FieldHelper.getText(mp.getTD()));
			tf_rw.setText(FieldHelper.getText(mp.getRw()));
			tf_re.setText(FieldHelper.getText(mp.getRe()));
			tf_top.setText(FieldHelper.getText(mp.getTop()));
			tf_netpay.setText(FieldHelper.getText(mp.getNetpay()));
			tf_porosity.setText(FieldHelper.getText(mp.getPorosity()));
			tf_permeability.setText(FieldHelper.getText(mp.getPermeability()));
			tf_kc.setText(FieldHelper.getText(mp.getKc()));
			tf_reservoir_pressure.setText(FieldHelper.getText(mp.getReservoir_pressure()));
			tf_saturation_pressure.setText(FieldHelper.getText(mp.getSaturation_pressure()));
			tf_pc_mineral_scales.setText(FieldHelper.getText(mp.getPc_mineral_scales()));
			tf_pc_organic_scales.setText(FieldHelper.getText(mp.getPc_organic_scales()));
			tf_qo.setText(FieldHelper.getText(mp.getQo()));
			tf_qg.setText(FieldHelper.getText(mp.getQg()));
			tf_bhp.setText(FieldHelper.getText(mp.getBhp()));
			tf_uo.setText(FieldHelper.getText(mp.getUo()));
			tf_ug.setText(FieldHelper.getText(mp.getUg()));
			tf_bo.setText(FieldHelper.getText(mp.getBo()));
			tf_bg.setText(FieldHelper.getText(mp.getBg()));
			tf_mineral_scales.setText(FieldHelper.getText(mp.getMineral_scales()));
			tf_fines_blockage.setText(FieldHelper.getText(mp.getFines_blockage()));
			tf_organic_scales.setText(FieldHelper.getText(mp.getOrganic_scales()));
			tf_relative_permeability.setText(FieldHelper.getText(mp.getRelative_permeability()));
			tf_induced_damage.setText(FieldHelper.getText(mp.getInduced_damage()));
			tf_geomechanical_damage.setText(FieldHelper.getText(mp.getGeomechanical_damage()));
			tf_msp1_p10.setText(FieldHelper.getText(mp.getMsp1_p10()));
			tf_msp1_p90.setText(FieldHelper.getText(mp.getMsp1_p90()));
			tf_msp1_valor.setText(FieldHelper.getText(mp.getMsp1_valor()));
			tf_fbp1_p10.setText(FieldHelper.getText(mp.getFbp1_p10()));
			tf_fbp1_p90.setText(FieldHelper.getText(mp.getFbp1_p90()));
			tf_fbp1_valor.setText(FieldHelper.getText(mp.getFbp1_valor()));
			tf_osp1_p10.setText(FieldHelper.getText(mp.getOsp1_p10()));
			tf_osp1_p90.setText(FieldHelper.getText(mp.getOsp1_p90()));
			tf_osp1_valor.setText(FieldHelper.getText(mp.getOsp1_valor()));
			tf_krp1_p10.setText(FieldHelper.getText(mp.getKrp1_p10()));
			tf_krp1_p90.setText(FieldHelper.getText(mp.getKrp1_p90()));
			tf_krp1_valor.setText(FieldHelper.getText(mp.getKrp1_valor()));
			tf_idp1_p10.setText(FieldHelper.getText(mp.getIdp1_p10()));
			tf_idp1_p90.setText(FieldHelper.getText(mp.getIdp1_p90()));
			tf_idp1_valor.setText(FieldHelper.getText(mp.getIdp1_valor()));
			tf_gdp1_p10.setText(FieldHelper.getText(mp.getGdp1_p10()));
			tf_gdp1_p90.setText(FieldHelper.getText(mp.getGdp1_p90()));
			tf_gdp1_valor.setText(FieldHelper.getText(mp.getGdp1_valor()));
			tf_msp2_p10.setText(FieldHelper.getText(mp.getMsp2_p10()));
			tf_msp2_p90.setText(FieldHelper.getText(mp.getMsp2_p90()));
			tf_msp2_valor.setText(FieldHelper.getText(mp.getMsp2_valor()));
			tf_fbp2_p10.setText(FieldHelper.getText(mp.getFbp2_p10()));
			tf_fbp2_p90.setText(FieldHelper.getText(mp.getFbp2_p90()));
			tf_fbp2_valor.setText(FieldHelper.getText(mp.getFbp2_valor()));
			tf_osp2_p10.setText(FieldHelper.getText(mp.getOsp2_p10()));
			tf_osp2_p90.setText(FieldHelper.getText(mp.getOsp2_p90()));
			tf_osp2_valor.setText(FieldHelper.getText(mp.getOsp2_valor()));
			tf_krp2_p10.setText(FieldHelper.getText(mp.getKrp2_p10()));
			tf_krp2_p90.setText(FieldHelper.getText(mp.getKrp2_p90()));
			tf_krp2_valor.setText(FieldHelper.getText(mp.getKrp2_valor()));
			tf_idp2_p10.setText(FieldHelper.getText(mp.getIdp2_p10()));
			tf_idp2_p90.setText(FieldHelper.getText(mp.getIdp2_p90()));
			tf_idp2_valor.setText(FieldHelper.getText(mp.getIdp2_valor()));
			tf_gdp2_p10.setText(FieldHelper.getText(mp.getGdp2_p10()));
			tf_gdp2_p90.setText(FieldHelper.getText(mp.getGdp2_p90()));
			tf_gdp2_valor.setText(FieldHelper.getText(mp.getGdp2_valor()));
			tf_msp3_p10.setText(FieldHelper.getText(mp.getMsp3_p10()));
			tf_msp3_p90.setText(FieldHelper.getText(mp.getMsp3_p90()));
			tf_msp3_valor.setText(FieldHelper.getText(mp.getMsp3_valor()));
			tf_fbp3_p10.setText(FieldHelper.getText(mp.getFbp3_p10()));
			tf_fbp3_p90.setText(FieldHelper.getText(mp.getFbp3_p90()));
			tf_fbp3_valor.setText(FieldHelper.getText(mp.getFbp3_valor()));
			tf_osp3_p10.setText(FieldHelper.getText(mp.getOsp3_p10()));
			tf_osp3_p90.setText(FieldHelper.getText(mp.getOsp3_p90()));
			tf_osp3_valor.setText(FieldHelper.getText(mp.getOsp3_valor()));
			tf_krp3_p10.setText(FieldHelper.getText(mp.getKrp3_p10()));
			tf_krp3_p90.setText(FieldHelper.getText(mp.getKrp3_p90()));
			tf_krp3_valor.setText(FieldHelper.getText(mp.getKrp3_valor()));
			tf_idp3_p10.setText(FieldHelper.getText(mp.getIdp3_p10()));
			tf_idp3_p90.setText(FieldHelper.getText(mp.getIdp3_p90()));
			tf_idp3_valor.setText(FieldHelper.getText(mp.getIdp3_valor()));
			tf_gdp3_p10.setText(FieldHelper.getText(mp.getGdp3_p10()));
			tf_gdp3_p90.setText(FieldHelper.getText(mp.getGdp3_p90()));
			tf_gdp3_valor.setText(FieldHelper.getText(mp.getGdp3_valor()));
			tf_msp4_p10.setText(FieldHelper.getText(mp.getMsp4_p10()));
			tf_msp4_p90.setText(FieldHelper.getText(mp.getMsp4_p90()));
			tf_msp4_valor.setText(FieldHelper.getText(mp.getMsp4_valor()));
			tf_fbp4_p10.setText(FieldHelper.getText(mp.getFbp4_p10()));
			tf_fbp4_p90.setText(FieldHelper.getText(mp.getFbp4_p90()));
			tf_fbp4_valor.setText(FieldHelper.getText(mp.getFbp4_valor()));
			tf_osp4_p10.setText(FieldHelper.getText(mp.getOsp4_p10()));
			tf_osp4_p90.setText(FieldHelper.getText(mp.getOsp4_p90()));
			tf_osp4_valor.setText(FieldHelper.getText(mp.getOsp4_valor()));
			tf_krp4_p10.setText(FieldHelper.getText(mp.getKrp4_p10()));
			tf_krp4_p90.setText(FieldHelper.getText(mp.getKrp4_p90()));
			tf_krp4_valor.setText(FieldHelper.getText(mp.getKrp4_valor()));
			tf_idp4_p10.setText(FieldHelper.getText(mp.getIdp4_p10()));
			tf_idp4_p90.setText(FieldHelper.getText(mp.getIdp4_p90()));
			tf_idp4_valor.setText(FieldHelper.getText(mp.getIdp4_valor()));
			tf_gdp4_p10.setText(FieldHelper.getText(mp.getGdp4_p10()));
			tf_gdp4_p90.setText(FieldHelper.getText(mp.getGdp4_p90()));
			tf_gdp4_valor.setText(FieldHelper.getText(mp.getGdp4_valor()));
			tf_msp5_p10.setText(FieldHelper.getText(mp.getMsp5_p10()));
			tf_msp5_p90.setText(FieldHelper.getText(mp.getMsp5_p90()));
			tf_msp5_valor.setText(FieldHelper.getText(mp.getMsp5_valor()));
			tf_fbp5_p10.setText(FieldHelper.getText(mp.getFbp5_p10()));
			tf_fbp5_p90.setText(FieldHelper.getText(mp.getFbp5_p90()));
			tf_fbp5_valor.setText(FieldHelper.getText(mp.getFbp5_valor()));
			tf_osp5_p10.setText(FieldHelper.getText(mp.getOsp5_p10()));
			tf_osp5_p90.setText(FieldHelper.getText(mp.getOsp5_p90()));
			tf_osp5_valor.setText(FieldHelper.getText(mp.getOsp5_valor()));
			tf_krp5_p10.setText(FieldHelper.getText(mp.getKrp5_p10()));
			tf_krp5_p90.setText(FieldHelper.getText(mp.getKrp5_p90()));
			tf_krp5_valor.setText(FieldHelper.getText(mp.getKrp5_valor()));

		}
		this.mp = mp;
		handleButtonAction(null);
		sc.setModule(mp);
	}

	@Override
	public void isValidForm() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return null;
	}
}
