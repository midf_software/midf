package application.controllers.project.scenary.preanalysis.multiparametric;

import application.controllers.AppMain;
import application.controllers.MainController;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.models.Project;
import application.models.Scenario;

public class MultiparametricWorkflow extends ScenaryWorkflowBase{

	public MultiparametricWorkflow(MainController mainController, Project project, Scenario scenario) {
		super(mainController, project, scenario);
		// TODO Auto-generated constructor stub
		this.inputMenuFXML = AppMain.getConfigProperties().getProperty("multiparametric.menu");
	}

	@Override
	public void initScenarioWorkflow() {
		// No se muestra nada ya que Scenaryworkflow ya mostro GeneralData
		/*setModal(
				AppMain.getConfigProperties().getProperty(
				"multiparametric.view"), AppMain.getLabelsProperties().getProperty(
						"multiparametric.title"));*/
	}

}
