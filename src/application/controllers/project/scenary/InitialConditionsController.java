package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.project.ProjectWorkflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.NoValidationRule;
import application.models.FormationCondition;
import application.models.InitialCondition;
import application.models.GeologicalUnit;
import application.models.Layer;
import application.models.Project;
import application.utils.TableHelper;

/**
 * @Project initial conditions GUI controller.
 * 
 * @author CV
 *
 */
public class InitialConditionsController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	/** List containing the GUI components to ease validations */
	private List<Node> componentsList = new ArrayList<Node>();
	
	@FXML
	private TableView<InitialCondition> propertiesTable;
	
	@FXML
	private TableColumn<InitialCondition, String> guColumn;

	@FXML
	private TableColumn<InitialCondition, Integer> layerColumn;
	
	@FXML
	private TableColumn<InitialCondition, String> blocksColumn;

	@FXML
	private TableColumn<InitialCondition, Double> pressureColumn;

	@FXML
	private TableColumn<InitialCondition, Double> waterSaturationColumn;

	@FXML
	private TableColumn<InitialCondition, Double> gasSaturationColumn;
	
	private ObservableList<InitialCondition> tableData;
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator(
				new ValidationClause("Initial Conditions Table", propertiesTable, 
						new NoValidationRule(),
						new NoValidationRule(),
						new NoValidationRule(),
						new DecimalNumberRule(0, 100000),
						new DecimalNumberRule(0, 1),
						new DecimalNumberRule(0, 1)
						)
				);
	}

	public void initializeTable() {
		List<TableColumn<InitialCondition, ?>> cols = new ArrayList<>();
		
		cols.add(guColumn);
		cols.add(layerColumn);
		cols.add(blocksColumn);
		cols.add(pressureColumn);
		cols.add(waterSaturationColumn);
		cols.add(gasSaturationColumn);

		ObservableList<TableColumn<InitialCondition, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("guName");
		atts.add("layerNumber");
		atts.add("rule");
		atts.add("initialReservoirPressure");
		atts.add("initialWaterSaturation");
		atts.add("initialGasSaturation");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(int.class);
		attsTypes.add(String.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);

		TableHelper<InitialCondition> helper = new TableHelper<InitialCondition>(
				propertiesTable, colsList, atts, attsTypes);

		List<InitialCondition> data = new ArrayList<InitialCondition>();

		for (GeologicalUnit ug : workflowController.getProject().getCurrent()
				.getInputData().getGUList()) {
			for(Layer layer: ug.getLayers()){
			     data.addAll(layer.getInitialConditions());
			     for(InitialCondition ic : layer.getInitialConditions()){
			    	 ic.setGuName(ug.getName());
			    	 ic.setLayerNumber(layer.getLayerNumber());
			     }
			}
		}

		tableData = FXCollections.observableArrayList(data);

		propertiesTable.setItems(tableData);
		int phases = workflowController.getProject()
				.getCurrent().getInputData().getReservoir().getPhases();
		if( phases == 1) {
			waterSaturationColumn.setEditable(false);
			waterSaturationColumn.getStyleClass().add("field-disable");
			waterSaturationColumn.setPrefWidth(0);
			//TODO aplicar CSS a celdas respectivas
		}
		else if (phases == 2) {
			gasSaturationColumn.setEditable(false);
			gasSaturationColumn.getStyleClass().add("field-disable");
			//TODO aplicar CSS a celdas respectivas
			gasSaturationColumn.setPrefWidth(0);
		}
	}
	
	/**
	 * Add new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {

		GeologicalUnit firstGu = workflowController.getProject().getCurrent()
				.getInputData().getGUList().get(0);
		Layer firstLayer = firstGu.getLayers().get(0);

		InitialCondition newIc = new InitialCondition();
		newIc.setGuName(firstGu.getName());
		newIc.setLayerNumber(firstLayer.getLayerNumber());
		
		firstLayer.addInitialCondition(newIc);

		tableData.add(newIc);
	}

	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		InitialCondition selectedic = propertiesTable.getSelectionModel().getSelectedItem();
		if (selectedic != null) {
			tableData.remove(propertiesTable.getSelectionModel()
					.getSelectedIndex());
			selectedic.getLayer().getInitialConditions().remove(selectedic);
			selectedic.delete();
		}
	}
	
	/**
	 * Next button action handler, calls the next form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"pvt.view"),
				AppMain.getLabelsProperties().getProperty(
						"pvt.title"));
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("rock.properties.view"),
				AppMain.getLabelsProperties().getProperty("rock.properties.title"));
	}

	/**
	 * Clean button action handler, cleans the GUI fields
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table
	}

	/**
	 * Validates the form
	 * @throws Exception 
	 */
	@Override
	public void isValidForm() throws Exception {
		fv.validate();
	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {
		for(InitialCondition Ic : tableData){
			Ic.saveIt();
		}
	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
	}

	/**
	 * Sets the workflfow controller.
	 */

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}
	
	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}
}
