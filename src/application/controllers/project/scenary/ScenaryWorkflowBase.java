package application.controllers.project.scenary;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.Workflow;
import application.models.Project;
import application.models.Scenario;

/**
 * Workflow base para input de datos de escenarios. Workflows especificos pueden heredar de aqui.
 * @author jeyson
 *
 */

public abstract class ScenaryWorkflowBase extends Workflow{
	private Project project;
	private ScenaryControlledScreen currentScreenController;
	protected String inputMenuFXML;
	
	public ScenaryWorkflowBase(MainController mainController, Project project, Scenario scenario) {
		super(mainController);
		this.project = project;
		project.setCurrent(scenario); ///TODO innecesario?
	}
	
	public abstract void initScenarioWorkflow(); 
	
	/**
	 * Sets an internal panel (screen) on the workspace, saves and validates the
	 * current form.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setInternalPanel(final String resource) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			// TODO: msg properties + campo errado (manejo excepciones)
			showDialog("NO VALIDA");
			return currentScreenController;
		}

		if (currentScreenController != null) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ScenaryControlledScreen) super
				.setInternalPanel(resource);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}
	
	/**
	 * Shows a modal over the workspace, saves and validates the
	 * current form.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setModal(final String resource, String title) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			// TODO: msg properties + campo errado (manejo excepciones)
			//e.printStackTrace();
			showDialog("Validation errors:\n" + currentScreenController.getValidator().getErrorMessagesStr());
			return currentScreenController;
		}

		if (currentScreenController != null) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ScenaryControlledScreen) super
				.setModal(resource, title, true, true);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}

	/**
	 * Project getter.
	 * 
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}
	
	
	public void createInputDataMenu() {
		
		if(this.inputMenuFXML.equals("")){
			return;
		}
		
		try {
			
			MainController mc = this.getMainController();
			FXMLLoader loader = new FXMLLoader(getClass().getResource(this.inputMenuFXML));
			loader.setController(mc);
			Parent loadScreen = (Parent) loader.load();
			mc.addScenarioMenu(loadScreen);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
