package application.controllers.project.scenary;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.models.BaseData;
import application.models.KrsGasValue;
import application.models.Grid;
import application.models.KrsWaterValue;
import application.models.Project;
import application.models.PvtValue;
import application.utils.FieldHelper;
import application.utils.FileTableParser;
import application.utils.TableHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;

public class KRSController implements Initializable,
ScenaryControlledScreen {
	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<KrsGasValue> gasLiquidTable;

	@FXML
	private TableColumn<KrsGasValue, Double> sgColumn;

	@FXML
	private TableColumn<KrsGasValue, Double> pcogColumn;

	@FXML
	private TableColumn<KrsGasValue, Double> krgColumn;
	
	@FXML
	private TableColumn<KrsGasValue, Double> kroColumn1;
	
	@FXML
	private TableView<KrsWaterValue> waterOilTable;
	
	@FXML
	private TableColumn<KrsWaterValue, Double> swColumn;

	@FXML
	private TableColumn<KrsWaterValue, Double> pcwoColumn;

	@FXML
	private TableColumn<KrsWaterValue, Double> krwColumn;
	
	@FXML
	private TableColumn<KrsWaterValue, Double> kroColumn2;
	
	@FXML
	private Tab gasLiquidTab;
	@FXML
	private Tab waterOilTab;
	@FXML
	private TabPane tabPane;
	
	private ObservableList<KrsGasValue> tableGasData;
	private ObservableList<KrsWaterValue> tableWaterData;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void initializeGasTable() {
		List<TableColumn<KrsGasValue, ?>> cols = new ArrayList<>();

		cols.add(sgColumn);
		cols.add(pcogColumn);
		cols.add(krgColumn);
		cols.add(kroColumn1);
		
		ObservableList<TableColumn<KrsGasValue, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("sg");
		atts.add("pcog");
		atts.add("krg");
		atts.add("kro");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		

		TableHelper<KrsGasValue> helper = new TableHelper<KrsGasValue>(gasLiquidTable,
				colsList, atts, attsTypes);

		tableGasData = FXCollections
				.observableArrayList(workflowController.getProject()
						.getCurrent().getInputData().getKrsGasList());
		gasLiquidTable.setItems(tableGasData);
		
		if(workflowController.getProject()
		.getCurrent().getInputData().getReservoir().getPhases() == 2) {
			this.gasLiquidTab.setDisable(true);
			SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
			selectionModel.select(this.waterOilTab);
		}
	}
	
	public void initializeWaterTable() {
		List<TableColumn<KrsWaterValue, ?>> cols = new ArrayList<>();

		cols.add(swColumn);
		cols.add(pcwoColumn);
		cols.add(krwColumn);
		cols.add(kroColumn2);
		
		ObservableList<TableColumn<KrsWaterValue, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("sw");
		atts.add("pcwo");
		atts.add("krw");
		atts.add("kro");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		

		TableHelper<KrsWaterValue> helper = new TableHelper<KrsWaterValue>(waterOilTable,
				colsList, atts, attsTypes);

		tableWaterData = FXCollections
				.observableArrayList(workflowController.getProject()
						.getCurrent().getInputData().getKrsWaterList());
		waterOilTable.setItems(tableWaterData);
		
		if(workflowController.getProject()
				.getCurrent().getInputData().getReservoir().getPhases() != 2) {
					this.waterOilTab.setDisable(true);
					SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
					selectionModel.select(this.gasLiquidTab);
				}
	}
		
	/**
	 * Add new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {
		// TODO: add
		/*KrsGasValue newGeoUnit = new KrsGasValue();
		newGeoUnit.setName("New GU");
		Grid grid = this.workflowController.getProject().getCurrent().getInputData().getGrid();
		
		newGeoUnit.setGrid(grid); //TODO optimizar doble referencia 
		grid.addGU(newGeoUnit);
		
		tableGasData.add(newGeoUnit);*/
	}
	
	/**
	 * Import
	 * 
	 * @param ev
	 */
	@FXML
	protected void importGasTableAction(MouseEvent ev) {
		File file = this.workflowController.showFileChooser();
		List<double[]>data = FileTableParser.parseFile(file.getAbsolutePath());
		BaseData inputData = this.workflowController.getProject().getCurrent().getInputData();
		
		for (int i=0; i<data.size(); i++){
			double []d= data.get(i);
			KrsGasValue kgas = new KrsGasValue(d[0],d[1],d[2],d[3]);
			inputData.addKrsGasValue(kgas);
			this.tableGasData.add(kgas);
		}
	}
	
	/**
	 * Import
	 * 
	 * @param ev
	 */
	@FXML
	protected void importWaterTableAction(MouseEvent ev) {
		File file = this.workflowController.showFileChooser();
		List<double[]>data = FileTableParser.parseFile(file.getAbsolutePath());
		BaseData inputData = this.workflowController.getProject().getCurrent().getInputData();
		
		for (int i=0; i<data.size(); i++){
			double []d= data.get(i);
			KrsWaterValue kwater = new KrsWaterValue(d[0],d[1],d[2],d[3]);
			inputData.addKrsWaterValue(kwater);
			this.tableWaterData.add(kwater);
		}
	}
	
	
	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// TODO: delete
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("pvt.view"),
				AppMain.getLabelsProperties().getProperty("pvt.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"perforations.view"), AppMain.getLabelsProperties()
						.getProperty("perforations.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table
	}

	@Override
	public void setScreenData(Project project) {
		//
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
	}

	@Override
	public void isValidForm() {
		// TODO Validate
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeGasTable();
		initializeWaterTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return null;
	}
}
