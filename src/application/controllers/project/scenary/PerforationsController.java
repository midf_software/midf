package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.models.BaseData;
import application.models.FormationCondition;
import application.models.GeologicalUnit;
import application.models.InitialCondition;
import application.models.Layer;
import application.models.PerforationCondition;
import application.models.Grid;
import application.models.PerforationCondition;
import application.models.Project;
import application.models.PvtValue;
import application.models.Reservoir;
import application.utils.FieldHelper;
import application.utils.TableHelper;
import application.controllers.simulation.SkinFactorCalculator;
import application.controllers.validation.FormValidator;
/**
 * GU view data GUI controller.
 * 
 * @author CV
 *
 */
public class PerforationsController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<PerforationCondition> perforationTable;

	@FXML
	private TableColumn<PerforationCondition, String> guColumn;

	@FXML
	private TableColumn<PerforationCondition, String> layerColumn;

	@FXML
	private TableColumn<PerforationCondition, String> perforatedColumn;
	

	
	@FXML
	private TextField bhfpField;
	
	@FXML
	private TextField qtField;
	
	@FXML
	private Button skinButton;
		
	private ObservableList<PerforationCondition> tableData;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
 
	}

	public void initializeTable() {
		List<TableColumn<PerforationCondition, ?>> cols = new ArrayList<>();

		cols.add(guColumn);
		cols.add(layerColumn);
		cols.add(perforatedColumn);

		ObservableList<TableColumn<PerforationCondition, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("guName");
		atts.add("layerNumber");
		atts.add("perforated");

		
		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(String.class);
		attsTypes.add(String.class);
		attsTypes.add(double.class);

		TableHelper<PerforationCondition> helper = new TableHelper<PerforationCondition>(perforationTable,
				colsList, atts, attsTypes);
		
		List<PerforationCondition> data = new ArrayList<PerforationCondition>();

		for (GeologicalUnit ug : workflowController.getProject().getCurrent()
				.getInputData().getGUList()) {

			for (Layer layer : ug.getLayers()) {
				data.addAll(layer.getPerforationConditions());
				for (PerforationCondition fc : layer.getPerforationConditions()) {
					System.out.println("otros");
					fc.setGuName(ug.getName()); 
					fc.setLayerNumber(layer.getLayerNumber());
					System.out.println(System.identityHashCode(fc));
				}
			}
		}
		
		tableData = FXCollections.observableArrayList(data);
		perforationTable.setItems(tableData);
	}
	
	/**
	 * skin button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void skinButtonAction(MouseEvent ev) {
		/*
	   double qt = FieldHelper.getDouble(this.qtField.getText());
	   double bhfp = FieldHelper.getDouble(this.bhfpField.getText());
	   double  h,  pr,  sg,  sw,  bg,  bo,  bw,  ug,  uw,  uo,  e_radius,  well_radius,  radial_perm;
	   
	   SkinFactorCalculator sfc = new SkinFactorCalculator();
	   BaseData bd = workflowController.getProject().getCurrent()
				.getInputData();
	   
	   Reservoir rs =  workflowController.getProject().getCurrent()
				.getInputData().getReservoir();
	   PvtValue pv =  bd.getPvtList().get(bd.getPvtList().size()-1);
	   h = rs.getFormationThickness();
	   well_radius = rs.getWellboreRadius();
	   e_radius = rs.getExternalRadius();
	   
	   for (GeologicalUnit uge : workflowController.getProject().getCurrent()
				.getInputData().getGUList()) {

			for (Layer ly : uge.getLayers()) {
				for (PerforationCondition fc : ly.getPerforationConditions()) {
					
					//System.out.println(System.identityHashCode(fc));
					//Layer ly  = fc.getLayer();
					System.out.println(ly);
					FormationCondition lfc = ly.getFormationConditions().get(0);
					InitialCondition lic = ly.getInitialConditions().get(0);
					pr = lic.getInitialReservoirPressure();
					sg = lic.getInitialGasSaturation();
					sw = lic.getInitialWaterSaturation();
					radial_perm = lfc.getPermeabilityX();
					bg = pv.getBg();
				    bo = pv.getBo();
				    bw = pv.getBw();
				    ug = pv.getUg();
				    uo = pv.getUo();
				    uw = pv.getUw();
				    
				    double skin = sfc.calculateSkin(qt, bhfp, h, pr, sg, sw, bg, bo, bw, ug, uw, uo, e_radius, well_radius, radial_perm);
					fc.setSkin(skin);
					fc.save();
					
				}
			}
	   }
		this.initializeTable();*/
	}
	
	
	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// TODO: delete, ojo doble referencia (grid y layer)
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("krs.view"),
				AppMain.getLabelsProperties().getProperty("krs.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"operating.view"), AppMain.getLabelsProperties()
						.getProperty("operating.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table, delete?
	}

	@Override
	public void setScreenData(Project project) {
		for(PerforationCondition gu : tableData){
			gu.saveIt(); 
		}
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
	}

	@Override
	public void isValidForm() {
		// TODO Validate
		// Total used layers = grid vertical blocks num
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return null;
	}

}
