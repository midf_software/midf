package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.models.Project;
import application.models.Reservoir;

/**
 * @Project reservoir GUI controller.
 * 
 * @author CV
 *
 */
public class ReservoirController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	private List<Node> componentsList = new ArrayList<Node>();

	@FXML
	private TextField formationThicknessText;

	@FXML
	private TextField wellboreRadiusText;

	@FXML
	private TextField permeabilityRadialText;

	@FXML
	private TextField porosityText;

	@FXML
	private TextField externalRadiusText;

	@FXML
	private TextField formationTopDepthText;

	@FXML
	private TextField permeabilityVerticalText;
	
	@FXML	
	private RadioButton oilGasRadio ;
	
	@FXML 
	private RadioButton oilWaterRadio ;
	
	@FXML 
	private RadioButton waterGasRadio ;
	@FXML
	private TextField oilSatText;
	@FXML
	private TextField gasSatText;
	@FXML
	private TextField waterSatText;

	private ToggleGroup phaseGroup;

	//@FXML
	//private TextField rockDensityText;
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		/*componentsList.add(formationThicknessText);
		componentsList.add(wellboreRadiusText);
		componentsList.add(permeabilityRadialText);
		componentsList.add(porosityText);
		componentsList.add(externalRadiusText);
		componentsList.add(formationTopDepthText);
		componentsList.add(permeabilityVerticalText);
		componentsList.add(rockDensityText);*/
		
		//this.porosityText.setDisable(true);
		//this.permeabilityRadialText.setDisable(true);
		//this.permeabilityVerticalText.setDisable(true);
		phaseGroup = new ToggleGroup();
		oilGasRadio.setToggleGroup(phaseGroup);
		oilGasRadio.setUserData("1");
		oilWaterRadio.setToggleGroup(phaseGroup);
		oilWaterRadio.setUserData("2");
		waterGasRadio.setToggleGroup(phaseGroup);
		waterGasRadio.setUserData("3");
		
		
		phaseGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
		    public void changed(ObservableValue<? extends Toggle> ov,
		            Toggle old_toggle, Toggle new_toggle) {
		                if (phaseGroup.getSelectedToggle() != null) {
		                	
		                	gasSatText.setDisable(false);
		                	waterSatText.setDisable(false);
		                	oilSatText.setDisable(false);
		                	
		                	switch(Integer.parseInt((String)phaseGroup.getSelectedToggle().getUserData())) {
		                	case 1:
		                		
		                		waterSatText.setDisable(true);
		                		waterSatText.setText(waterSatText.getText() == null?"0.0":waterSatText.getText());
		                		break;
		                	case 2:
		                		//TextField td = gasSatText;
		                		gasSatText.setDisable(true);
		                		gasSatText.setText(gasSatText.getText() == null?"0.0":gasSatText.getText());
		                		break;
		                	case 3:
		                		oilSatText.setDisable(true);
		                		oilSatText.setText(oilSatText.getText() == null?"0.0":oilSatText.getText());
		                		break;
		                		
		                	}
		                   
		                }                
		            }
		    });
		
		//oilGasRadio.setSelected(true);
		fv = new FormValidator(
				new ValidationClause("Formation Thickness", formationThicknessText, new DecimalNumberRule(1, 100000)),
				new ValidationClause("Wellbore Radius", wellboreRadiusText, new DecimalNumberRule(0, 100000)),
				new ValidationClause("External Radius", externalRadiusText, new DecimalNumberRule(0, 100000)),
				new ValidationClause("FormationTopDepth", formationTopDepthText, new DecimalNumberRule(1, 100000))
				
				);
	}

	/**
	 * Next button action handler, calls the next form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"grid.data.view"),
				AppMain.getLabelsProperties().getProperty(
						"grid.data.title"));
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"scenary.general.data.view"),
						AppMain.getLabelsProperties().getProperty(
								"general.data.title"));
	}

	/**
	 * Clean button action handler, cleans the GUI fields
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		formationThicknessText.setText("");
		wellboreRadiusText.setText("");
		permeabilityRadialText.setText("");
		porosityText.setText("");
		externalRadiusText.setText("");
		formationTopDepthText.setText("");
		permeabilityVerticalText.setText("");
		//rockDensityText.setText("");
	}

	/**
	 * Validates the form
	 * @throws Exception 
	 */
	@Override
	public void isValidForm() throws Exception {
		/*for (Node component : componentsList) {
			if (component instanceof TextField) {
				validateNumber(((TextField) component).getText());
			}
		}*/
		fv.validate();
	}

	/**
	 * Validates if a text contains numbers only.
	 * 
	 * @param text
	 *            Text to validate
	 * @throws NumberFormatException
	 *             If text is not a number
	 */
	private void validateNumber(String text) throws NumberFormatException {
		if (!text.equals("".trim())) {
			Double.parseDouble(text);
		}
	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {

		double externalRadius = Double.parseDouble(externalRadiusText.getText()
				.equals("") ? "0" : externalRadiusText.getText());
		double formationDepthTop = Double.parseDouble(formationTopDepthText
				.getText().equals("") ? "0" : formationTopDepthText.getText());
		double formationThickness = Double.parseDouble(formationThicknessText
				.getText().equals("") ? "0" : formationThicknessText.getText());
		double porosity = Double
				.parseDouble(porosityText.getText().equals("") ? "0"
						: porosityText.getText());
		double radialPermeability = Double.parseDouble(permeabilityRadialText
				.getText().equals("") ? "0" : permeabilityRadialText.getText());
		double rockDensity = 0.0;
		double verticalPermeability = Double
				.parseDouble(permeabilityVerticalText.getText().equals("") ? "0"
						: permeabilityVerticalText.getText());
		double wellboreRadius = Double.parseDouble(wellboreRadiusText.getText()
				.equals("") ? "0" : wellboreRadiusText.getText());

		int phases = Integer.parseInt((String)this.phaseGroup.getSelectedToggle().getUserData());
		
		Reservoir reservoir;
		
		if(project.getCurrent().getInputData().getReservoir() == null){
			reservoir = new Reservoir();
		} else {
			reservoir = project.getCurrent().getInputData().getReservoir() ;
		}
		
		reservoir.setExternalRadius(externalRadius);
		reservoir.setFormationDepthTop(formationDepthTop);
		reservoir.setFormationThickness(formationThickness);
		reservoir.setPorosity(porosity);
		reservoir.setRadialPermeability(radialPermeability);
		reservoir.setRockDensity(rockDensity);
		reservoir.setVerticalPermeability(verticalPermeability);
		reservoir.setWellboreRadius(wellboreRadius);
		reservoir.setPhases(phases);
		
		project.getCurrent().getInputData().setReservoir(reservoir);
		
		reservoir.saveIt();
	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
		int i=0;
		int b = i;
		oilGasRadio.setSelected(true);
		if (project != null && project.getCurrent().getInputData().getReservoir() != null) {
			Reservoir rs = project.getCurrent().getInputData().getReservoir() ;
			externalRadiusText.setText(String.valueOf(rs
					.getExternalRadius()));
			formationTopDepthText.setText(String.valueOf(rs
					.getFormationDepthTop()));
			formationThicknessText.setText(String.valueOf(rs.getFormationThickness()));
			porosityText.setText(String.valueOf(rs.getPorosity()));
			permeabilityRadialText.setText(String.valueOf(rs.getRadialPermeability()));
			//rockDensityText.setText(String.valueOf(rs.getRockDensity()));
			permeabilityVerticalText.setText(String.valueOf(rs.getVerticalPermeability()));
			wellboreRadiusText.setText(String.valueOf(rs.getWellboreRadius()));
			switch(rs.getPhases()) {
			case 1:
				this.oilGasRadio.setSelected(true);
				break;
			case 2:
				this.oilWaterRadio.setSelected(true);
				break;
			case 3:
				this.waterGasRadio.setSelected(true);
				break;
			}
		}
	}

	/**
	 * Sets the workflfow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}

}
