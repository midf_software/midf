package application.controllers.project.scenary;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.models.FormationCondition;
import application.models.GeologicalUnit;
import application.models.Grid;
import application.models.InitialCondition;
import application.models.Layer;
import application.models.PerforationCondition;
import application.models.Project;
import application.utils.FieldHelper;

/**
 * Grid view data GUI controller.
 * 
 * @author CV
 *
 */
public class GridDataController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TextField radialText;

	@FXML
	private TextField angularText;

	@FXML
	private TextField verticalText;
	
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator(
				new ValidationClause("Radial Blocks", radialText, new DecimalNumberRule(1, 100)),
				new ValidationClause("Angular Blocks", angularText, new DecimalNumberRule(1, 1)),
				new ValidationClause("Vertical Blocks", verticalText, new DecimalNumberRule(1, 100))
				);
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"reservoir.view"),
						AppMain.getLabelsProperties().getProperty(
								"reservoir.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("gu.data.view"),
				AppMain.getLabelsProperties().getProperty("gu.data.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		radialText.setText("");
		angularText.setText("");
		verticalText.setText("");
	}

	/**
	 * Sets the screen components data on the current scenary 
	 */
	@Override
	public void setScreenData(Project project) {
		
		boolean isNewGrid = false;
		Grid grid;
				
		if(project.getCurrent().getInputData().getGrid() == null){
			grid = new Grid();
			isNewGrid = true;
		} else {
			grid = project.getCurrent().getInputData().getGrid() ;
		}
		
		grid.setVerticalNumblocks(FieldHelper.getDouble(verticalText.getText()));
		grid.setAngularNumblocks(FieldHelper.getDouble(angularText.getText()));
		grid.setRadialNumblocks(FieldHelper.getDouble(radialText.getText()));
		
		project.getCurrent().getInputData().setGrid(grid);
		grid.saveIt();
				
		// Sincronizar layers
		int numLayers =  grid.getLayerList().size();
		
		// Crear layers si se ingresa un numero mayor de bloques verticales
		// que el numero de capas capas existente
		while (numLayers < grid.getVerticalNumblocks()){
			Layer ly = new Layer();
			//crearle unas condiciones de capa por defecto
			//ly.set
			ly.setLayerNumber(numLayers + 1);
			grid.addLayer(ly);
			
			ly.addFormationCondition(new FormationCondition());
			ly.addInitialCondition(new InitialCondition());
			ly.addPerforationCondition(new PerforationCondition());
			ly.saveIt();
			
			numLayers++;
		}
		// Eliminar layers por encima del nuevo numero de capas especificado
		while(grid.getVerticalNumblocks() < numLayers){
			Layer ly = grid.getLayerList().get(numLayers - 1);
			ly.deleteCascade();
			numLayers--;
		}
		
		// if new grid then create a default GU
		if (isNewGrid){
			GeologicalUnit gu = new GeologicalUnit();
			gu.setName("Geological unit");
			gu.setGrid(grid);
			grid.addGU(gu);
			gu.setThickness(project.getCurrent().getInputData().getReservoir().getFormationThickness());
			gu.setNumLayers((int) grid.getVerticalNumblocks());
			gu.saveIt();
			
		}
		
		// TODO: update GU indexes
	}

	/**
	 * Set the current scenary data on the screen components
	 */
	@Override
	public void loadScreenData(Project project) {
		if (project != null && project.getCurrent().getInputData().getGrid() != null) {
			Grid grid = project.getCurrent().getInputData().getGrid();
			verticalText.setText(FieldHelper.getText(grid.getVerticalNumblocks()));
			angularText.setText(FieldHelper.getText(grid.getAngularNumblocks()));
			radialText.setText(FieldHelper.getText(grid.getRadialNumblocks()));
		}
	}

	/**
	 * validates the form
	 * @throws Exception 
	 */
	@Override
	public void isValidForm() throws Exception {
		fv.validate();
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}

}
