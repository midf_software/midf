package application.controllers.project.scenary.basic;

import application.controllers.AppMain;
import application.controllers.MainController;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.models.Project;
import application.models.Scenario;

public class BasicSimulationWorkflow extends ScenaryWorkflowBase{
	public BasicSimulationWorkflow(MainController mainController, Project project,
			Scenario scenario) {
		super(mainController, project, scenario);
		// TODO Auto-generated constructor stub
		this.inputMenuFXML = AppMain.getConfigProperties().getProperty("basic.simulation.menu");
	}

	@Override
	public void initScenarioWorkflow() {
		// TODO Auto-generated method stub
		
	}

}
