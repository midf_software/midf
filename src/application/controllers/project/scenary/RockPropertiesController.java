package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.NoValidationRule;
import application.models.FormationCondition;
import application.models.GeologicalUnit;
import application.models.Layer;
import application.models.Project;
import application.utils.TableHelper;

/**
 * @Project initial conditions GUI controller.
 * 
 * @author CV
 *
 */
public class RockPropertiesController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<FormationCondition> propertiesTable;

	@FXML
	private TableColumn<FormationCondition, Double> guColumn;

	@FXML
	private TableColumn<FormationCondition, Double> layerColumn;

	@FXML
	private TableColumn<FormationCondition, Double> blocksColumn;

	@FXML
	private TableColumn<FormationCondition, Double> porosityColumn;

	@FXML
	private TableColumn<FormationCondition, Double> radialColumn;

	@FXML
	private TableColumn<FormationCondition, Double> angularColumn;

	@FXML
	private TableColumn<FormationCondition, Double> verticalColumn;

	private ObservableList<FormationCondition> tableData;
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator(
				new ValidationClause("Initial Conditions Table", propertiesTable, 
						new NoValidationRule(),
						new NoValidationRule(),
						new NoValidationRule(),
						new DecimalNumberRule(0, 100000),
						new DecimalNumberRule(0, 100000),
						new DecimalNumberRule(0, 100000),
						new DecimalNumberRule(0, 100000)
						)
				);
	}

	public void initializeTable() {
		List<TableColumn<FormationCondition, ?>> cols = new ArrayList<>();
		guColumn.setEditable(false);
		layerColumn.setEditable(false);

		cols.add(guColumn);
		cols.add(layerColumn);
		cols.add(blocksColumn);
		cols.add(porosityColumn);
		cols.add(radialColumn);
		cols.add(angularColumn);
		cols.add(verticalColumn);

		ObservableList<TableColumn<FormationCondition, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("guName");
		atts.add("layerNumber");
		atts.add("rule");
		atts.add("porosity");
		atts.add("permeabilityX");
		atts.add("permeabilityY");
		atts.add("permeabilityZ");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(int.class);
		attsTypes.add(String.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);

		TableHelper<FormationCondition> helper = new TableHelper<FormationCondition>(
				propertiesTable, colsList, atts, attsTypes);

		List<FormationCondition> data = new ArrayList<FormationCondition>();

		for (GeologicalUnit ug : workflowController.getProject().getCurrent()
				.getInputData().getGUList()) {

			for (Layer layer : ug.getLayers()) {
				data.addAll(layer.getFormationConditions());
				
				for (FormationCondition fc : layer.getFormationConditions()) {
					fc.setGuName(ug.getName());
					fc.setLayerNumber(layer.getLayerNumber());
				}
			}
		}

		tableData = FXCollections.observableArrayList(data);

		propertiesTable.setItems(tableData);
	}

	/**
	 * Add new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {

		GeologicalUnit firstGu = workflowController.getProject().getCurrent()
				.getInputData().getGUList().get(0);
		Layer firstLayer = firstGu.getLayers().get(0);

		FormationCondition newFc = new FormationCondition();
		newFc.setGuName(firstGu.getName());
		newFc.setLayerNumber(firstLayer.getLayerNumber());
		
		firstLayer.addFormationCondition(newFc);

		tableData.add(newFc);
	}

	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		FormationCondition selectedFc = propertiesTable.getSelectionModel().getSelectedItem();
		if (selectedFc != null) {
			tableData.remove(propertiesTable.getSelectionModel()
					.getSelectedIndex());
			selectedFc.getLayer().getFormationConditions().remove(selectedFc);
			selectedFc.delete();
		}
	}

	/**
	 * Next button action handler, calls the next form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"initial.conditions.view"),
				AppMain.getLabelsProperties().getProperty(
						"initial.conditions.title"));
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("gu.data.view"),
				AppMain.getLabelsProperties().getProperty("gu.data.title"));
	}

	/**
	 * Clean button action handler, cleans the GUI fields
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table
	}

	/**
	 * Validates the form
	 * @throws Exception 
	 */
	@Override
	public void isValidForm() throws Exception {
		// perm rules
		// blocks regex
		fv.validate();
	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {
		for(FormationCondition fc : tableData){
			fc.saveIt();
		}
	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
	}

	/**
	 * Sets the workflfow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}
}
