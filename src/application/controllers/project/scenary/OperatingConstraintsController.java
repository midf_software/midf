package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.models.BaseData;
import application.models.FormationCondition;
import application.models.GeologicalUnit;
import application.models.InitialCondition;
import application.models.Layer;
import application.models.OperationCondition;
import application.models.Grid;
import application.models.OperationCondition;
import application.models.Project;
import application.models.PvtValue;
import application.models.Reservoir;
import application.utils.FieldHelper;
import application.utils.TableHelper;
import application.utils.TableHelper.EditingCell;
import application.controllers.simulation.SkinFactorCalculator;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.StringValueRule;
/**
 * OperatingConstraints view data GUI controller.
 * 
 * @author JMC
 *
 */
public class OperatingConstraintsController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<OperationCondition> operationTable;

	@FXML
	private TableColumn<OperationCondition, String> timeColumn;

	@FXML
	private TableColumn<OperationCondition, Double> qgColumn;

	@FXML
	private TableColumn<OperationCondition, Double> qoColumn;

	@FXML
	private TableColumn<OperationCondition, Double> qwColumn;

	@FXML
	private TableColumn<OperationCondition, Double> bhfpColumn;

	@FXML
	private TableColumn<OperationCondition, Double> dbhfpColumn;
		
	
	private ObservableList<OperationCondition> tableData;
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator(new ValidationClause("Operating Constraints Table", operationTable,
				new StringValueRule(),
				new DecimalNumberRule(0, 100000), //TODO definir max
				new DecimalNumberRule(0, 100000),
				new DecimalNumberRule(0, 100000),
				new DecimalNumberRule(0, 100000),
				new DecimalNumberRule(0, 100000)
				));
	}

	public void initializeTable() {
		List<TableColumn<OperationCondition, ?>> cols = new ArrayList<>();

		cols.add(timeColumn);
		cols.add(qgColumn);
		cols.add(qoColumn);
		cols.add(qwColumn);
		cols.add(bhfpColumn);
		cols.add(dbhfpColumn);

		ObservableList<TableColumn<OperationCondition, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("date");
		atts.add("qg");
		atts.add("qo");
		atts.add("qw");
		atts.add("bhfp");
		atts.add("dbhfp");
		
		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);

		TableHelper<OperationCondition> helper = new TableHelper<OperationCondition>(operationTable,
				colsList, atts, attsTypes);
		
		List<OperationCondition> data = new ArrayList<OperationCondition>();

		for(OperationCondition oc: workflowController.getProject().getCurrent().getInputData().getOcList()) {
			oc.setStartDate(workflowController.getProject().getCurrent().getDate()); //Set start date
			data.add(oc);
		}
		tableData = FXCollections.observableArrayList(data);
		operationTable.setItems(tableData);
	}

	/**
	 * new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {
		OperationCondition oc = new OperationCondition();
		oc.setTime(0);
		oc.setQg(0);
		oc.setQo(0);
		oc.setQw(0);
		oc.setBhfp(0);
		oc.setDbhfp(0);
		oc.setStartDate(workflowController.getProject().getCurrent().getDate()); //Set start date
		this.workflowController.getProject().getCurrent().getInputData().addOperationCondition(oc);
		/*for(Node n: this.operationTable.lookupAll("TableRow")){
			TableRow tr = (TableRow) n;
			List<Node> child = tr.getChildrenUnmodifiable();
			EditingCell ch1  = (EditingCell) child.get(0);
			ch1.setStyle("-fx-background-color: red");
			ch1.applyCss();
			int i = 0;
			i = i+1;
		}*/
		
		tableData.add(oc);
		
	}

	
	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// TODO: delete, ojo doble referencia (grid y layer)
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("perforations.view"),
				AppMain.getLabelsProperties().getProperty("perforations.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"skin.view"),
				AppMain.getLabelsProperties().getProperty(
						"skin.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table, delete?
	}

	@Override
	public void setScreenData(Project project) {
		for(OperationCondition oc : tableData){
			oc.saveIt(); 
		}
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
	}

	@Override
	public void isValidForm() throws Exception {
		// TODO Validate
		fv.validate();
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return fv;
	}

}
