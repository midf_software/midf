package application.controllers.project.scenary;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.models.BaseData;
import application.models.PvtDensity;
import application.models.PvtValue;
import application.models.Grid;
import application.models.Project;
import application.models.PvtValue;
import application.utils.FieldHelper;
import application.utils.FileTableParser;
import application.utils.TableHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;

public class PVTController implements Initializable,
ScenaryControlledScreen {
	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<PvtValue> pvtTable;

	@FXML
	private TableColumn<PvtValue, Double> pressureColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> ugColumn;

	@FXML
	private TableColumn<PvtValue, Double> uoColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> uwColumn;

	@FXML
	private TableColumn<PvtValue, Double> bgColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> boColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> bwColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> rsColumn;
	
	@FXML
	private TableColumn<PvtValue, Double> rvColumn;
	
	@FXML
	private TextField gasField;
	
	@FXML
	private TextField waterField;
	
	@FXML
	private TextField oilField;
	
	private ObservableList<PvtValue> tableData;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
 
	}

	public void initializeTable() {
		List<TableColumn<PvtValue, ?>> cols = new ArrayList<>();

		cols.add(pressureColumn);
		cols.add(ugColumn);
		cols.add(uoColumn);
		cols.add(uwColumn);
		cols.add(bgColumn);
		cols.add(boColumn);
		cols.add(bwColumn);
		cols.add(rsColumn);
		cols.add(rvColumn);
		
		ObservableList<TableColumn<PvtValue, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();
		
		atts.add("pressure");
		atts.add("ug");
		atts.add("uo");
		atts.add("uw");
		atts.add("bg");
		atts.add("bo");
		atts.add("bw");
		atts.add("rs");
		atts.add("rv");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);
		

		TableHelper<PvtValue> helper = new TableHelper<PvtValue>(pvtTable,
				colsList, atts, attsTypes);

		tableData = FXCollections
				.observableArrayList(workflowController.getProject()
						.getCurrent().getInputData().getPvtList());
		pvtTable.setItems(tableData);
	}
	
	/**
	 * Add new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {
		// TODO: add
		PvtValue newPvtValue = new PvtValue();
		tableData.add(newPvtValue);
	}
	/**
	 * import button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void importButtonAction(MouseEvent ev) {
		// TODO: import
		File file = this.workflowController.showFileChooser();
		List<double[]>data = FileTableParser.parseFile(file.getAbsolutePath());
		BaseData inputData = this.workflowController.getProject().getCurrent().getInputData();
		
		for (int i=0; i<data.size(); i++){
			double []d= data.get(i);
			PvtValue pval = new PvtValue(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8]);
			inputData.addPvtValue(pval);
			tableData.add(pval);
		}
	}
	
	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// TODO: delete
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("initial.conditions.view"), 
				AppMain.getLabelsProperties().getProperty("initial.conditions.title")); 
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"krs.view"), AppMain.getLabelsProperties() 
						.getProperty("krs.title")); 
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table
	}

	@Override
	public void setScreenData(Project project) {
		// Guardar densidades
		PvtDensity pden;
		if(project.getCurrent().getInputData().getPvtDensities() != null) {
			pden = project.getCurrent().getInputData().getPvtDensities();
		}
		else {
			pden = new PvtDensity();
			project.getCurrent().getInputData().setPvtDensities(pden);
		}
		
		pden.setGasDensity(FieldHelper.getDouble(this.gasField.getText()));
		pden.setWaterDensity(FieldHelper.getDouble(this.waterField.getText()));
		pden.setOilDensity(FieldHelper.getDouble(this.oilField.getText()));
		pden.saveIt();
		
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
		PvtDensity pden = project.getCurrent().getInputData().getPvtDensities();
		if (pden != null) {
			this.gasField.setText(FieldHelper.getText(pden.getGasDensity()));
			this.waterField.setText(FieldHelper.getText(pden.getWaterDensity()));
			this.oilField.setText(FieldHelper.getText(pden.getOilDensity()));
		}
	}

	@Override
	public void isValidForm() {
		// TODO Validate
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return null;
	}
}
