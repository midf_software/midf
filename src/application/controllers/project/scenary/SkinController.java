package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.models.BaseData;
import application.models.FormationCondition;
import application.models.GeologicalUnit;
import application.models.InitialCondition;
import application.models.Layer;
import application.models.SkinCondition;
import application.models.Grid;
import application.models.SkinCondition;
import application.models.Project;
import application.models.PvtValue;
import application.models.Reservoir;
import application.utils.FieldHelper;
import application.utils.TableHelper;
import application.utils.TableHelper.EditingCell;
import application.controllers.simulation.SkinFactorCalculator;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
/**
 * OperatingConstraints view data GUI controller.
 * 
 * @author JMC
 *
 */
public class SkinController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<SkinCondition> propertiesTable;

	@FXML
	private TableColumn<SkinCondition, String> timeColumn;

	@FXML
	private TableColumn<SkinCondition, Double> skinvalColumn;

	@FXML
	private TableColumn<SkinCondition, Double> skinradColumn;

	
	private ObservableList<SkinCondition> tableData;
	private FormValidator fv;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator();
	}

	public void initializeTable() {
		List<TableColumn<SkinCondition, ?>> cols = new ArrayList<>();

		cols.add(timeColumn);
		cols.add(skinvalColumn);
		cols.add(skinradColumn);
	
		ObservableList<TableColumn<SkinCondition, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("date");
		atts.add("skin");
		atts.add("skinRadius");
		
		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(double.class);
		attsTypes.add(double.class);

		TableHelper<SkinCondition> helper = new TableHelper<SkinCondition>(propertiesTable,
				colsList, atts, attsTypes);
		
		List<SkinCondition> data = new ArrayList<SkinCondition>();

		
		for(SkinCondition sc: workflowController.getProject().getCurrent().getInputData().getSkinList()) {
			sc.setStartDate(workflowController.getProject().getCurrent().getDate()); //Set start date
			data.add(sc);
		}
		
		tableData = FXCollections.observableArrayList(data);
		propertiesTable.setItems(tableData);
	}

	/**
	 * new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {
		SkinCondition oc = new SkinCondition();
		oc.setTime(0);
		oc.setSkin(0);
		oc.setSkinRadius(0);
		oc.setStartDate(workflowController.getProject().getCurrent().getDate()); //Set start date
		this.workflowController.getProject().getCurrent().getInputData().addSkinCondition(oc);
		/*for(Node n: this.operationTable.lookupAll("TableRow")){
			TableRow tr = (TableRow) n;
			List<Node> child = tr.getChildrenUnmodifiable();
			EditingCell ch1  = (EditingCell) child.get(0);
			ch1.setStyle("-fx-background-color: red");
			ch1.applyCss();
			int i = 0;
			i = i+1;
		}*/
		
		tableData.add(oc);
		
	}

	
	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// TODO: delete, ojo doble referencia (grid y layer)
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("operating.view"),
				AppMain.getLabelsProperties().getProperty("operating.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"simulation.conditions.view"),
				AppMain.getLabelsProperties().getProperty(
						"simulation.conditions.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table, delete?
	}

	@Override
	public void setScreenData(Project project) {
		for(SkinCondition oc : tableData){
			oc.saveIt(); //TODO 
		}
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
	}

	@Override
	public void isValidForm() throws Exception {
		// TODO Validate
		fv.validate();
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return fv;
	}

}
