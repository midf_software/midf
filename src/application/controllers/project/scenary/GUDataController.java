package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.IntegerNumberRule;
import application.controllers.validation.rules.StringValueRule;
import application.models.GeologicalUnit;
import application.models.Grid;
import application.models.Project;
import application.utils.FieldHelper;
import application.utils.TableHelper;

/**
 * GU view data GUI controller.
 * 
 * @author CV
 *
 */
public class GUDataController implements Initializable, ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TableView<GeologicalUnit> guTable;

	@FXML
	private TableColumn<GeologicalUnit, String> nameColumn;

	@FXML
	private TableColumn<GeologicalUnit, Double> thicknessColumn;

	@FXML
	private TableColumn<GeologicalUnit, Integer> numLayersColumn;

	@FXML
	private Label verticalLabel;

	private ObservableList<GeologicalUnit> tableData;
	private FormValidator fv;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fv = new FormValidator(new ValidationClause("GU Table", guTable,
				new StringValueRule(), new DecimalNumberRule(0, 100000),
				new IntegerNumberRule(1, 100)));
	}

	public void initializeTable() {
		List<TableColumn<GeologicalUnit, ?>> cols = new ArrayList<>();

		cols.add(nameColumn);
		cols.add(thicknessColumn);
		cols.add(numLayersColumn);

		ObservableList<TableColumn<GeologicalUnit, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("name");
		atts.add("thickness");
		atts.add("numLayers");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(double.class);
		attsTypes.add(int.class);

		TableHelper<GeologicalUnit> helper = new TableHelper<GeologicalUnit>(
				guTable, colsList, atts, attsTypes);

		tableData = FXCollections.observableArrayList(workflowController
				.getProject().getCurrent().getInputData().getGUList());
		this.verticalLabel.setText(FieldHelper.getText(workflowController
				.getProject().getCurrent().getInputData().getGrid()
				.getVerticalNumblocks()));
		guTable.setItems(tableData);
	}

	/**
	 * Add new row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void newRowButtonAction(MouseEvent ev) {
		GeologicalUnit newGeoUnit = new GeologicalUnit();
		newGeoUnit.setName("New GU");
		newGeoUnit.setThickness(0);
		newGeoUnit.setNumLayers(0);
		Grid grid = this.workflowController.getProject().getCurrent()
				.getInputData().getGrid();

		newGeoUnit.setGrid(grid); // TODO optimizar doble referencia
		grid.addGU(newGeoUnit);

		tableData.add(newGeoUnit);
	}

	/**
	 * Delete row button action
	 * 
	 * @param ev
	 */
	@FXML
	protected void deleteRowButtonAction(MouseEvent ev) {
		// 1. clearlayers
		// 2. ug.delete
		// 3. grid.synclayers
		GeologicalUnit gu = guTable.getSelectionModel()
				.getSelectedItem();
		if (gu != null) {
			Grid grid = gu.getGrid();
			
			tableData.remove(gu);
			gu.clearLayers();
			gu.delete();
			
			grid.getGuList().remove(gu);
			grid.syncLayersAndUgs();
		}
		
	}

	/**
	 * Previous button action handler, calls the prev form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("grid.data.view"),
				AppMain.getLabelsProperties().getProperty("grid.data.title"));
	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty(
						"rock.properties.view"), AppMain.getLabelsProperties()
						.getProperty("rock.properties.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		// TODO: clean table, delete?
	}

	@Override
	public void setScreenData(Project project) {
		for (GeologicalUnit gu : tableData) {
			gu.saveIt();
		}
	}

	@Override
	public void loadScreenData(Project project) {
		// Table loaded in initializeTable()
	}

	@Override
	public void isValidForm() throws Exception {
		// Total used layers = grid vertical blocks num
		fv.validate();
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
		initializeTable();
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}

}
