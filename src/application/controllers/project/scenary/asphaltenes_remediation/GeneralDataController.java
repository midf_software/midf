package application.controllers.project.scenary.asphaltenes_remediation;
import java.net.URL;
import java.util.ResourceBundle;

import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryControlledScreen;
import application.controllers.validation.FormValidator;
import application.models.Project;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

public class GeneralDataController  implements Initializable, ScenaryControlledScreen{
	    private AsphaltenesRemediationWorkflow workflowController;
	 

	    @FXML
	    void cleanButtonAction(ActionEvent event) {

	    }

	    @FXML
	    void prevButtonAction(ActionEvent event) {

	    }

	    @FXML
	    void closeButtonAction(ActionEvent event) {

	    }

		@Override
		public void setWorkflowController(Workflow workflowController) {
			// TODO Auto-generated method stub
			this.workflowController = (AsphaltenesRemediationWorkflow) workflowController;
		}

		@Override
		public Workflow getWorkflowController() {
			// TODO Auto-generated method stub
			return workflowController;
		}

		@Override
		public void setScreenData(Project project) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void loadScreenData(Project project) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void isValidForm() throws Exception {
			// TODO Auto-generated method stub
			
		}

		@Override
		public FormValidator getValidator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			// TODO Auto-generated method stub
			
		}
}
