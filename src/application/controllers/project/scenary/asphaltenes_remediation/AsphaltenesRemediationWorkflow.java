package application.controllers.project.scenary.asphaltenes_remediation;

import application.controllers.AppMain;
import application.controllers.MainController;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.models.Project;
import application.models.Scenario;

public class AsphaltenesRemediationWorkflow extends ScenaryWorkflowBase{
	
	public AsphaltenesRemediationWorkflow(MainController mainController, Project project,
			Scenario scenario) {
		super(mainController, project, scenario);
		// TODO Auto-generated constructor stub
		this.inputMenuFXML = AppMain.getConfigProperties().getProperty("asphaltenes_remediation.menu");
	}

	@Override
	public void initScenarioWorkflow() {
		// TODO Auto-generated method stub
		this.getMainController().getScenarioInfoTab().setExpanded(true);
	}

}
