package application.controllers.project.scenary.asphaltenes;

import application.controllers.AppMain;
import application.controllers.MainController;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.models.Project;
import application.models.Scenario;

public class AsphaltenesWorkflow extends ScenaryWorkflowBase{
	
	public AsphaltenesWorkflow(MainController mainController, Project project,
			Scenario scenario) {
		super(mainController, project, scenario);
		// TODO Auto-generated constructor stub
		this.inputMenuFXML = AppMain.getConfigProperties().getProperty("asphaltenes.menu");
	}

	@Override
	public void initScenarioWorkflow() {
		// TODO Auto-generated method stub
		this.getMainController().getScenarioInfoTab().setExpanded(true);
	}

}
