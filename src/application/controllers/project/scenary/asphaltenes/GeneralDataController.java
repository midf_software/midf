package application.controllers.project.scenary.asphaltenes;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryControlledScreen;
import application.controllers.validation.FormValidator;
import application.models.Project;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class GeneralDataController implements Initializable,
		ScenaryControlledScreen {
	private AsphaltenesWorkflow workflowController;

	@FXML
	private CheckBox onsetCheckBox;

	@FXML
	private CheckBox interactionCheckBox;

	@FXML
	private RadioButton apsRadio2;

	@FXML
	private RadioButton apsRadio1;

	@FXML
	private TextField temperatureText;

	@FXML
	private TextField densityText;

	@FXML
	private TextField vsaText;

	@FXML
	private TextField volText;

	@FXML
	private TextField onsetText;

	@FXML
	void cleanButtonAction(ActionEvent event) {

	}

	@FXML
	void prevButtonAction(ActionEvent event) {

	}

	@FXML
	void closeButtonAction(ActionEvent event) {

	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		// TODO Auto-generated method stub
		this.workflowController = (AsphaltenesWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		// TODO Auto-generated method stub
		return workflowController;
	}

	@Override
	public void setScreenData(Project project) {
		// TODO Auto-generated method stub

	}

	@Override
	public void loadScreenData(Project project) {
		// TODO Auto-generated method stub

	}

	@Override
	public void isValidForm() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public FormValidator getValidator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	/**
	 * Shows pdf help doc
	 * 
	 * @param ev
	 */
	@FXML
	protected void helpButtonAction(MouseEvent ev) {
		try {

			String projectPath = System.getProperty("user.dir");
			File helpFile = new File(projectPath + "\\res\\AyudaAsfaltenos.pdf");

			if (helpFile.exists()) {

				Process p = Runtime.getRuntime().exec(
						"rundll32 url.dll,FileProtocolHandler " + helpFile);
				p.waitFor();

			} else {

				System.out.println("File is not exists");

			}

			System.out.println("Done");

		} catch (Exception ex) {
			workflowController.showDialog(AppMain.getValidationMessages().getProperty("open_file_error"));
		}
	}
}
