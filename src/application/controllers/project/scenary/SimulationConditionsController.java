package application.controllers.project.scenary;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.models.Project;
import application.models.Simulation;
import application.models.SimulationCondition;

/**
 * @Project simulation conditions GUI controller.
 * 
 * @author CV
 *
 */
public class SimulationConditionsController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow controller. */
	private ScenaryWorkflow workflowController;

	/** List containing the GUI components to ease validations */
	private List<Node> componentsList = new ArrayList<Node>();

	
	@FXML
	private TextField maxTimeText;

	@FXML
	private TextField minTimeText;

	@FXML
	private TextField maxSaturationText;

	@FXML
	private TextField maxRsText;

	@FXML
	private TextField accelerationFactorText;

	@FXML
	private TextField relativeErrorText;

	@FXML
	private TextField maxPressureText;

	@FXML
	private TextField totalTimeText;

	@FXML
	private TextField maxIteratonsText;

	@FXML
	private TextField epsilonText;

	private FormValidator fv;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		componentsList.add(maxTimeText);
		componentsList.add(minTimeText);
		componentsList.add(maxSaturationText);
		componentsList.add(maxRsText);
		componentsList.add(accelerationFactorText);
		componentsList.add(relativeErrorText);
		componentsList.add(maxPressureText);
		componentsList.add(totalTimeText);
		componentsList.add(maxIteratonsText);
		componentsList.add(epsilonText);

	

		fv = new FormValidator(new ValidationClause("Max Time Dt", maxTimeText,
				new DecimalNumberRule(0, 365)), new ValidationClause(
				"Min Time Dt", minTimeText, new DecimalNumberRule(0, 365)),
				new ValidationClause("Max Saturation", maxSaturationText,
						new DecimalNumberRule()), new ValidationClause(
						"Max Rs", maxRsText, new DecimalNumberRule()),
				new ValidationClause("Accel Factor", accelerationFactorText,
						new DecimalNumberRule()), new ValidationClause(
						"RelativeError", relativeErrorText,
						new DecimalNumberRule()), new ValidationClause(
						"Max Pressure", maxPressureText,
						new DecimalNumberRule()), new ValidationClause(
						"Total Time", totalTimeText, new DecimalNumberRule()),
				new ValidationClause("Max Iterations", maxIteratonsText,
						new DecimalNumberRule()), new ValidationClause(
						"Epsilon", epsilonText, new DecimalNumberRule()));

	}

	/**
	 * Previous button action, shows the previous form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void prevButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("skin.view"), AppMain
						.getLabelsProperties().getProperty("skin.title"));
	}
	
	/**
	 * Next button action, shows the next form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void closeButtonAction(MouseEvent ev) {
		setScreenData(workflowController.getProject());
		try{
			fv.validate();
		}catch(Exception e){
			workflowController.showDialog("Validation errors:\n" + this.getValidator().getErrorMessagesStr());
			return;
		}
		workflowController.closeCurrentModals();
	}

	/**
	 * Cleans the GUI fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {

	
		maxTimeText.setText("");
		minTimeText.setText("");
		maxSaturationText.setText("");
		maxRsText.setText("");
		accelerationFactorText.setText("");
		relativeErrorText.setText("");
		maxPressureText.setText("");
		totalTimeText.setText("");
		maxIteratonsText.setText("");
		epsilonText.setText("");
	}

	/**
	 * Validates the form.
	 */
	@Override
	public void isValidForm() throws Exception {
		fv.validate();
	}

	/**
	 * Validates if text string cointains numbers only.
	 * 
	 * @param text
	 * @throws NumberFormatException
	 */
	private void validateNumber(String text) throws NumberFormatException {
		if (!text.equals("".trim())) {
			Double.parseDouble(text);
		}
	}

	/**
	 * Sets the GUI data into the @Project.
	 */
	@Override
	public void setScreenData(Project project) {

		SimulationCondition simulCondForm;

		if (project.getCurrent().getSimulation() == null) {
			project.getCurrent().setSimulation(new Simulation());

		}

		if (project.getCurrent().getSimulation().getSimulationCondition() == null) {
			simulCondForm = new SimulationCondition();
			project.getCurrent().getSimulation()
					.setSimulationCondition(simulCondForm);
		} else {
			simulCondForm = project.getCurrent().getSimulation()
					.getSimulationCondition();
		}

		double dtAccelFactor = Double.parseDouble(accelerationFactorText
				.getText().equals("") ? "0" : accelerationFactorText.getText());
		double epsilon = Double
				.parseDouble(epsilonText.getText().equals("") ? "0"
						: epsilonText.getText());
		double maxPressureDelta = Double.parseDouble(maxPressureText.getText()
				.equals("") ? "0" : maxPressureText.getText());
		double maxRsDelta = Double
				.parseDouble(maxRsText.getText().equals("") ? "0" : maxRsText
						.getText());
		double maxSaturationDelta = Double.parseDouble(maxSaturationText
				.getText().equals("") ? "0" : maxSaturationText.getText());
		double maxTimestep = Double.parseDouble(maxTimeText.getText()
				.equals("") ? "0" : maxTimeText.getText());
		double minTimestep = Double.parseDouble(minTimeText.getText()
				.equals("") ? "0" : minTimeText.getText());
		double numIterations = Double.parseDouble(maxIteratonsText.getText()
				.equals("") ? "0" : maxIteratonsText.getText());
	
		double relativeError = Double.parseDouble(relativeErrorText.getText()
				.equals("") ? "0" : relativeErrorText.getText());
		double totalTime = Double.parseDouble(totalTimeText.getText()
				.equals("") ? "0" : totalTimeText.getText());
		

		simulCondForm.setDtAccelFactor(dtAccelFactor);
		simulCondForm.setEpsilon(epsilon);
		simulCondForm.setMaxPressureDelta(maxPressureDelta);
		simulCondForm.setMaxRsDelta(maxRsDelta);
		simulCondForm.setMaxSaturationDelta(maxSaturationDelta);
		simulCondForm.setMaxTimestep(maxTimestep);
		simulCondForm.setMinTimestep(minTimestep);
		simulCondForm.setNumIterations(numIterations);
		simulCondForm.setRelativeError(relativeError);
		simulCondForm.setTotalTime(totalTime);

		project.getCurrent().getSimulation()
				.setSimulationCondition(simulCondForm);
	}

	/**
	 * Sets the @Project data into the GUI.
	 */
	@Override
	public void loadScreenData(Project project) {
		if (project != null
				&& project.getCurrent().getSimulation() != null
				&& project.getCurrent().getSimulation()
						.getSimulationCondition() != null) {

			SimulationCondition sc = project.getCurrent().getSimulation()
					.getSimulationCondition();
			accelerationFactorText
					.setText(String.valueOf(sc.getDtAccelFactor()));
			epsilonText.setText(String.valueOf(sc.getEpsilon()));
			maxPressureText.setText(String.valueOf(sc.getMaxPressureDelta()));
			maxRsText.setText(String.valueOf(sc.getMaxRsDelta()));
			maxSaturationText
					.setText(String.valueOf(sc.getMaxSaturationDelta()));
			maxTimeText.setText(String.valueOf(sc.getMaxTimestep()));
			minTimeText.setText(String.valueOf(sc.getMinTimestep()));
			maxIteratonsText.setText(String.valueOf(sc.getNumIterations()));
			relativeErrorText.setText(String.valueOf(sc.getRelativeError()));
			totalTimeText.setText(String.valueOf(sc.getTotalTime()));
		}
	}

	/**
	 * Sets the workflow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}
}
