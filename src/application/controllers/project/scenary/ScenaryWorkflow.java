package application.controllers.project.scenary;

import java.util.Date;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.Workflow;
import application.models.BaseData;
import application.models.Project;
import application.models.Scenario;
import application.utils.DateUtils;
import application.controllers.factory.ScenarioWorkflowFactory;

/**
 * Class controlling and showing the @Project workflow.
 * 
 * @author CV
 *
 */
public class ScenaryWorkflow extends Workflow {

	private ScenaryControlledScreen currentScreenController;

	private Project project;
	private ScenaryWorkflowBase specificWorkflow;
	
	/**
	 * Contructs a scenario workflow and inits the requiered models
	 * 
	 * @param mainController
	 *            Main GUI controller
	 */

	
	public ScenaryWorkflow(MainController mainController, Project project, String type) {
		super(mainController);
		this.project = project;
		
		Scenario scenario = new Scenario();
		scenario.setDate(DateUtils.dateToString(new Date()));
		scenario.setType(type);
		project.addRelatedModel(scenario);
		scenario.saveIt();
				
		BaseData inputData = new BaseData(); 
		scenario.setInputData(inputData);
		inputData.saveIt();
		
		project.setCurrent(scenario);
	}
	
	/**
	 * Contructs a ProjectWorkflow
	 * 
	 * @param mainController
	 *            Main GUI controller
	 */
	public ScenaryWorkflow(MainController mainController, Project project, Scenario scenario) {
		super(mainController);
		this.project = project;
		project.setCurrent(scenario);
	}

	/**
	 * Starts the scenario workflow, calling the general data form and creating a @Project
	 * instance
	 */
	public void initScenarioWorkflow() {
		this.getMainController().getBaseDataInfoTab().setExpanded(true);
		createScenarioSpecificWorkflow();
		
		setModal(
				AppMain.getConfigProperties().getProperty(
				"scenary.general.data.view"), AppMain.getLabelsProperties().getProperty(
						"general.data.title"));
	}
    /**
     * Instancia el workflow especifico del escenario (si tiene)
     */
	private void createScenarioSpecificWorkflow() {
		
		Project p = this.getProject();
		specificWorkflow = ScenarioWorkflowFactory.createWorkflow(this.getMainController(), p, p.getCurrent());
		if (specificWorkflow != null) {
			specificWorkflow.createInputDataMenu();
			this.specificWorkflow.initScenarioWorkflow();
			// Poner titulo al tab
			this.getMainController().getScenarioInfoTab().setText(p.getCurrent().getType() + " " + "data");
		}
    }
    
	/**
	 * Sets an internal panel (screen) on the workspace, saves and validates the
	 * current form.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setInternalPanel(final String resource) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			// TODO: msg properties + campo errado (manejo excepciones)
			showDialog("NO VALIDA");
			return currentScreenController;
		}

		if (currentScreenController != null) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ScenaryControlledScreen) super
				.setInternalPanel(resource);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}
	
	/**
	 * Shows a modal over the workspace, saves and validates the
	 * current form.
	 * 
	 * @param resource
	 *            FXML URL
	 */
	public ControlledScreen setModal(final String resource, String title) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			// TODO: msg properties + campo errado (manejo excepciones)
			//e.printStackTrace();
			showDialog("Validation errors:\n" + currentScreenController.getValidator().getErrorMessagesStr());
			return currentScreenController;
		}

		if (currentScreenController != null) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ScenaryControlledScreen) super
				.setModal(resource, title, true, true);
		
		getMainController().synchronizeAccordionView(resource);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}

	/**
	 * Project getter.
	 * 
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}
	
	public ScenaryWorkflowBase getSpecificWorkflow() {
		return specificWorkflow;
	}

}
