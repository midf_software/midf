package application.controllers.project.scenary;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.StringValueRule;
import application.models.Project;

/**
 * @Project general data GUI controller.
 * 
 * @author CV
 *
 */
public class ScenaryGeneralDataController implements Initializable,
		ScenaryControlledScreen {

	/** Workflow Controller. */
	ScenaryWorkflow workflowController;

	@FXML
	private TextField nameText;

	@FXML
	private TextField referenceText;

	@FXML
	private TextField typeText;

	@FXML
	private TextField dateText;

	@FXML
	private TextField descriptionText;
	private FormValidator fv;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		fv = new FormValidator(new ValidationClause("Name", nameText,
				new StringValueRule()), new ValidationClause("Reference",
				referenceText, new StringValueRule()), new ValidationClause(
				"Description", descriptionText, new StringValueRule()));

	}

	/**
	 * Next button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void nextButtonAction(MouseEvent ev) {
		workflowController.setModal(
				AppMain.getConfigProperties().getProperty("reservoir.view"),
				AppMain.getLabelsProperties().getProperty("reservoir.title"));
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		nameText.setText("");
		referenceText.setText("");
		typeText.setText("");
		descriptionText.setText("");
	}

	/**
	 * Validates the form
	 * 
	 * @throws Exception
	 */
	@Override
	public void isValidForm() throws Exception {
		//
		fv.validate();
	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {
		project.getCurrent().setName(nameText.getText());
		project.getCurrent().setRef(referenceText.getText());
		project.getCurrent().setType(typeText.getText());
		project.getCurrent().setDescription(descriptionText.getText());

		project.getCurrent().setDate(dateText.getText());

		project.getCurrent().saveIt();
	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
		if (project != null) {
			nameText.setText(project.getCurrent().getName());
			referenceText.setText(project.getCurrent().getRef());
			typeText.setText(project.getCurrent().getType());
			descriptionText.setText(project.getCurrent().getDescription());
			dateText.setText(project.getCurrent().getDate());
		}

	}

	/**
	 * Sets the workflfow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ScenaryWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}

}
