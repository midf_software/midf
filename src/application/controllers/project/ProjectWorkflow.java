package application.controllers.project;

import application.controllers.AppMain;
import application.controllers.ControlledScreen;
import application.controllers.MainController;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.models.Project;
import application.models.Scenario;

/**
 * Class controlling and showing the Project workflow.
 * 
 * @author CV
 *
 */
public class ProjectWorkflow extends Workflow {

	private ProjectControlledScreen currentScreenController;

	private ScenaryWorkflow scenaryWorkflow;

	private Project project;

	/**
	 * Contructs a ProjectWorkflow
	 * 
	 * @param mainController
	 *            Main GUI controller
	 */
	public ProjectWorkflow(MainController mainController) {
		super(mainController);
	}

	/**
	 * Starts the project workflow, calling the general data form and creating a
	 * Project instance
	 */
	public void initProjectWorkflow() {
		project = new Project();
		
		setModal(
				AppMain.getConfigProperties().getProperty(
						"project.general.data.view"), AppMain
						.getLabelsProperties()
						.getProperty("general.data.title"), true, false, false);
	}
	
	/**
	 * Calls project admin for an existing project
	 */
	public void initExistingProjectWorkflow(Project project) {
		this.project = project;
		
		setInternalPanel(AppMain.getConfigProperties()
				.getProperty("project.admin.view"), true);
	}

	/**
	 * Sets an internal panel (screen) on the workspace, saves and validates the
	 * current form.
	 * 
	 * @param resource
	 *            FXML URL
	 * @param save
	 *            indicates if the GUI data should be set on the model
	 */
	public ControlledScreen setInternalPanel(final String resource, boolean save) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			showDialog("Validation errors:\n" + currentScreenController.getValidator().getErrorMessagesStr());
			return currentScreenController;
		}

		if (currentScreenController != null && save) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ProjectControlledScreen) super
				.setInternalPanel(resource);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}

	/**
	 * Shows a modal over the workspace, saves and validates the current form.
	 * 
	 * @param resource
	 *            FXML URL
	 * @param title
	 *            window header title
	 * @param save
	 *            indicates if the GUI data should be set on the model
	 */
	public ControlledScreen setModal(final String resource, String title,
			boolean closeCurrent, boolean cleanWorkspace, boolean save) {

		try {
			if (currentScreenController != null) {
				currentScreenController.isValidForm();
			}
		} catch (Exception e) {
			showDialog("Validation errors:\n" + currentScreenController.getValidator().getErrorMessagesStr());
			return currentScreenController;
		}

		if (currentScreenController != null && save) {
			currentScreenController.setScreenData(project);
		}

		currentScreenController = (ProjectControlledScreen) super.setModal(
				resource, title, closeCurrent, cleanWorkspace);

		currentScreenController.loadScreenData(project);

		return currentScreenController;
	}

	/**
	 * Project getter.
	 * 
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * scenary workflow getter.
	 * 
	 * @return the project
	 */
	public ScenaryWorkflow getScenaryWorkflow() {
		return scenaryWorkflow;
	}

	/**
	 * scenary workflow setter.
	 */
	public void setScenaryWorkflow(ScenaryWorkflow scenaryWorkflow) {
		this.scenaryWorkflow = scenaryWorkflow;
	}

}
