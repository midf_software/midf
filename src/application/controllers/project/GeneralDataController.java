package application.controllers.project;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import javafx.util.StringConverter;
import application.controllers.AppMain;
import application.controllers.Workflow;
import application.controllers.validation.FormValidator;
import application.controllers.validation.ValidationClause;
import application.controllers.validation.rules.StringValueRule;
import application.models.Project;
import application.models.Well;
import application.utils.DateUtils;

/**
 * @Project general data GUI controller.
 * 
 * @author CV
 *
 */
public class GeneralDataController implements Initializable,
		ProjectControlledScreen {

	/** Workflow Controller. */
	ProjectWorkflow workflowController;

	@FXML
	private TextField projectNameText;

	// @FXML
	// private TextField referenceText;

	@FXML
	private TextField descriptionText;

	@FXML
	private ChoiceBox<Well> wellComboBox;

	private FormValidator fv;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		fv = new FormValidator(new ValidationClause("Name", projectNameText,
				new StringValueRule())/*
									 * , new ValidationClause("Reference",
									 * referenceText, new StringValueRule())
									 */);

	}

	/**
	 * Calls the next form, information is saved on setScreenData method
	 * 
	 * @param ev
	 */
	@FXML
	protected void saveButtonAction(MouseEvent ev) {
		// Load scenary admin
		workflowController.setInternalPanel(AppMain.getConfigProperties()
				.getProperty("project.admin.view"), true);
	}

	/**
	 * Cancel button action, shows next @Project form.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cancelButtonAction(MouseEvent ev) {
		Scene scene = projectNameText.getScene();
		if (scene != null) {
			Window window = scene.getWindow();
			if (window != null) {
				window.hide();
			}
		}
	}

	/**
	 * Cleans the form fields.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cleanButtonAction(MouseEvent ev) {
		projectNameText.setText("");
		// referenceText.setText("");
		descriptionText.setText("");
	}

	/**
	 * Validates the form
	 */
	@Override
	public void isValidForm() throws Exception {
		fv.validate();
	}

	/**
	 * Sets the screen data into the current @Project.
	 */
	@Override
	public void setScreenData(Project project) {
		project.setName(projectNameText.getText());
		// project.setRef(referenceText.getText());
		project.setDescription(descriptionText.getText());

		if (project.getDate() == null) {
			project.setDate(DateUtils.dateToString(new Date()));
		}
		
		Well well =	wellComboBox.getSelectionModel().getSelectedItem();
		if(well != null){
			project.setWell(well);
			well.add(project);
		}

		project.saveIt();
	}

	/**
	 * Loads the current @Project data into the screen.
	 */
	@Override
	public void loadScreenData(Project project) {
		if (project != null) {
			projectNameText.setText(project.getName());
			// referenceText.setText(project.getRef());
			descriptionText.setText(project.getDescription());

			StringConverter<Well> converter = new StringConverter<Well>() {

				@Override
				public String toString(Well object) {
					return object.getName();
				}

				@Override
				public Well fromString(String string) {
					return null;
				}

			};
			wellComboBox.setConverter(converter);
			wellComboBox.getItems().addAll(Well.findAll());

			if (project.getWell() != null) {
				wellComboBox.getSelectionModel().select((project.getWell()));
			}

		}
	}

	/**
	 * Sets the workflfow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (ProjectWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public FormValidator getValidator() {
		return fv;
	}

}
