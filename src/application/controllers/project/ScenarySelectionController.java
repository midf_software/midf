package application.controllers.project;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Window;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.controllers.project.scenary.preanalysis.multiparametric.MultiparametricWorkflow;
import application.controllers.validation.FormValidator;
import application.models.Project;
import application.models.ScenarioTypes;

/**
 * Class controlling the Scenario type controller
 * 
 * @author CV
 *
 */
public class ScenarySelectionController implements ProjectControlledScreen {

	/** Workflow controller. */
	ProjectWorkflow workFlowController;

	@FXML
	private BorderPane mainPanel;

	private void newScenarioAction(String type) {
		ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
				workFlowController.getMainController(),
				workFlowController.getProject(), type);
		workFlowController.setScenaryWorkflow(scenaryWorkflow);
		scenaryWorkflow.initScenarioWorkflow();
		// Closes the modal
		Scene scene = mainPanel.getScene();
		if (scene != null) {
			Window window = scene.getWindow();
			if (window != null) {
				window.hide();
			}
		}
	}
	/**
	 * New basic simulation
	 * 
	 * @param ev
	 */
	@FXML
	protected void newBasicScenarioAction(MouseEvent ev) {
		// TODO: create the scenario
		this.newScenarioAction(ScenarioTypes.BASIC_SIMULATION);
	}

	
	/**
	 * New multiparametric simulation
	 * 
	 * @param ev
	 */
	@FXML
	protected void newMultiparametricAction(MouseEvent ev) {
		// TODO: create the multiparametric analysis
		this.newScenarioAction(ScenarioTypes.MULTIPARAMETRIC);
	}
	
	/**
	 * New ipr simulation
	 * 
	 * @param ev
	 */
	@FXML
	protected void newIPRAction(MouseEvent ev) {
		// TODO: create the multiparametric analysis
		this.newScenarioAction(ScenarioTypes.IPR);
	}
	
	/**
	 * New asphaltenes diagnostics simulation
	 * 
	 * @param ev
	 */
	@FXML
	protected void newAsphaltenesDiagnosticAction(MouseEvent ev) {
		this.newScenarioAction(ScenarioTypes.ASPHALTENES_DIAGNOSTIC);
	}
	
	/**
	 * New asphaltenes remediation simulation
	 * 
	 * @param ev
	 */
	@FXML
	protected void newAsphaltenesRemediationAction(MouseEvent ev) {
		this.newScenarioAction(ScenarioTypes.ASPHALTENES_REMEDIATION);
	}
	
	/**
	 * Cancel button action.
	 * 
	 * @param ev
	 */
	@FXML
	protected void cancelButtonAction(MouseEvent ev) {
		Scene scene = mainPanel.getScene();
		if (scene != null) {
			Window window = scene.getWindow();
			if (window != null) {
				window.hide();
			}
		}
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workFlowController = (ProjectWorkflow) workflowController;

	}

	@Override
	public Workflow getWorkflowController() {
		return workFlowController;
	}

	@Override
	public void setScreenData(Project project) {

	}

	@Override
	public void loadScreenData(Project project) {

	}

	@Override
	public void isValidForm() {

	}

	@Override
	public FormValidator getValidator() {
		return null;
	}

}
