package application.controllers;

import application.controllers.project.ProjectWorkflow;
import application.models.Project;

/**
 * Class controlling and showing the workflow for main options and
 * screens.
 * 
 * @author CV
 *
 */
public class MainWorkflow extends Workflow {

	/** Project workflow manager */
	private ProjectWorkflow projectWorkflow;

	/**
	 * Construct
	 * 
	 * @param controller
	 *            Main GUI controller
	 */
	public MainWorkflow(MainController controller) {
		super(controller);
	}

	/**
	 * Creates a new @ProjectWorkflow
	 */
	public void instanceNewProject() {
		projectWorkflow = new ProjectWorkflow(getMainController());
		projectWorkflow.initProjectWorkflow();
	}

	/**
	 * Creates a new @ProjectWorkflow to load an existe project
	 */
	public void loadProject(Project project) {
		projectWorkflow = new ProjectWorkflow(getMainController());
		projectWorkflow.initExistingProjectWorkflow(project);
	}
	
	/**
	 * projectWorkflow getter.
	 * 
	 * @return
	 */
	public ProjectWorkflow getProjectWorkflow() {
		return projectWorkflow;
	}
	
}
