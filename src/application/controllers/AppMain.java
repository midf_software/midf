package application.controllers;
	
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import application.controllers.simulation.BasicSolverSimulator;
import application.models.GeologicalUnit;
import application.models.Grid;
import application.models.Layer;
import application.utils.PropertiesRulesParser;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * Aplication main class
 * inits config.properties and calls login view
 * 
 * @author CV
 *
 */

public class AppMain extends Application {
	
	/** Application properties to store urls and configs */
	private static Properties validationMessages = new Properties();
	
	/** Application properties to store urls and configs */
	private static Properties configProperties = new Properties();
	
	/** Application properties to store labels and names */
	private static Properties labelsProperties = new Properties();

	@Override
	public void start(Stage primaryStage) {
		DBClient.connect(); // Conexion debe estar dentro de hilo javafx
		
		try {
			
			initProperties();
			
			callSecurityAccess(primaryStage);			
			
		} catch(Exception e) {
			// TODO: Manejo de excepciones
			e.printStackTrace();
		}
	}
	
	/**
	 * Inits the config.properties file and the view location variables
	 * @throws IOException
	 */
	private void initProperties() throws IOException {
		configProperties.load(getClass().getResourceAsStream("/res/properties/config.properties"));
		labelsProperties.load(getClass().getResourceAsStream("/res/properties/label.properties")); 
		validationMessages.load(getClass().getResourceAsStream("/res/properties/validation.properties")); 
	}

	/**
	 * Calls the first app view (login)
	 * @param primaryStage
	 * @throws IOException
	 */
	void callSecurityAccess(Stage primaryStage) throws IOException {

		Parent root = FXMLLoader.load(getClass().getResource(getConfigProperties().getProperty("login.view")));
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource(getConfigProperties().getProperty("application.css")).toExternalForm());
		
		primaryStage.setTitle(getLabelsProperties().getProperty("Login.view.title"));
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {

		
	    initORMInstrumentation();
	
		// Launch JavaFX app
		launch(args);
		// Close DB conection
		DBClient.close();
		//List<Integer> li = PropertiesRulesParser.parsePropertieRule("*", 1, 2);
		
		//System.out.println("test");
		//GasTimSimulator gs = new GasTimSimulator();
		//gs.runtest();
	}
	
	/**
	 * Static java lite instrumentation
	 */
	private static void initORMInstrumentation() {
		// TODO: get project path, and use relative in .bat
		String projectPath = System.getProperty("user.dir");
		Runtime rn=Runtime.getRuntime();
		String path;
		try {
			
			path="cmd /c start " + projectPath + "\\res\\intr_script.bat";
			rn.exec(path);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		
		try {
			projectPath = System.getProperty("user.dir");
			path= projectPath + "/res/instr_script_unix.sh";
			System.out.println(path);
			rn.exec(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the application config properties
	 * @return Application properties
	 */
	public static Properties getConfigProperties() {
		return configProperties;
	}
	
	/**
	 * Returns the application labels properties
	 * @return Application properties
	 */
	public static Properties getLabelsProperties() {
		return labelsProperties;
	}
	
	/**
	 * Returns the validation messages properties
	 * @return Application properties
	 */
	public static Properties getValidationMessages() {
		return validationMessages;
	}
}
