package application.controllers.fields;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import netscape.javascript.JSObject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.models.BaseModel;
import application.models.GeologicalUnit;
import application.models.Well;
import application.models.Field;
import application.utils.FieldHelper;
import application.utils.TableHelper;

import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.event.UIEventHandler;

/**
 * Fields screen controller
 * 
 * @author CV
 *
 */
public class FieldsController implements Initializable, ControlledScreen , MapComponentInitializedListener{
	
	MainWorkflow workflowController;
	 @FXML
	    private Button b_show_pane_new_well;
	    
	    @FXML
	    private Button b_add_well;
	    
	    @FXML
	    private TextField nameText;
	    
	    @FXML
	    private TextField sectorText;
	    
	    @FXML
	    
	    private TextField latText;
	    @FXML
	    private TextField longText;
	    
	    
	    @FXML
	    private GoogleMapView mapView;
	    @FXML
	    TableView<Well> wellTable;
	    
	    @FXML
	    TableColumn<Well, String> wellColumn;
	    
	    @FXML
	    TableColumn<Well, String> latlongColumn;
	    
	    /*@FXML
	    TableColumn<Well, Double> radiusColumn;*/
	    
	    private ObservableList<Well> tableData;
	    private GoogleMap map;
	    private Field firstField;
	    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Specific options for user/roles?
		 mapView.addMapInializedListener(this);
		
	
	}

	/**
	 * Sets the workflow controller.
	 */
	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = (MainWorkflow) workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	@Override
	public void mapInitialized() {
		// TODO Auto-generated method stub
		//Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();
        
        LatLong initial_point = new LatLong(4.613401, -74.082591); //Bogota
        
        mapOptions.center(initial_point)
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(true)
                .panControl(true)
                .rotateControl(false)
                .scaleControl(true)
                .streetViewControl(false)
                .zoomControl(true)
                .zoom(7);
                   
        map = mapView.createMap(mapOptions);
        
        List <Well> wells = Well.findAll();
		List <Field> fields = Field.findAll();
		
		for(Well w: wells){
			this.addWellMarker(new LatLong(w.getDouble("lat"), w.getDouble("long_")),"Well: " + w.getString("name"), true, w);
		}
		int cnt=0;
		for(Field f: fields) {
			this.addReservoirPoly(f.getString("name"),f.getString("poly_points"), cnt++>0?false:true, f);
		}
		
		
		this.initializeTable();
	}
	
	public void initializeTable() {
		
		//Tabla
		List <Field> fields = Field.findAll();
		Field sample = fields.get(0);
		this.firstField = sample;
		List <Well> sample_wells = sample.getAll(Well.class);
		 
		List<TableColumn<Well, ?>> cols = new ArrayList<>();

		cols.add(this.wellColumn);
		cols.add(this.latlongColumn);
		//cols.add(this.radiusColumn);
		this.wellColumn.setEditable(false);
		this.latlongColumn.setEditable(false);
		//this.radiusColumn.setEditable(false);
		
		ObservableList<TableColumn<Well, ?>> colsList = FXCollections
				.observableList(cols);

		List<String> atts = new ArrayList<>();

		atts.add("name");
		atts.add("latlong");
		//atts.add("radius");

		List<Class> attsTypes = new ArrayList<>();

		attsTypes.add(String.class);
		attsTypes.add(String.class);
		//attsTypes.add(double.class);

		TableHelper<Well> helper = new TableHelper<Well>(wellTable,
				colsList, atts, attsTypes);

		tableData = FXCollections
				.observableArrayList(sample_wells);
		wellTable.setItems(tableData);
	}
	
	public void addWellMarker(LatLong well_position, String well_name, boolean well_icon, BaseModel data){
        //Add a marker to the map
        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position( well_position )
                    .visible(Boolean.TRUE)
                    .title(well_name);
        if (well_icon)           
        	markerOptions.icon("https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/location-24-32.png");
        
        CustomMarker marker = new CustomMarker(markerOptions, data);
        map.addMarker(marker);  
        
        map.addUIEventHandler(marker, UIEventType.click, 
        		new UIEventHandler() {

			@Override
			public void handle(JSObject arg0) {
				// TODO Auto-generated method stub
				System.out.println("click hand " + arg0);
				//Marker m = (Marker) arg0;
			}
		
		
		}
        		
        		);
        //  this.addReservoirPoly("name", "4.209786,-71.807327;4.334408,-71.777115;4.315237,-71.466751;4.164588,-71.667252");
    }
    
    public void addReservoirPoly(String field_name, String points_str, boolean show_marker, BaseModel data) {
    	LatLong [] a = this.getPolyPoints(points_str);
    	MVCArray b = new MVCArray(a);
    	
    	PolylineOptions options = new PolylineOptions();
    	options.clickable(false).draggable(false).editable(false).path(b).strokeColor("#944ab3");
    	Polyline pl = new Polyline(options);
    	map.addMapShape(pl);
    	if(show_marker)
    	this.addWellMarker(a[0],field_name , false, data);
    }
    
    
    public LatLong[] getPolyPoints(String points_str) {
    	
    	String [] pairs = points_str.split(";");
    	LatLong []plist = new LatLong[pairs.length+1];
    	int cnt = 0;
    	for(String pair: pairs) {
    		String [] p = pair.split(",");
    		double lat = Double.parseDouble(p[0]);
    		double lng = Double.parseDouble(p[1]);
    		LatLong lg = new LatLong(lat, lng);
    		plist[cnt++] = lg;
    	}
    	plist[cnt] = plist[0];
    	return plist;
    }
    
    @FXML
    private void addButtonAction(MouseEvent ev){
    	Well new_well = new Well();
    	double lat = FieldHelper.getDouble(this.latText.getText());
    	double long_ = FieldHelper.getDouble(this.longText.getText());
    	new_well.set("name",this.nameText.getText() );
    	new_well.set("wellbore_radius",0.0);
    	new_well.set("lat",lat);
    	new_well.set("long_",long_);
    	this.firstField.add(new_well);
    	new_well.saveIt();
    	this.addWellMarker(new LatLong(lat,  long_), this.nameText.getText(), true, new_well);
    }
    
    public class  CustomMarker extends Marker {
    	private BaseModel data;
		public CustomMarker(MarkerOptions markerOptions, BaseModel data) {
			super(markerOptions);
			// TODO Auto-generated constructor stub
			this.data = data;
		}
    	
    }
    
    
}
