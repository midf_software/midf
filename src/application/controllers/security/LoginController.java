package application.controllers.security;

import java.io.IOException;

import application.controllers.AppMain;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Security login class
 * Validates users and roles and calls the main application GUI
 * @author CV
 *
 */
public class LoginController {

	/** password text field. */
	@FXML
	private PasswordField securityText;

	/** Validation label. */
	@FXML
	private Label invalidCodeLabel;

	/**
	 * Continue button action.
	 * Validates the security code.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	protected void continueAction(MouseEvent event) throws IOException {
			callMainGUI();
	}

	/**
	 * Enter* key action TODO: focus manager
	 * @param event
	 * @throws IOException
	 */
	@FXML
	protected void checkKeyAction(KeyEvent event) throws IOException {
		if (event.getCharacter().equals("\r")) {
			callMainGUI();
		}
	}

	/**
	 * Validatesthe security code and calls the main interface
	 * @throws IOException
	 */
	void callMainGUI() throws IOException {

		// TODO: DB / Roles
		if (securityText.getText().equals(AppMain.getConfigProperties().get("security.code"))) {
			Stage primaryStage = (Stage) securityText.getScene().getWindow();

			Parent root = FXMLLoader.load(getClass().getResource(
					AppMain.getConfigProperties().getProperty("main.view")));

			Scene scene = new Scene(root);
			scene.getStylesheets().add(
					getClass().getResource(AppMain.getConfigProperties().getProperty("application.css"))
							.toExternalForm());

			primaryStage.setTitle(AppMain.getLabelsProperties().getProperty("main.view.title"));
			primaryStage.setScene(scene);
			primaryStage.setResizable(true);
			primaryStage.setMaximized(true);
			Screen scr = Screen.getPrimary();
			Rectangle2D bounds = scr.getVisualBounds();
			primaryStage.setX(bounds.getMinX());
			primaryStage.setY(bounds.getMinY());
			primaryStage.setWidth(bounds.getWidth());
			primaryStage.setHeight(bounds.getHeight());
			primaryStage.show();

		} else {
			invalidCodeLabel.setVisible(true);
		}
	}
}
