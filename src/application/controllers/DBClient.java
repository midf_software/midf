package application.controllers;

import org.javalite.activejdbc.Base;

public class DBClient {
	static boolean connected = false;
	
	public static void connect() {
		try {
			Base.open("org.mariadb.jdbc.Driver", "jdbc:mysql://localhost/midf", "root", "root");
			connected = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Warning: No Database connection");
		}
	}
	
	public static void close() {
		try {
			Base.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		connected = false;
	}
}
