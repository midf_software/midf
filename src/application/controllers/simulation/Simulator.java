package application.controllers.simulation;

import application.models.BaseData;
import application.models.Project;
import application.models.Simulation;

public abstract class Simulator {
	public  abstract boolean run(Simulation simulation, BaseData baseData);
}