package application.controllers.simulation;
import java.util.*;
import java.lang.*;
import java.io.*;

public class SkinFactorCalculator
{
	
	public double calculateSkin(double qt, double bhfp, double h, double pr, double sg, double sw, double bg, double bo, double bw, double ug, double uw, double uo, double e_radius, double well_radius, double radial_perm) {
		double S = 0.0;
		double J, B, m;
		double so = 1 - (sg + sw);
		J = qt / (pr - bhfp);
		B = sg * bg + so * bo + sw * bw;
		m = Math.exp(sg*Math.log(ug) + so*Math.log(uo) + sw*Math.log(uw));
		double Kj = (141.2 * J * B * m *(Math.log(e_radius/well_radius) - (3.0/4.0))) / h;
		S = ((radial_perm / Kj) - 1 ) * (Math.log(e_radius/well_radius)- (3.0/4.0));
		return S;
	}
}