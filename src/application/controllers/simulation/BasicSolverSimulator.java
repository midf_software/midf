package application.controllers.simulation;
import java.io.File;
import java.nio.file.Files;
import java.util.List;

import application.models.BaseData;
import application.models.FormationCondition;
import application.models.Grid;
import application.models.InitialCondition;
import application.models.GeologicalUnit;
import application.models.Layer;
import application.models.Project;
import application.models.PvtDensity;
import application.models.Reservoir;
import application.models.Scenario;
import application.models.Simulation;
import application.models.SimulationCondition;

public class BasicSolverSimulator extends Simulator{
	final static String sharedLib = "gastim";//libgastim
	static boolean libLoaded = false;
	
	private native int rungastim(
			// Reservoir
			 double formation_thickness, double external_radius, 
			 double wellbore_radius, double formation_depth_top,
			 double rock_density, 
			 // Simulation conditions
			 double  max_timestep, double min_timestep,
			 double total_time, double max_pressure_delta,
			 double max_saturation_delta, double max_rs_delta,
			 double dt_accel_factor, double  relative_error,
			 double epsilon, 
			 double num_iterations,
			 // Grid data
			 double radial_numblocks, double angular_numblocks, double vertical_numblocks,
			 double grid_thickness[],
			 // Formation conditions
			 double porosity[], double perm_i[], double perm_j[], double perm_k[],
			 // Initial conditions
			 double initial_pressure[], double gas_saturation[], double water_saturation[],
			 //KRS
			 double gasliquid_data[], double wateroil_data[],
			 //PVT
			 double pvt_data[], double gas_density, double oil_density, double water_density,
			 //Skin and perforation
			 double perforation_data[], double skin_data[], double operating_constraints_data[]
			 ); 
	
	//DLL load
	static {
		try {
			System.loadLibrary(sharedLib);
			libLoaded = true;
		} catch (UnsatisfiedLinkError e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
			System.out.println("Unable to load DLL: " + sharedLib);
		}	
	}
	
	
	public boolean  run(Scenario scenario, Simulation simulation, BaseData baseData) {
		BaseData inputData =baseData;
		Grid grid = inputData.getGrid();
		
		Reservoir rs = inputData.getReservoir();
		SimulationCondition sc =simulation.getSimulationCondition();
		List <double[]> form_conds = inputData.getBlocksFormationConditions();
		List <double[]> initial_conds = inputData.getBlocksInitialConditions();
		double[] gasliquid_data = inputData.getKrsGasLiquidValues();
		double[] wateroil_data = {0.0, 0.0, 0.0, 0.0}; //inputData.getKrsWaterOilValues();
		double[] pvt_values = inputData.getPvtValues();
		PvtDensity pd = inputData.getPvtDensities();
		double[] skin_data = inputData.getSkinConditionsValues();
		double[] perf_data = grid.getLayersPerforations();
		double[] operating_data = inputData.getOperationConditionsValues(); 
		//Run gastim
		if (libLoaded){
			try {
				//run gastim
				int result = 0;
				result = rungastim(rs.getFormationThickness(), rs.getExternalRadius(), //Reservoir 
						  rs.getWellboreRadius(), rs.getFormationDepthTop(), 
						  rs.getRockDensity(),
						  // Sim conds
						  sc.getMaxTimestep(), sc.getMinTimestep(), 
						  sc.getTotalTime(), sc.getMaxPressureDelta(), 
						  sc.getMaxSaturationDelta(), sc.getMaxRsDelta(), 
						  sc.getDtAccelFactor(), sc.getRelativeError(), 
						  sc.getEpsilon(), sc.getNumIterations(),
						  // Grid data
						  grid.getRadialNumblocks(), grid.getAngularNumblocks(), grid.getVerticalNumblocks(),
						  grid.getLayersThickness(),
						  // Formation conditions
						  form_conds.get(0), form_conds.get(1), form_conds.get(2), form_conds.get(3),
						  // Initial conditions
						  initial_conds.get(0), initial_conds.get(1), initial_conds.get(2),
						  // KRS
						  gasliquid_data, wateroil_data,
						  // PVT
						  pvt_values, pd.getGasDensity(), pd.getOilDensity(), pd.getWaterDensity(),
						  // Skin and perforation
						  perf_data, skin_data, operating_data
						);
						//result = 0;  
				if (result == 0) {
					throw new Exception(); //TODO custom simulator exception
				}
				this.saveData(scenario);
				System.out.println("Returned to Java");
			}
			catch(Exception e) {
				e.printStackTrace();
				System.out.println("Error running GasTim");
				return false;
			}
			return true; // Successful simulation run
		}
		else{
			return false;
		}
	}
	
	public void runtest() {
		int result = 0;
		double [] fc = {1.0,2.0, 3.0};
		
		try {
			System.out.println("calling");
			result = rungastim(1.0, 1.0, //Reservoir 
					  1.0, 1.0, 
					  1.0,
					  // Sim conds
					  1.0,1.0, 
					  1.0, 1.0, 
					  1.0, 1.0, 
					  1.0, 1.0, 
					  1.0, 1.0,
					  // Grid data
					  1.0, 1.0, 1.0,
					  fc,
					  // Formation conditions
					  fc, fc, fc,fc,
					  // Initial conditions
					 fc,fc,fc,
					  // KRS
					 fc, fc,
					 //PVT
					  fc, 1.0, 1.0, 1.0,
					  //Skin and perforation
					  fc, fc, fc
					);
			System.out.println("end calling");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Almacenar output
	 */
	public void saveData(Scenario sc) {
		//TODO temporal renombrar archivos de salida con id de escenario
		renameFile("./Results/Cumulative Gas SC.txt", "./Results/"+String.valueOf(sc.getPk())+"_Cumulative Gas SC.txt");
		renameFile("./Results/Cumulative Oil SC.txt", "./Results/"+String.valueOf(sc.getPk())+"_Cumulative Oil SC.txt");
		renameFile("./Results/Cumulative Water SC.txt", "./Results/"+String.valueOf(sc.getPk())+"_Cumulative Water SC.txt");
		
		renameFile("./Results/Gas Pressure.txt", "./Results/"+String.valueOf(sc.getPk())+"_Gas Pressure.txt");
		renameFile("./Results/Gas Saturation.txt", "./Results/"+String.valueOf(sc.getPk())+"_Gas Saturation.txt");
		
		renameFile("./Results/Oil Pressure.txt", "./Results/"+String.valueOf(sc.getPk())+"_Oil Pressure.txt");
		renameFile("./Results/Oil Saturation.txt", "./Results/"+String.valueOf(sc.getPk())+"_Oil Saturation.txt");


		renameFile("./Results/Water Pressure.txt", "./Results/"+String.valueOf(sc.getPk())+"_Water Pressure.txt");
		renameFile("./Results/Water Saturation.txt", "./Results/"+String.valueOf(sc.getPk())+"_Water Saturation.txt");
		
			
	}
	
	public void renameFile(String oldname, String newname) {
		// File (or directory) with old name
	    File file = new File(oldname);

	    // File (or directory) with new name
	    File file2 = new File(newname);
	    if(file2.exists()) {
	    	file2.delete();
	    }

	    // Rename file (or directory)
	    boolean success = file.renameTo(file2);
	    if (!success) {
	        // File was not successfully renamed
	    }
	}

	@Override
	public boolean run(Simulation simulation, BaseData baseData) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	

}
