package application.controllers.simulation.results;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import application.controllers.ControlledScreen;
import application.controllers.MainWorkflow;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflow;
import application.models.Scenario;
import application.utils.DateUtils;
import application.utils.PlatformHelper;

public class ResultsController implements Initializable, ControlledScreen {

	/** Workflow controller. */
	private Workflow workflowController;

	/** Vertical blocks amount */
	private double layers;

	@FXML
	private LineChart<Number, Number> gasChart;

	@FXML
	private LineChart<Number, Number> oilChart;

	@FXML
	private LineChart<Number, Number> waterChart;

	@FXML
	private ChoiceBox<String> propertiesChoiceBox;

	@FXML
	private Slider layerSlider;

	@FXML
	private CheckBox AllLayersCheckbox;

	@FXML
	private Slider timeSlider;

	@FXML
	private Canvas propertyPlottingCanvas;

	@FXML
	private CheckBox cumulativeCheckbox;
	@FXML
	private NumberAxis oilAxis;

	/**
	 * Properties to select in the choice box
	 */
	private HashMap<String, String> label2propMap = new HashMap<>();

	private final String GAS_PRESSURE_PROPERTY = "gas_pressure";
	private final String GAS_SATURATION_PROPERTY = "gas_saturation";
	private final String OIL_PRESSURE_PROPERTY = "oil_pressure";
	private final String OIL_SATURATION_PROPERTY = "oil_saturation";
	private final String WATER_PRESSURE_PROPERTY = "water_pressure";
	private final String WATER_SATURATION_PROPERTY = "water_saturation";

	private final String GAS_PRESSURE_LABEL = "Gas pressure";
	private final String GAS_SATURATION_LABEL = "Gas saturation";
	private final String OIL_PRESSURE_LABEL = "Oil pressure";
	private final String OIL_SATURATION_LABEL = "Oil saturation";
	private final String WATER_PRESSURE_LABEL = "Water pressure";
	private final String WATER_SATURATION_LABEL = "Water saturation";

	private Scenario sc;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		/*
		 * Fill the properties to label map
		 */
		label2propMap.put(GAS_PRESSURE_LABEL, GAS_PRESSURE_PROPERTY);
		label2propMap.put(GAS_SATURATION_LABEL, GAS_SATURATION_PROPERTY);
		label2propMap.put(OIL_PRESSURE_LABEL, OIL_PRESSURE_PROPERTY);
		label2propMap.put(OIL_SATURATION_LABEL, OIL_SATURATION_PROPERTY);
		label2propMap.put(WATER_PRESSURE_LABEL, WATER_PRESSURE_PROPERTY);
		label2propMap.put(WATER_SATURATION_LABEL, WATER_SATURATION_PROPERTY);
		
		//oilTimeAxis.setAutoRanging(true);
		//oilTimeAxis.setTickLabelRotation(90);
		
		/*
		 * choicebox settings
		 */
		propertiesChoiceBox.getItems().addAll(GAS_PRESSURE_LABEL,
				GAS_SATURATION_LABEL, OIL_PRESSURE_LABEL, OIL_SATURATION_LABEL,
				WATER_PRESSURE_LABEL, WATER_SATURATION_LABEL);

		propertiesChoiceBox.getSelectionModel().selectedIndexProperty()
				.addListener(new ChangeListener<Number>() {

					public void changed(ObservableValue ov, Number value,
							Number new_value) {

						updateGraphic(label2propMap.get(propertiesChoiceBox
								.getItems().get((int) new_value)),
								(int) layerSlider.getValue(), (int) timeSlider
										.getValue());

					}

				});

		AllLayersCheckbox.selectedProperty().addListener(
				new ChangeListener<Boolean>() {

					public void changed(ObservableValue ov, Boolean old_val,
							Boolean new_val) {
						double layerValue = layerSlider.getValue();
						if (new_val) {
							layerValue = -1; // Todas las capas
							System.out.println("All layers");
						}
						updateGraphic(label2propMap.get(propertiesChoiceBox
								.getSelectionModel().getSelectedItem()),
								layerValue, timeSlider.getValue());
					}
				});

		cumulativeCheckbox.selectedProperty().addListener(
				new ChangeListener<Boolean>() {

					public void changed(ObservableValue ov, Boolean old_val,
							Boolean new_val) {
						double layerValue = layerSlider.getValue();
						if (new_val) {

						}
						PlatformHelper.run(() -> {loadProductionCharts(new_val);});
						
					}
				});

	}

	/**
	 * Crea los charts de produccion
	 */
	public void loadProductionCharts(boolean cum) {
		/*
		 * production data (oil. gas. water) laoded from file
		 */
		List<double[]> points;
		double tmp = 0;
		double val = 0;
		XYChart.Series series = new XYChart.Series();
		series.setName("Gas");

		points = getProductionResultsFromFile(String.valueOf(sc.getPk())
				+ "_Cumulative Gas SC.txt", cum);
		for (int i = 0; i < points.size(); i++) {
			double[] point = points.get(i);
			if (cum) {
				val = point[1];
			} else {
				val = point[1] - tmp;
				tmp = point[1];

			}
			Date dt = (DateUtils.addDays(DateUtils.stringToDate(this.sc.getDate()), (int)point[0]));
			series.getData().add(new XYChart.Data(dt, val));
		}

		tmp = 0;
		val = 0;
		gasChart.setCreateSymbols(false);
		gasChart.getData().clear();
		gasChart.getData().add(series);
	
		XYChart.Series series2 = new XYChart.Series();
		series2.setName("Oil");
		points = getProductionResultsFromFile(String.valueOf(sc.getPk())
				+ "_Cumulative Oil SC.txt", cum);
		for (int i = 0; i < points.size(); i++) {
			double[] point = points.get(i);
			if (cum) {
				val = point[1];
			} else {
				val = point[1] - tmp;

				tmp = point[1];

			}
			Date dt = (DateUtils.addDays(DateUtils.stringToDate(this.sc.getDate()), (int)point[0]));
			//series2.getData().add(new XYChart.Data(point[0], val));
			series2.getData().add(new XYChart.Data(dt, val));
		}

		oilChart.setCreateSymbols(false);
		oilChart.getData().clear();
		oilChart.getData().add(series2);
		if(cum) {
			oilChart.setTitle("Oil  Production (bbl) vs Time");
			oilAxis.setLabel("Production (bbl)");
		}
		else {
			oilChart.setTitle("Qo (BPD) vs Time");
			oilAxis.setLabel("Qo (BPD)");
		}
		
		tmp = 0;
		val = 0;
		XYChart.Series series3 = new XYChart.Series();
		series3.setName("Water");
		points = getProductionResultsFromFile(String.valueOf(sc.getPk())
				+ "_Cumulative Water SC.txt", cum);
		for (int i = 0; i < points.size(); i++) {
			double[] point = points.get(i);
			if (cum) {
				val = point[1];
			} else {
				val = point[1] - tmp;
				tmp = point[1];
			}
			Date dt = (DateUtils.addDays(DateUtils.stringToDate(this.sc.getDate()), (int)point[0]));
			series3.getData().add(new XYChart.Data(dt, val));
		}

		waterChart.setCreateSymbols(false);
		waterChart.getData().clear();
		waterChart.getData().add(series3);

	}

	public void setLayers(double layers) {
		this.layers = layers;

		/*
		 * Sliders settings
		 */
		layerSlider.setMin(1);
		layerSlider.setMax(layers);
		layerSlider.setValue(1);
		layerSlider.setShowTickLabels(true);
		layerSlider.setShowTickMarks(true);
		layerSlider.setMajorTickUnit(1);
		layerSlider.setMinorTickCount(0);
		layerSlider.setBlockIncrement(1);
		layerSlider.setSnapToTicks(true);

		timeSlider.setMin(1);
		timeSlider.setMax(13); // TODO: Total days
		timeSlider.setValue(1);
		timeSlider.setShowTickLabels(true);
		timeSlider.setMajorTickUnit(13);
		timeSlider.setMinorTickCount(12);
		timeSlider.setBlockIncrement(1);
		timeSlider.setSnapToTicks(true);

		// Listen for Slider value changes
		layerSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {

				updateGraphic(label2propMap.get(propertiesChoiceBox
						.getSelectionModel().getSelectedItem()),
						(double) newValue, timeSlider.getValue());
			}
		});

		timeSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {

				updateGraphic(label2propMap.get(propertiesChoiceBox
						.getSelectionModel().getSelectedItem()), layerSlider
						.getValue(), (double) newValue);
			}
		});

		// Select the first item to call the first chart
		propertiesChoiceBox.getSelectionModel().select(0);
	}

	/**
	 * Uses selectedProperty, layerSlider and timeSlider values to show the
	 * respective graphic.
	 */
	private void updateGraphic(String selectedProperty, double layerValue,
			double timeValue) {
		// plot2DMplot(selectedProperty, layerSlider.getValue(),
		// timeSlider.getValue());
		String property = selectedProperty;
		// TODO llamar cuando AllLayerscheckbox se clickee / cambie de valor
		// TODO si Alllayerscheckbox es TRUE layervalue = 1
		int lv = (int) layerSlider.getValue();
		if (AllLayersCheckbox.isSelected()) {
			lv = -1;
		}
		long base_tstamp = DateUtils.getTimeStamp(DateUtils.stringToDate(this.sc.getDate()));
		GridPlotMPL.plot2DMplot(property, lv, (int) timeSlider.getValue(),
				sc.getPk(), base_tstamp);

		GraphicsContext gc = propertyPlottingCanvas.getGraphicsContext2D();
		String ly = ((Integer) (int) layerValue).toString();
		String tm = ((Integer) (int) timeValue).toString();
		System.out.println(ly);
		System.out.println(tm);
		Image im = new Image("file:./res/props/render.png");

		double x = (propertyPlottingCanvas.getWidth() - im.getWidth()) / 2;
		double y = (propertyPlottingCanvas.getHeight() - im.getHeight()) / 2;

		gc.clearRect(0, 0, propertyPlottingCanvas.getWidth(),
				propertyPlottingCanvas.getHeight());

		gc.drawImage(im, x, y);

	}

	public List<double[]> getProductionResultsFromFile(String filename,
			boolean cum) {

		int numline = 0;
		int startline = 6; // Linea donde se empiezan a imprimir los resultados
		List<Double> x = new ArrayList<Double>();
		List<Double> y = new ArrayList<Double>();
		double[] point;
		List<double[]> points = new ArrayList<double[]>();

		try {
			BufferedReader br = new BufferedReader(new FileReader("./Results/"
					+ filename));
			// BufferedReader br = new BufferedReader(new
			// FileReader("./res/solver/Results/"+filename));
			// TODO correct path

			String line = br.readLine();

			while (line != null) {
				numline++;
				if (numline > startline) {
					line = line.trim();
					int s = line.indexOf(' '); // El primer espacio es el
												// separador
					point = new double[2];
					point[0] = (Double.parseDouble(line.substring(0, s))); // Get
																			// X
																			// value
					point[1] = (Double.parseDouble(line.substring(s))); // Get Y
																		// value
					points.add(point); // Add point
				}
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		return points;
	}

	@FXML
	protected void backToScenarioButtonAction(MouseEvent ev) {
		Scenario currentScenario = ((MainWorkflow) workflowController)
				.getProjectWorkflow().getProject().getCurrent();
		if (currentScenario != null) {
//			((MainWorkflow) workflowController).getProjectWorkflow()
//					.getProject().setCurrent(currentScenario);
			ScenaryWorkflow scenaryWorkflow = new ScenaryWorkflow(
					workflowController.getMainController(),
					((MainWorkflow) workflowController).getProjectWorkflow()
							.getProject(), currentScenario);
			((MainWorkflow)workflowController).getProjectWorkflow().setScenaryWorkflow(scenaryWorkflow);
			scenaryWorkflow.initScenarioWorkflow();
		}
	}

	@Override
	public void setWorkflowController(Workflow workflowController) {
		this.workflowController = workflowController;
	}

	@Override
	public Workflow getWorkflowController() {
		return workflowController;
	}

	public Scenario getScenario() {
		return sc;
	}

	public void setScenario(Scenario sc) {
		this.sc = sc;
	}
	 @FXML
	    private void exportEvent(ActionEvent event) {
		 List<double[]> points_gas = getProductionResultsFromFile(String.valueOf(sc.getPk())
					+ "_Cumulative Gas SC.txt", false);
		 List<double[]> points_oil = getProductionResultsFromFile(String.valueOf(sc.getPk())
					+ "_Cumulative Oil SC.txt", false);
		 
		 String cadena = "";
		 double tmp = 0;
		 
		 cadena += "Oil Production \r\n";
		 cadena += "Time [days]\t Qo[BPD] \r\n";
		 tmp =0;
		 
		 for (int i = 0; i < points_oil.size(); i++) {
				double[] point = points_oil.get(i);
				cadena += String.format("%s \t %s \r\n", point[0], point[1] - tmp);
				tmp = point[1];
			}
		 cadena += "Gas Production \r\n";
		 cadena += "Time [days]\t Production \r\n";
		 tmp =0;
		 
		 for (int i = 0; i < points_gas.size(); i++) {
				double[] point = points_gas.get(i);
				cadena += String.format("%s \t %s \r\n", point[0], point[1] - tmp);
				tmp = point[1];
			}
		 
		 JFileChooser fileChooser = new JFileChooser();
	        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("todos los archivos *.csv", "csv","CSV"));//filtro para ver solo archivos .csv
	        int seleccion = fileChooser.showSaveDialog(null);
	        try{
	            if (seleccion == JFileChooser.APPROVE_OPTION){//comprueba si ha presionado el boton de aceptar
	                File JFC = fileChooser.getSelectedFile();
	                String PATH = JFC.getAbsolutePath();//obtenemos el path del archivo a guardar
	                PrintWriter printwriter = new PrintWriter(JFC);
	                printwriter.print(cadena);//escribe en el archivo todo lo que se encuentre en el JTextArea
	                printwriter.close();//cierra el archivo

	                //comprobamos si a la hora de guardar obtuvo la extension y si no se la asignamos
	                if(!(PATH.endsWith(".csv"))){
	                    File temp = new File(PATH+".csv");
	                    JFC.renameTo(temp);//renombramos el archivo
	                }

	                JOptionPane.showMessageDialog(null,"Guardado exitoso!", "Guardado exitoso!", JOptionPane.INFORMATION_MESSAGE);
	            }
	        }catch (Exception e){//por alguna excepcion salta un mensaje de error
	            JOptionPane.showMessageDialog(null,"Error al guardar el archivo!", "Oops! Error", JOptionPane.ERROR_MESSAGE);
	        }
	 }
}