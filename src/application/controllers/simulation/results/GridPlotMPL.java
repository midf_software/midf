package application.controllers.simulation.results;

import java.io.IOException;

public class GridPlotMPL {
	public static void plot2DMplot(String propertieName, int layerNum, int timeNum, int id, long base_tstamp){
		Runtime r = Runtime.getRuntime();
		String[] args = {"python", "./res/scripts/gridplot_mpl.py", propertieName, ((Integer)layerNum).toString(), ((Integer)timeNum).toString(), ((Integer)id).toString() , ((Long)base_tstamp).toString()  };
		System.out.println(args[1] + args[2] + args[3]);
		
		try {
			Process p = r.exec(args);
			int exitCode = p.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
