package application.controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import application.controllers.factory.ScenarioExecutionFactory;
import application.controllers.factory.ScenarioResultsFactory;
import application.controllers.results.ScenarioResultsManager;
import application.execution.ScenarioExecutionManager;

/**
 * Class controlling the main view.
 * 
 * @author CV
 *
 */
public class MainController implements Initializable {

	/** Workspace panel showing some GUI screens and panels */
	@FXML
	private StackPane workspacePanel;

	/** Toolbar used to navigate on scenary workflow */
	@FXML
	private Accordion projectInfoToolbar;

	/** Panel containing the scenary toolba r */
	@FXML
	private GridPane scenaryToolbarPanel;

	/** Scenary execution toolbar */
	@FXML
	private AnchorPane scenaryProcessToolBarPanel;

	@FXML
	private TitledPane baseDataInfoTab;

	@FXML
	private TitledPane scenarioInfoTab;

	@FXML
	private AnchorPane scenarioInfoPanel;

	@FXML
	private Button generalDataButton;
	@FXML
	private Button reservoirDataButton;
	@FXML
	private Button gridDataButton;
	@FXML
	private Button guDataButton;
	@FXML
	private Button rockDataButton;
	@FXML
	private Button initialConditionsDataButton;
	@FXML
	private Button fluidDataButton;
	@FXML
	private Button permeabilitiesDataButton;
	@FXML
	private Button perforationsDataButton;
	@FXML
	private Button constraintsDataButton;
	@FXML
	private Button skinDataButton;

	/** Workflow controller for main options and screens */
	private MainWorkflow mainWorkflowController;

	private boolean initialized = false;

	/**
	 * Initializes the main controller, Only called after login screen
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		// Inits the workflow controller
		// Si este controlador ya esta instanciado y se asigna a otro fxml se
		// vuelve a inicializar
		// Con este flag evitamos reinstanciar MainWorkFlow
		if (this.initialized) {
			return;
		}
		mainWorkflowController = new MainWorkflow(this);
		mainWorkflowController.setInternalPanel(AppMain.getConfigProperties()
				.getProperty("dashboard.view"));
		this.initialized = true;
	}

	/**
	 * Enables/disables the project/simulation sideboard and simulation button
	 * 
	 * @param enable
	 */
	public void enableProjectToolbar(boolean enable) {
		if (enable) {
			if (scenaryToolbarPanel.isVisible())
				return;
			scenaryToolbarPanel.setVisible(true);
			scenaryToolbarPanel.setMaxWidth(500);
			scenaryProcessToolBarPanel.setVisible(true);
			scenaryProcessToolBarPanel.setMaxHeight(40);
		} else {
			if (!scenaryToolbarPanel.isVisible())
				return;
			scenaryToolbarPanel.setVisible(false);
			scenaryToolbarPanel.setMaxWidth(0);
			scenaryProcessToolBarPanel.setVisible(false);
			scenaryProcessToolBarPanel.setMaxHeight(0);
		}
	}

	/**
	 * New project action instances a new project and shows the initial project
	 * form, general data
	 * 
	 * @param event
	 */
	@FXML
	protected void newProjectButtonAction(MouseEvent event) {
		mainWorkflowController.instanceNewProject();
	}

	/**
	 * Dashboard button action, shows the dashboard screen
	 * 
	 * @param event
	 */
	@FXML
	protected void dashboardButtonAction(MouseEvent event) {
		mainWorkflowController.setInternalPanel(AppMain.getConfigProperties()
				.getProperty("dashboard.view"));
	}

	/**
	 * Help button action, shows the help pdf
	 * 
	 * @param event
	 */
	@FXML
	protected void helpButtonAction(MouseEvent event) {
		// TODO: Help system
		try {

			String projectPath = System.getProperty("user.dir");
			File helpFile = new File(projectPath + "\\res\\AyudaMIDF.pdf");
			
			if (helpFile.exists()) {

				Process p = Runtime
						.getRuntime()
						.exec("rundll32 url.dll,FileProtocolHandler " + helpFile);
				p.waitFor();

			} else {

				System.out.println("File is not exists");

			}

			System.out.println("Done");

		} catch (Exception ex) {
			mainWorkflowController.showDialog(AppMain.getValidationMessages().getProperty("open_file_error"));
		}
	}
	
	/**
	 * Load project button action, shows the load project screen
	 * 
	 * @param event
	 */
	@FXML
	protected void loadButtonAction(MouseEvent event) {
		mainWorkflowController.setInternalPanel(AppMain.getConfigProperties()
				.getProperty("load.project.view"));
	}

	@FXML
	protected void setGeneralDataScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"scenary.general.data.view"),
						AppMain.getLabelsProperties().getProperty(
								"general.data.title"));
	}

	@FXML
	protected void setReservoirScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"reservoir.view"),
						AppMain.getLabelsProperties().getProperty(
								"reservoir.title"));
	}

	@FXML
	protected void setGridDataScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"grid.data.view"),
						AppMain.getLabelsProperties().getProperty(
								"grid.data.title"));
	}

	@FXML
	protected void setGUDataScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"gu.data.view"),
						AppMain.getLabelsProperties().getProperty(
								"gu.data.title"));
	}

	@FXML
	protected void setRockPropertiesScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"rock.properties.view"),
						AppMain.getLabelsProperties().getProperty(
								"rock.properties.title"));
	}

	@FXML
	protected void setInitialConditionsScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"initial.conditions.view"),
						AppMain.getLabelsProperties().getProperty(
								"initial.conditions.title"));
	}

	@FXML
	protected void setSimulationConditionsScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"simulation.conditions.view"),
						AppMain.getLabelsProperties().getProperty(
								"simulation.conditions.title"));
	}

	@FXML
	protected void setPVTScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty("pvt.view"),
						AppMain.getLabelsProperties().getProperty("pvt.title"));
	}

	@FXML
	protected void setKRSScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty("krs.view"),
						AppMain.getLabelsProperties().getProperty("krs.title"));
	}

	@FXML
	protected void setPerforationsScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"perforations.view"),
						AppMain.getLabelsProperties().getProperty(
								"perforations.title"));
	}

	@FXML
	protected void setOperatingConstraintsScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"operating.view"),
						AppMain.getLabelsProperties().getProperty(
								"operating.title"));
	}

	@FXML
	protected void setSkinScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty("skin.view"),
						AppMain.getLabelsProperties().getProperty("skin.title"));
	}

	@FXML
	protected void setMultiparametricScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.getSpecificWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"multiparametric.view"),
						AppMain.getLabelsProperties().getProperty(
								"multiparametric.title"));
	}

	@FXML
	protected void setIPRScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.getSpecificWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty("ipr.view"),
						AppMain.getLabelsProperties().getProperty("ipr.title"));
	}

	@FXML
	protected void setAsphaltenesGeneralScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.getSpecificWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"asphaltenes.general.view"),
						AppMain.getLabelsProperties().getProperty(
								"asphaltenes.general.title"));
	}

	@FXML
	protected void setAsphaltenesFractionsScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.getSpecificWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"asphaltenes.fractions.view"),
						AppMain.getLabelsProperties().getProperty(
								"asphaltenes.fractions.title"));
	}
	
	@FXML
	protected void setAsphaltenesRemediationGeneralScreen(MouseEvent ev) {
		mainWorkflowController
				.getProjectWorkflow()
				.getScenaryWorkflow()
				.getSpecificWorkflow()
				.setModal(
						AppMain.getConfigProperties().getProperty(
								"asphaltenes_remediation.general.view"),
						AppMain.getLabelsProperties().getProperty(
								"asphaltenes_remediation.general.title"));
	}
	@FXML
	protected void scenariosButtonAction(MouseEvent event) {
		// show project admin view
		mainWorkflowController.getProjectWorkflow()
				.initExistingProjectWorkflow(
						mainWorkflowController.getProjectWorkflow()
								.getProject());
	}

	@FXML
	protected void resultsButtonAction(MouseEvent event) {
		// show scenario results if available

		ScenarioResultsManager src = ScenarioResultsFactory.createResults(this,
				mainWorkflowController.getProjectWorkflow().getProject()
						.getCurrent());
		src.showResults();
	}

	@FXML
	protected void simulateButtonAction(MouseEvent event) {
		if (mainWorkflowController.getProjectWorkflow().getProject() != null
				&& mainWorkflowController.getProjectWorkflow().getProject()
						.getCurrent() != null) {

			ScenarioExecutionManager sec;
			sec = ScenarioExecutionFactory.createExecution(this,
					mainWorkflowController.getProjectWorkflow().getProject()
							.getCurrent());
			sec.execute();

			mainWorkflowController.showDialog("Execution finished!"); // TODO:msg
			// LLamar resultados
			this.resultsButtonAction(null);

		} else {
			mainWorkflowController
					.showDialog("Incomplete data.\nPlease check the information.");
			// TODO:msg properties, validate
		}
	}

	public void addInternalPanel(Parent screen) {
		// Is there a screen already displayed
		removeInternalPanel();
		getWorkspacePanel().getChildren().add(screen);

	}

	public void addScenarioMenu(Parent screen) {
		if (!this.scenarioInfoPanel.getChildren().isEmpty()) {
			this.scenarioInfoPanel.getChildren().remove(0);
		}
		this.scenarioInfoPanel.getChildren().add(screen);
	}

	public void removeInternalPanel() {
		if (!getWorkspacePanel().getChildren().isEmpty()) {
			getWorkspacePanel().getChildren().remove(0);
		}
	}

	public StackPane getWorkspacePanel() {
		return workspacePanel;
	}

	public TitledPane getBaseDataInfoTab() {
		return baseDataInfoTab;
	}

	public TitledPane getScenarioInfoTab() {
		return scenarioInfoTab;
	}

	public MainWorkflow getMainWorkflowController() {
		return mainWorkflowController;
	}

	/**
	 * Synchronaze the GUI against the current data modal.
	 * 
	 * @param resource
	 */
	public void synchronizeAccordionView(String resource) {

		Button selectedButton = mapResourceButton(resource);

		if (selectedButton != null) {
			baseDataInfoTab.setExpanded(true);
			removePreviousSelection();
			showAsSelected(selectedButton, true);
		} else {
			scenarioInfoTab.setExpanded(true);
		}

	}

	/**
	 * Changes the button style if selected
	 * 
	 * @param selectedButton
	 * @param selected
	 */
	private void removePreviousSelection() {
		showAsSelected(generalDataButton, false);
		showAsSelected(reservoirDataButton, false);
		showAsSelected(gridDataButton, false);
		showAsSelected(guDataButton, false);
		showAsSelected(rockDataButton, false);
		showAsSelected(initialConditionsDataButton, false);
		showAsSelected(fluidDataButton, false);
		showAsSelected(permeabilitiesDataButton, false);
		showAsSelected(perforationsDataButton, false);
		showAsSelected(constraintsDataButton, false);
		showAsSelected(skinDataButton, false);
	}

	/**
	 * Changes the button style if selected
	 * 
	 * @param selectedButton
	 * @param selected
	 */
	private void showAsSelected(Button selectedButton, boolean selected) {
		if (selected) {
			selectedButton.getStyleClass().add("selected-Button");
			selectedButton.applyCss();
		} else {
			selectedButton.getStyleClass().remove("selected-Button");
			selectedButton.applyCss();
		}
	}

	/**
	 * Returns the accodion button for the given resource
	 * 
	 * @param selectedButton
	 */
	private Button mapResourceButton(String resource) {
		if (resource.equals(AppMain.getConfigProperties().getProperty(
				"scenary.general.data.view"))) {
			return generalDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"reservoir.view"))) {
			return reservoirDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"grid.data.view"))) {
			return gridDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"gu.data.view"))) {
			return guDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"rock.properties.view"))) {
			return rockDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"initial.conditions.view"))) {
			return initialConditionsDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"pvt.view"))) {
			return fluidDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"krs.view"))) {
			return permeabilitiesDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"perforations.view"))) {
			return perforationsDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"operating.view"))) {
			return constraintsDataButton;
		} else if (resource.equals(AppMain.getConfigProperties().getProperty(
				"skin.view"))) {
			return skinDataButton;
		}
		return null;
	}
}
