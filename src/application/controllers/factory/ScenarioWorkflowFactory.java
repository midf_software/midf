package application.controllers.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import application.models.Project;
import application.models.Scenario;
import application.models.ScenarioTypes;
import application.controllers.MainController;
import application.controllers.Workflow;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.controllers.project.scenary.asphaltenes.AsphaltenesWorkflow;
import application.controllers.project.scenary.asphaltenes_remediation.AsphaltenesRemediationWorkflow;
import application.controllers.project.scenary.basic.BasicSimulationWorkflow;
import application.controllers.project.scenary.preanalysis.ipr.IPRWorkflow;
import application.controllers.project.scenary.preanalysis.multiparametric.MultiparametricWorkflow;

public class ScenarioWorkflowFactory {
	private static final Map <String, Class<? extends ScenaryWorkflowBase>> workflows;
	static {
		workflows = new HashMap<String, Class<? extends ScenaryWorkflowBase>> ();
		// Listado de WorkFlows.
		workflows.put(ScenarioTypes.BASIC_SIMULATION, BasicSimulationWorkflow.class);
		workflows.put(ScenarioTypes.MULTIPARAMETRIC, MultiparametricWorkflow.class);
		workflows.put(ScenarioTypes.IPR, IPRWorkflow.class);
		workflows.put(ScenarioTypes.ASPHALTENES_DIAGNOSTIC, AsphaltenesWorkflow.class);
		workflows.put(ScenarioTypes.ASPHALTENES_REMEDIATION, AsphaltenesRemediationWorkflow.class);
	}
	
	/**
	 * Devuelve el workflow especifico del escenario. En caso de no tener devuelve null.
	 * @param mc
	 * @param p
	 * @param sc
	 * @return
	 */
	public static ScenaryWorkflowBase createWorkflow(MainController mc, Project p, Scenario sc) {
		try {
			Class <? extends ScenaryWorkflowBase> cls = ScenarioWorkflowFactory.workflows.get(sc.getType());
			if (cls == null) {
				return  null;
			}
			
			Constructor<? extends ScenaryWorkflowBase> constructor =  cls.getConstructor(MainController.class, Project.class, Scenario.class);
			return (ScenaryWorkflowBase) constructor.newInstance(mc, p, sc);
			
		}  catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
