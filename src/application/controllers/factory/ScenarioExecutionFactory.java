package application.controllers.factory;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import application.controllers.MainController;
import application.execution.AsphaltenesExecutionManager;
import application.execution.AsphaltenesRemediationExecutionManager;
import application.execution.BasicSimulationExecutionManager;
import application.execution.IPRExecutionManager;
import application.execution.MultiparametricExecutionManager;
import application.execution.ScenarioExecutionManager;
import application.models.Scenario;
import application.models.ScenarioTypes;


public class ScenarioExecutionFactory {
	private static final Map <String, Class<? extends ScenarioExecutionManager>> execs;
	static {
		execs = new HashMap<String, Class<? extends ScenarioExecutionManager>> ();
		// Listado de Controladores de resultados.
		execs.put(ScenarioTypes.BASIC_SIMULATION, BasicSimulationExecutionManager.class);
		execs.put(ScenarioTypes.MULTIPARAMETRIC, MultiparametricExecutionManager.class);
		execs.put(ScenarioTypes.IPR, IPRExecutionManager.class);
		execs.put(ScenarioTypes.ASPHALTENES_DIAGNOSTIC, AsphaltenesExecutionManager.class);
		execs.put(ScenarioTypes.ASPHALTENES_REMEDIATION, AsphaltenesRemediationExecutionManager.class);
	}
	
	/**
	 * Devuelve el workflow especifico del escenario. En caso de no tener devuelve null.
	 * @param mc
	 * @param p
	 * @param sc
	 * @return
	 */
	public static ScenarioExecutionManager createExecution(MainController mc,  Scenario sc) {
		try {
			Class <? extends ScenarioExecutionManager> cls = ScenarioExecutionFactory.execs.get(sc.getType());
			if (cls == null) {
				return  null;
			}
			
			Constructor<? extends ScenarioExecutionManager> constructor =  cls.getConstructor(MainController.class, Scenario.class);
			return (ScenarioExecutionManager) constructor.newInstance(mc, sc);
			
		}  catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
