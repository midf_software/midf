package application.controllers.factory;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import application.controllers.MainController;
import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.controllers.project.scenary.preanalysis.multiparametric.MultiparametricWorkflow;
import application.controllers.results.AsphaltenesRemediationResultsManager;
import application.controllers.results.AsphaltenesResultsManager;
import application.controllers.results.BasicSimulationResultsManager;
import application.controllers.results.MultiparametricResultsManager;
import application.controllers.results.ScenarioResultsManager;
import application.controllers.results.ipr.IPRResultsManager;
import application.models.Project;
import application.models.Scenario;
import application.models.ScenarioTypes;

public class ScenarioResultsFactory {
	private static final Map <String, Class<? extends ScenarioResultsManager>> results;
	static {
		results = new HashMap<String, Class<? extends ScenarioResultsManager>> ();
		// Listado de Controladores de resultados.
		results.put(ScenarioTypes.BASIC_SIMULATION, BasicSimulationResultsManager.class);
		results.put(ScenarioTypes.MULTIPARAMETRIC, MultiparametricResultsManager.class);
		results.put(ScenarioTypes.IPR, IPRResultsManager.class);
		results.put(ScenarioTypes.ASPHALTENES_DIAGNOSTIC, AsphaltenesResultsManager.class);
		results.put(ScenarioTypes.ASPHALTENES_REMEDIATION, AsphaltenesRemediationResultsManager.class);
	}
	
	/**
	 * Devuelve el workflow especifico del escenario. En caso de no tener devuelve null.
	 * @param mc
	 * @param p
	 * @param sc
	 * @return
	 */
	public static ScenarioResultsManager createResults(MainController mc,  Scenario sc) {
		try {
			Class <? extends ScenarioResultsManager> cls = ScenarioResultsFactory.results.get(sc.getType());
			if (cls == null) {
				return  null;
			}
			
			Constructor<? extends ScenarioResultsManager> constructor =  cls.getConstructor(MainController.class, Scenario.class);
			return (ScenarioResultsManager) constructor.newInstance(mc, sc);
			
		}  catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}

