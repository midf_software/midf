package application.controllers.validation;

import java.util.ArrayList;
import java.util.List;

import application.controllers.validation.exceptions.IllegalValueException;
import application.controllers.validation.exceptions.NumberFormatInvalidException;
import application.controllers.validation.exceptions.NumberNotInRangeException;
import application.controllers.validation.exceptions.ValidationException;
import application.controllers.validation.rules.DecimalNumberRule;
import application.controllers.validation.rules.ValidationRule;
import application.utils.TableHelper.EditingCell;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * ValidationClause es una clausula de validacion que sirve para validar un
 * campo segun su valor. En caso de error genera un mensaje de error y modifica
 * la apariencia del campo.
 * 
 * Se especifica el nombre del campo a validar, el Nodo javafx del campo
 * (TextField o TableView) y las reglas (ValidationRules) que pueden ser 1 o
 * más.
 * 
 * Realiza la validacion del campo (TextField) o tabla (TableView) segun las
 * reglas de validacion definidas (valRules). Ver constructor.
 * 
 * - Si el campo a validar es TextField se aplican todas las reglas pasadas en
 * el construcutor.
 * 
 * - Si el campo a validar es un TableView se recorre cada columna y se aplica
 * la regla correspondiente segun la posicion de la columna. Tener en cuenta que
 * los TableView tienen un "validador" básico incorporado para números mal
 * ingresados (controlado por la excepcion NuberFormatException).
 * 
 * Si alguna regla de validacion falla (lanza alguna excepcion) se guarda el
 * mensaje de error de la excepcion en this.errorMessages.
 * 
 * EJ: new ValidationClause("Max Time Dt", maxTimeText, new DecimalNumberRule(0,
 * 365));
 * 
 * @author Jeyson Molina
 */
public class ValidationClause {
	private Node node;
	private String fieldName;
	private ValidationRule[] valRules;
	private List<String> errorMessages = new ArrayList<String>();

	/**
	 * 
	 * @param fname
	 *            (Nombre del campo mostrado al usuario)
	 * @param n
	 *            (Nodo javafx. TextField o TableView)
	 * @param vr
	 *            (Reglas de validacion, 1 o más)
	 */
	public ValidationClause(String fname, Node n, ValidationRule... vr) {
		node = n;
		valRules = vr;
		fieldName = fname;
	}
	
/**
 * Valida el campo (textfield) o la tabla (cada celda que representa un campo) aplicando las reglas
 * asignadas.
 * @return
 */
	public boolean validate() {
		boolean valid = true;
		this.errorMessages.clear();
		if (node instanceof TextField) {
			// TEXTFIELD
			TextField tf = (TextField) node;
			valid = this.validateField(tf, this.valRules); // Se usan todos los
															// valRules para
															// este field
			if (!valid) {
				this.setFieldWarningStyle(tf);
			}
		} else if (node instanceof TableView) {
			// TABLA
			TableView tv = (TableView) node;
			int cnt = 0;
			int rowcount = 0;
			int totalrows = tv.getItems().size(); // Tableview suele crear mas
													// tablerows que la cantidad
													// de items en la lista
													// observable

			for (Node n : tv.lookupAll("TableRow")) {
				rowcount += 1;
				if (rowcount > totalrows) { // este control es para no
											// inspeccionar tablerows
											// adicionales que no tienen info
					break;
				}
				TableRow tr = (TableRow) n;
				List<Node> child = tr.getChildrenUnmodifiable();
				cnt = 0;
				for (Node m : child) {
					EditingCell ec = (EditingCell) m;
					TextField tf = ec.getTextField();
					String columnName = ((TableColumn) tv.getColumns().get(cnt))
							.getText();

					if (!this.validateField(ec, columnName, rowcount,
							valRules[cnt])) {// Se usa el valRule en la pos
												// actual
						valid = false;
						this.setFieldWarningStyle(ec);
					}
					cnt += 1;
				}

			}
		}

		return valid;
	}

	private boolean validateField(EditingCell ec, String columnName, int row,
			ValidationRule... valRules) {
		String value;
		value = ec.getText();
		boolean result = this.validateField(ec, value, valRules);
		if (!result) {
			// Agregar info sobre columna y fila donde ocurrió el error de
			// validación
			String s = ((String) this.errorMessages.get(this.errorMessages
					.size() - 1)).concat(String.format(
					" (Column: %s, Row: %s)", columnName, row));
			this.errorMessages.remove(this.errorMessages.size() - 1);
			this.errorMessages.add(s);
		}
		return result;
	}

	private boolean validateField(TextField tf, ValidationRule... valRules) {
		String value;
		value = tf.getText();
		return this.validateField(tf, value, valRules);
	}

	private boolean validateField(Node tf, String value,
			ValidationRule... valRules) {
		boolean valid = true;
		this.setFieldNormalStyle(tf);// TODO expensive?
		try {
			for (ValidationRule vr : valRules) {
				vr.validate(value);
			}
			// TODO mensajes de error
		} catch (ValidationException e) {
			valid = false;
			this.errorMessages.add(e.getErrorMsg(fieldName));
			e.print(fieldName);
		}
		return valid;
	}

	private void setFieldWarningStyle(Node n) {
		n.getStyleClass().add("field-error");
		n.applyCss();
	}

	private void setFieldNormalStyle(Node n) {
		n.getStyleClass().remove("field-error");
		//n.setStyle("-fx-background-color: #ffff");
		n.applyCss();
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

}
