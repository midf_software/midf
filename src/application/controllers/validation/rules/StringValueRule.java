package application.controllers.validation.rules;

import application.controllers.validation.exceptions.IllegalValueException;

public class StringValueRule extends ValidationRule {

	@Override
	public void validate(String value) throws IllegalValueException {
		if (value == null || value.trim().equals("")) {

			throw new IllegalValueException(value);
		}
	}

}
