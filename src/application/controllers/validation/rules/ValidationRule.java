package application.controllers.validation.rules;

import application.controllers.validation.exceptions.IllegalValueException;
import application.controllers.validation.exceptions.NumberFormatInvalidException;
import application.controllers.validation.exceptions.NumberNotInRangeException;

public abstract class ValidationRule {
	
	public abstract void validate(String value) throws IllegalValueException, NumberFormatInvalidException, NumberNotInRangeException;
	

	
}
