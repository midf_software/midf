package application.controllers.validation.rules;

import application.controllers.validation.exceptions.NumberFormatInvalidException;
import application.controllers.validation.exceptions.NumberNotInRangeException;

public class DecimalNumberRule extends ValidationRule{
	private Double min, max;
	
	public DecimalNumberRule() {
		super();
	}
	
	public DecimalNumberRule(double min, double max) {
		super();
		this.min = min;
		this.max = max;
	}
	
	public void validate(String value) throws NumberFormatInvalidException, NumberNotInRangeException {
	   double val;
	   try {
		   val = Double.parseDouble(value);
	   } catch (NumberFormatException e) {
		 throw new NumberFormatInvalidException(value);
	   }
	   if (min != null) {
			if(val<min || val>max){
				throw new NumberNotInRangeException(value, String.valueOf(min), String.valueOf(max));
			}
		}
	}

}
