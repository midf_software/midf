package application.controllers.validation.rules;

import application.controllers.validation.exceptions.IllegalValueException;
import application.controllers.validation.exceptions.NumberNotInRangeException;

public class NoValidationRule extends ValidationRule{

	@Override
	public void validate(String value) throws IllegalValueException,
			NumberFormatException, NumberNotInRangeException {
		//Does nothing
	}

}
