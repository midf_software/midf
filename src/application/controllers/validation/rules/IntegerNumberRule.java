package application.controllers.validation.rules;

import application.controllers.validation.exceptions.NumberFormatInvalidException;
import application.controllers.validation.exceptions.NumberNotInRangeException;

public class IntegerNumberRule extends ValidationRule{
	private Integer min, max;
	
	public IntegerNumberRule() {
		super();
	}
	
	public IntegerNumberRule(int min, int max) {
		super();
		this.min = min;
		this.max = max;
	}
	@Override
	public void validate(String value) throws NumberFormatInvalidException, NumberNotInRangeException {
		int val;
		try {
		  val = Integer.parseInt(value);
	   } catch (NumberFormatException e) {
		 throw new NumberFormatInvalidException(value);
	   }
		
		if (min != null) {
			if(val<min || val>max){
				throw new NumberNotInRangeException(value, String.valueOf(min), String.valueOf(max));
			}
		}
		
	}

}
