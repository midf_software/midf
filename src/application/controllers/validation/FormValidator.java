package application.controllers.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * Usado para validar un formulario de la interfaz gráfica compuesto por campos de texto, tablas, etc.
 * Ej de declaración:
 * 
 * FormValidator fv = new FormValidator(new ValidationClause("Nombre de campo", TextField tf, ValidationRule ...), ...)
 * 
 * El constructor puede recibir 1 o mas instancias de ValidationClause.
 * 
 * ValidationClause es una clausula de validacion que sirve para validar un campo.
 * Se especifica el nombre del campo a validar, el Nodo javafx del campo (TextField o TableView)
 *  y las reglas (ValidationRules) que pueden ser 1 o más.
 * 
 * La validación se hace en dos etapas:
 * 
 * Etapa 1: Validacion para cada campo.
 * - Por cada ValidationClause se valida el valor del campo especificado aplicando las reglas especificadas.
 * - Todos los campos se validan y se van recolectando los mensajes de error generados.
 * 
 * Etapa 2: Validacion de reglas del negocio:
 * - Se validan las reglas de validacion definidas en el metodo validateBLRules. Si se desea incluir
 * reglas del negocio se debe heredar de esta clase y sobreescribir este método definiendo el algoritmo
 * de validacion dentro del mismo.
 * 
 * @author Jeyson Molina
 */
public class FormValidator {
	private ValidationClause[] clauses;
	private List<String> errorMessages = new ArrayList<String>();
	
	public FormValidator(ValidationClause...clauses) {
		this.clauses = clauses;
	}
	
	public boolean validate() throws Exception{
		boolean valid=true;
		this.errorMessages.clear();
		for(ValidationClause clause: clauses){
			if(!clause.validate()) {
				valid = false;
				//recoger mensajes de error generados
				this.errorMessages.addAll(clause.getErrorMessages());
			}
			
		}
		if(valid) {
			return this.validateBLRules(); // Si se pasan las reglas de cada campo se procede a 
										   // validar reglas del negocio
		}
		else {
			throw new Exception();
		}
	}
	/**
	 * Validacion de reglas de negocio (BL). Sobreescribir este método para validar campos segun reglas
	 * del negocio especificas. Si alguna regla no pasa se deberá lanzar una excepción.
	 * @return
	 */
	protected boolean validateBLRules() throws Exception {
		return true;
	}
	
	public String getErrorMessagesStr() {
		StringJoiner sj = new StringJoiner("\n");
		for(String str: this.errorMessages) {
			sj.add(str);
		}
		return sj.toString();
	}
	
	public List<String> getErrorMessages() {
		return this.errorMessages;
	}
}
