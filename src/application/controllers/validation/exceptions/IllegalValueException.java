package application.controllers.validation.exceptions;


public class IllegalValueException extends ValidationException{

	public IllegalValueException(String value) {
		super(value);
	}

	@Override
	public String getErrorMsg(String fieldName) {
		return String.format(ValidationException.messages.getProperty("value_invalid"), value, fieldName);
	}

}
