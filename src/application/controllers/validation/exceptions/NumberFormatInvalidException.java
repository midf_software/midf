package application.controllers.validation.exceptions;


public class NumberFormatInvalidException extends ValidationException{

	public NumberFormatInvalidException(String value) {
		super(value);
	}

	@Override
	public String getErrorMsg(String fieldName) {
		// TODO Auto-generated method stub
		return String.format(ValidationException.messages.getProperty("number_format_invalid"), value, fieldName);
	}

}
