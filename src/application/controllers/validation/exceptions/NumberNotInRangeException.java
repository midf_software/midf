package application.controllers.validation.exceptions;


public class NumberNotInRangeException extends ValidationException{
	private String min, max;
	@Override
	public String getErrorMsg(String fieldName) {
		// TODO Auto-generated method stub
		return String.format(ValidationException.messages.getProperty("number_out_of_range"), value, min, max, fieldName);
	}
	
	public NumberNotInRangeException(String val, String min, String max) {
		super(val);
		this.min = min;
		this.max = max;
	}
}
