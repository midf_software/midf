package application.controllers.validation.exceptions;
import java.io.IOException;
import java.util.Properties;

public abstract class ValidationException extends Exception{
	protected static Properties messages = new Properties();
	protected String value;
	static {
		try {
			messages.load(ValidationException.class.getResourceAsStream("/res/properties/validation.properties"));
		} catch (IOException e) {
			System.out.println("Could not load validation messages properties.");
		} 
	}
	public ValidationException(String value) {
		super();
		this.value = value;
	}
	public abstract String getErrorMsg(String fieldName);
	
	public void print(String fieldName) {
		System.out.println(getErrorMsg(fieldName));
	}
}
