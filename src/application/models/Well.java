package application.models;


public class Well extends BaseModel{

	private String name;
	private String latlong;
	private double radius;
	public String getName() {
		return getString("name");
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLatlong() {
		return String.valueOf(this.getDouble("lat")) + ", " + this.getDouble("long_");
	}
	
	public void setLatlong(String latlong) {
		this.latlong = latlong;
	}
	public double getRadius() {
		return getDouble("wellbore_radius");
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	/**
	 * Checks different instances of the same well
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj != null && this.getId().equals(((Well)obj).getId())){
			return true;
		}
		return super.equals(obj);
	}
}
