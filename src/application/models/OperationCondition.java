package application.models;

public class OperationCondition extends TimeData{
	private double qg;
	private double qo;
	private double qw;
	private double bhfp;
	private double dbhfp;
	
	
	public double getQg() {
		qg = getDouble("qg");
		return qg;
	}
	
	public void setQg(double qg) {
		set("qg", qg);
		this.qg = qg;
	}
	
	public double getQo() {
		qo = getDouble("qo");
		return qo;
	}
	
	public void setQo(double qo) {
		set("qo", qo);
		this.qo = qo;
	}
	
	public double getQw() {
		qw = getDouble("qw");
		return qw;
	}
	
	public void setQw(double qw) {
		set("qw", qw);
		this.qw = qw;
	}
	
	public double getBhfp() {
		bhfp = getDouble("bhfp");
		return bhfp;
	}
	
	public void setBhfp(double bhfp) {
		set("bhfp", bhfp);
		this.bhfp = bhfp;
	}
	
	public double getDbhfp() {
		dbhfp = getDouble("dbhfp");
		return dbhfp;
	}
	
	public void setDbhfp(double dbhfp) {
		set("dbhfp", dbhfp);
		this.dbhfp = dbhfp;
	}

	
}
