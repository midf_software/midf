package application.models;


/**
 * Modelo que representa un caso de simulacion de flujo (solver de flujo)
 * @author Jeyson Molina
 *
 */
public class Simulation extends BaseModel{
	private SimulationCondition simulationCondition;

	public SimulationCondition getSimulationCondition() {
	    simulationCondition = getRelatedModel(SimulationCondition.class, simulationCondition);
	    return simulationCondition;
	}

	public void setSimulationCondition(SimulationCondition simulationCondition) {
		//this.setP
		this.simulationCondition = simulationCondition;
		add(this.simulationCondition);
	}
}
