package application.models;

import org.javalite.activejdbc.annotations.Table;

@Table("ipr")
public class IPR extends ModuleDataset{
	
	private double pr;
	private double pb;
	private double pwfPF;
	private double q;
	private double f2;
	private double model;
	  
	public double getPr() {
		pr = getDouble("pr");
		return pr;
	}
	public void setPr(double pr) {
		this.pr = pr;
		set("pr", pr);
	}
	public double getPb() {
		pb = getDouble("pb");
		return pb;
	}
	public void setPb(double pb) {
		this.pb = pb;
		set("pb", pb);
	}
	public double getPwfPF() {
		pwfPF = getDouble("pwfpf");
		return pwfPF;
	}
	public void setPwfPF(double pwfPF) {
		this.pwfPF = pwfPF;
		set("pwfpf", pwfPF);
	}
	public double getQ() {
		q = getDouble("q");
		return q;
	}
	public void setQ(double q) {
		this.q = q;
		set("q", q);
	}
	public double getF2() {
		f2 = getDouble("f2");
		return f2;
	}
	public void setF2(double f2) {
		this.f2 = f2;
		set("f2", f2);
	}
	public double getModel() {
		return model;
	}
	public void setModel(double model) {
		this.model = model;
	}
}
