package application.models;

/**
 * PVT fluid properties
 * @author Jeyson
 *
 */

public class PvtValue extends BaseModel{
	private double pressure;
	private double ug;
	private double uo;
	private double uw;
	private double bg;
	private double bo;
	private double bw;
	private double rs;
	private double rv;
	
	public PvtValue() {
		super();
	}
	public PvtValue(double pressure, double ug, double uo, double uw, double bg, double bo, double bw, double rs, double rv) {
		super();
		setPressure(pressure);
		setUg(ug);
		setUo(uo);
		setUw(uw);
		setBg(bg);
		setBo(bo);
		setBw(bw);
		setRs(rs);
		setRv(rv);
		
	}
	public double getUg() {
		ug = getDouble("ug");
		return ug;
	}
	public void setUg(double ug) {
		this.ug = ug;
		set("ug", ug);
	}
	public double getUo() {
		uo = getDouble("uo");
		return uo;
	}
	public void setUo(double uo) {
		this.uo = uo;
		set("uo", uo);
	}
	public double getUw() {
		uw = getDouble("uw");
		return uw;
	}
	public void setUw(double uw) {
		this.uw = uw;
		set("uw", uw);
	}
	public double getBg() {
		bg = getDouble("bg");
		return bg;
	}
	public void setBg(double bg) {
		this.bg = bg;
		set("bg", bg);
	}
	public double getBo() {
		bo = getDouble("bo");
		return bo;
	}
	public void setBo(double bo) {
		this.bo = bo;
		set("bo", bo);
	}
	public double getBw() {
		bw = getDouble("bw");
		return bw;
	}
	public void setBw(double bw) {
		this.bw = bw;
		set("bw", bw);
	}
	public double getRs() {
		rs = getDouble("rs");	
		return rs;
	}
	public void setRs(double rs) {
		this.rs = rs;
		set("rs", rs);
	}
	public double getRv() {
		rv = getDouble("rv");
		return rv;
	}
	public void setRv(double rv) {
		this.rv = rv;
		set("rv", rv);
	}
	public double getPressure() {
		pressure = getDouble("pressure");
		return pressure;
	}
	public void setPressure(double pressure) {
		this.pressure = pressure;
		set("pressure", pressure);
	}
}