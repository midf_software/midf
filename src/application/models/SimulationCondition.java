package application.models;


public class SimulationCondition extends BaseModel {

	private double radialNumblocks; // No. De bloques en dirección radial deprecada
	private double verticalNumblocks; // No. De bloques en dirección vertical deprecada
	private double maxTimestep;// Máximo time step
	private double minTimestep; // Minimo time step
	private double totalTime;// Tiempo total de simulación
	private double maxPressureDelta;// Delta de presión máximo permitido
	private double maxSaturationDelta;// Delta de saturación máximo permitido
	private double maxRsDelta; // Delta de Rs máximo permitido
	private double dtAccelFactor; // Factor para acelerar Dt
	private double relativeError; // Error Relativo
	private double epsilon;// Epsilon
	private double numIterations;// Número máximo de iteraciones

	@Deprecated
	public double getRadialNumblocks() {
		return radialNumblocks;
	}

	@Deprecated
	public void setRadialNumblocks(double radial_numblocks) {
		this.radialNumblocks = radial_numblocks;
	}

	@Deprecated
	public double getVerticalNumblocks() {
		return verticalNumblocks;
	}

	@Deprecated
	public void setVerticalNumblocks(double vertical_numblocks) {
		this.verticalNumblocks = vertical_numblocks;
	}

	public double getMaxTimestep() {
		maxTimestep = getDouble("max_timestep");
		return maxTimestep;
	}

	public void setMaxTimestep(double max_timestep) {
		this.maxTimestep = max_timestep;
		set("max_timestep", maxTimestep);
	}

	public double getMinTimestep() {
		minTimestep = getDouble("min_timestep");
		return minTimestep;
	}

	public void setMinTimestep(double min_timestep) {
		this.minTimestep = min_timestep;
		set("min_timestep", minTimestep);
	}

	public double getTotalTime() {
		totalTime = getDouble("total_time");
		return totalTime;
	}

	public void setTotalTime(double total_time) {
		this.totalTime = total_time;
		set("total_time", totalTime);
	}

	public double getMaxPressureDelta() {
		maxPressureDelta = getDouble("max_pressuredelta"); 
		return maxPressureDelta;
	}

	public void setMaxPressureDelta(double max_pressure_delta) {
		this.maxPressureDelta = max_pressure_delta;
		set("max_pressuredelta", this.maxPressureDelta);
	}

	public double getMaxSaturationDelta() {
		maxSaturationDelta = getDouble("max_saturationdelta");
		return maxSaturationDelta;
	}

	public void setMaxSaturationDelta(double max_saturation_delta) {
		this.maxSaturationDelta = max_saturation_delta;
		set("max_saturationdelta", maxSaturationDelta);
	}

	public double getMaxRsDelta() {
		maxRsDelta = getDouble("max_rsdelta");
		return maxRsDelta;
	}

	public void setMaxRsDelta(double max_rs_delta) {
		this.maxRsDelta = max_rs_delta;
		set("max_rsdelta", maxRsDelta);
	}

	public double getDtAccelFactor() {
		dtAccelFactor = getDouble("dt_accelfactor");
		return dtAccelFactor;
	}

	public void setDtAccelFactor(double dt_accel_factor) {
		this.dtAccelFactor = dt_accel_factor;
		set("dt_accelfactor", dtAccelFactor);
	}

	public double getRelativeError() {
		relativeError = getDouble("relative_error");
		return relativeError;
	}

	public void setRelativeError(double relative_error) {
		this.relativeError = relative_error;
		set("relative_error", relative_error);
	}

	public double getEpsilon() {
		epsilon = getDouble("epsilon");
		return epsilon;
	}

	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
		set("epsilon", this.epsilon);
	}

	public double getNumIterations() {
		numIterations = getDouble("num_iterations");
		return numIterations;
	}

	public void setNumIterations(double num_iterations) {
		this.numIterations = num_iterations;
		set("num_iterations", num_iterations);
	}

}