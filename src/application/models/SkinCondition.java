package application.models;

import application.utils.DateUtils;

public class SkinCondition extends TimeData{
	
	private double skin;
	private double skinRadius;
	
	
	public double getSkin() {
		skin = getDouble("skin");
		return skin;
	}
	public void setSkin(double skin) {
		set("skin", skin);
		this.skin = skin;
	}
	public double getSkinRadius() {
		skinRadius = getDouble("skin_radius");
		return skinRadius;
	}
	public void setSkinRadius(double skinRadius) {
		set("skin_radius", skinRadius);
		this.skinRadius = skinRadius;
	}
	
}
