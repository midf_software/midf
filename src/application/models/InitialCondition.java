package application.models;

import org.javalite.activejdbc.annotations.Table;

/**
 * Condiciones iniciales
 * 
 * @author Jeyson Molina
 *
 */
public class InitialCondition extends LayerCondition{

	private double initialReservoirPressure;//4450
	private double initialGasSaturation;//0.6
	private double initialWaterSaturation;//0.2
	
	private int layerNumber;
	private String guName;

	public InitialCondition() {
		super();
		// default values
		setInitialReservoirPressure(0);
		setInitialGasSaturation(0);
		setInitialWaterSaturation(0);
	}
	
	public double getInitialReservoirPressure() {
		initialReservoirPressure = getDouble("reservoir_pressure");
		return initialReservoirPressure;
	}

	public void setInitialReservoirPressure(double initialReservoirPressure) {
		this.initialReservoirPressure = initialReservoirPressure;
		set("reservoir_pressure", initialReservoirPressure);
	}

	public double getInitialGasSaturation() {
		this.initialGasSaturation = getDouble("gas_saturation");
		return initialGasSaturation;
	}

	public void setInitialGasSaturation(double initialGasSaturation) {
		this.initialGasSaturation = initialGasSaturation;
		set("gas_saturation", initialGasSaturation);
	}

	public double getInitialWaterSaturation() {
		this.initialWaterSaturation = getDouble("water_saturation");
		return initialWaterSaturation;
	}

	public void setInitialWaterSaturation(double initialWaterSaturation) {
		this.initialWaterSaturation = initialWaterSaturation;
		set("water_saturation", initialWaterSaturation);
	}

	public int getLayerNumber() {
		return layerNumber;
	}

	public void setLayerNumber(int layerNum) {
		this.layerNumber = layerNum;
	}

	public String getGuName() {
		return guName;
	}

	public void setGuName(String guName) {
		this.guName = guName;
	}
	
}
