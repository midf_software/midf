package application.models;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.javalite.activejdbc.annotations.BelongsTo;

public class Project extends BaseModel {
    private String name;
    private String ref;
    private String description;
    private String date;
    private List<Scenario> scenarios = new ArrayList<Scenario>();
    
    private Well well;
	
	private Scenario current; // Escenario actual
	
   
	public String getName() {
		name = getString("name");
		return name;
	}
	public void setName(String name) {
		this.name = name;
		set("name", name);
	}
	public String getDescription() {
		description = getString("description");
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
		set("description", description);
	}
	public String getRef() {
		ref = getString("ref");
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
		set("ref", ref);
	}
	public String getDate() {
		date = getString("date");
		return date;
	}
	public void setDate(String date) {
		this.date = date;
		set("date", date);
	}
	public List<Scenario> getScenarios() {
		List<Scenario> list = getAll(Scenario.class).orderBy("name asc");
		scenarios = list; //TODO redundancy?
		return scenarios;
	}
	public void setScenarios(List<Scenario> scenarios) {
		this.scenarios = scenarios;
	}
	public void addScenario(Scenario scenario) {
		this.scenarios.add(scenario);
		add(scenario); //orm list
	}
	public void removeScenario(Scenario scenario) {
		this.scenarios.remove(scenario);
	}
	public Scenario getCurrent() {
		return current;
	}
	public void setCurrent(Scenario current) {
		this.current = current;
	}
	public Well getWell() {
		well = this.getRelatedModel(Well.class, well, true);
		return well;
	}
	public void setWell(Well well) {
		this.well = well;
	}
}
