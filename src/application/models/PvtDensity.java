package application.models;

import org.javalite.activejdbc.annotations.Table;

@Table("pvt_densities")
public class PvtDensity extends BaseModel{
	
	private double gasDensity;
	private double oilDensity;
	private double waterDensity;
	
	public double getGasDensity() {
		gasDensity = getDouble("gas_density");
		return gasDensity;
	}
	public void setGasDensity(double gasDensity) {
		this.gasDensity = gasDensity;
		set("gas_density", gasDensity);
	}
	public double getOilDensity() {
		oilDensity = getDouble("oil_density");
		return oilDensity;
	}
	public void setOilDensity(double oilDensity) {
		this.oilDensity = oilDensity;
		set("oil_density", oilDensity);
	
	}
	public double getWaterDensity() {
		waterDensity = getDouble("water_density");
		return waterDensity;
	}
	public void setWaterDensity(double waterDensity) {
		this.waterDensity = waterDensity;
		set("water_density", waterDensity);
	}
}
