package application.models;

import application.utils.DateUtils;

public abstract class TimeData extends BaseModel{
	private double time;
	private String startDate;
	
	public double getTime() {
		time = getDouble("time");
		return time;
	}
	public void setTime(double time) {
		set("time", time);
		this.time = time;
	}
	
	
	public String getDate() {
		  return DateUtils.dateToString(DateUtils.addDays(DateUtils.stringToDate(this.startDate), (int) this.getTime()));
		}
		
		public void setDate(String date) {
			int days;
			try {
				days = DateUtils.getDays(DateUtils.stringToDate(this.startDate), DateUtils.stringToDate(date));
				System.out.println("days: " + days);
				this.setTime(days);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public String getStartDate() {
			return startDate;
		}
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

}
