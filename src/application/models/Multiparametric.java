package application.models;

import java.awt.Font;

import javafx.embed.swing.SwingNode;
import javafx.scene.control.Tab;

import org.javalite.activejdbc.annotations.Table;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.SpiderWebPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

@Table("multiparametric")
public class Multiparametric extends ModuleDataset{
	
	//Datos Generales    
    //String field;
    String type_well;
    //String date;
    double TD;
    double rw;
    double re;
    
    //Datos Yacimiento
    double top;
    double netpay;
    double porosity;
    double permeability;
    double kc;
    double reservoir_pressure;
    double saturation_pressure;
    double pc_mineral_scales;
    double pc_organic_scales;
    
    //Datos produccion
    double qo;
    double qg;
    double bhp;
    
    //Datos PVT
    double uo;
    double ug;
    double bo;
    double bg;
    
    //Datos kd/ki
    double mineral_scales;
    double fines_blockage;
    double organic_scales;
    double relative_permeability;
    double induced_damage;
    double geomechanical_damage;
    
    //Datos Percentil
    //Parte 1
    double msp1_p10;
    double msp1_p90;
    double msp1_valor;
    double fbp1_p10;
    double fbp1_p90;
    double fbp1_valor;
    double osp1_p10;
    double osp1_p90;
    double osp1_valor;
    double krp1_p10;
    double krp1_p90;
    double krp1_valor;
    double idp1_p10;
    double idp1_p90;
    double idp1_valor;
    double gdp1_p10;
    double gdp1_p90;
    double gdp1_valor;    
    
    //Parte 2
    double msp2_p10;
    double msp2_p90;
    double msp2_valor;
    double fbp2_p10;
    double fbp2_p90;
    double fbp2_valor;
    double osp2_p10;
    double osp2_p90;
    double osp2_valor;
    double krp2_p10;
    double krp2_p90;
    double krp2_valor;
    double idp2_p10;
    double idp2_p90;
    double idp2_valor;
    double gdp2_p10;
    double gdp2_p90;
    double gdp2_valor;    

    //Parte 3
    double msp3_p10;
    double msp3_p90;
    double msp3_valor;
    double fbp3_p10;
    double fbp3_p90;
    double fbp3_valor;
    double osp3_p10;
    double osp3_p90;
    double osp3_valor;
    double krp3_p10;
    double krp3_p90;
    double krp3_valor;
    double idp3_p10;
    double idp3_p90;
    double idp3_valor;
    double gdp3_p10;
    double gdp3_p90;
    double gdp3_valor;    
    
    //Parte 4
    double msp4_p10;
    double msp4_p90;
    double msp4_valor;
    double fbp4_p10;
    double fbp4_p90;
    double fbp4_valor;
    double osp4_p10;
    double osp4_p90;
    double osp4_valor;
    double krp4_p10;
    double krp4_p90;
    double krp4_valor;
    double idp4_p10;
    double idp4_p90;
    double idp4_valor;
    double gdp4_p10;
    double gdp4_p90;
    double gdp4_valor;    

    //Parte 5
    double msp5_p10;
    double msp5_p90;
    double msp5_valor;
    double fbp5_p10;
    double fbp5_p90;
    double fbp5_valor;
    double osp5_p10;
    double osp5_p90;
    double osp5_valor;
    double krp5_p10;
    double krp5_p90;
    double krp5_valor;

    //Variables de la parte estadistica
    double msp1;
    double msp2;
    double msp3;
    double msp4;
    double msp5;
    
    double fbp1;
    double fbp2;
    double fbp3;
    double fbp4;
    double fbp5;
    
    double osp1;
    double osp2;
    double osp3;
    double osp4;
    double osp5;
    
    double krp1;
    double krp2;
    double krp3;
    double krp4;
    double krp5;
    
    double idp1;
    double idp2;
    double idp3;
    double idp4;
    
    double gdp1;
    double gdp2;
    double gdp3;
    double gdp4;    
    
    //Variables de la parte estadistica
    double statistical_mineral_scales;
    double statistical_fines_blockage;
    double statistical_organic_scales;
    double statistical_relative_permeability;
    double statistical_induced_damage;
    double statistical_geomechanical_damage;
    double statistical_total;

    double statistical_mineral_scales_avg;
    double statistical_fines_blockage_avg;
    double statistical_organic_scales_avg;
    double statistical_relative_permeability_avg;
    double statistical_induced_damage_avg;
    double statistical_geomechanical_damage_avg;
    
    //Variables de la parte de skin
    double skin_mineral_scales;
    double skin_fines_blockage;
    double skin_organic_scales;
    double skin_relative_permeability;
    double skin_induced_damage;
    double skin_geomechanical_damage;
    double skin_total;
    
    double skin_mineral_scales_avg;
    double skin_fines_blockage_avg;
    double skin_organic_scales_avg;
    double skin_relative_permeability_avg;
    double skin_induced_damage_avg;
    double skin_geomechanical_damage_avg;
    
    double mineral_scales_avg;
    double fines_blockage_avg;
    double organic_scales_avg;
    double relative_permeability_avg;
    double induced_damage_avg;
    double geomechanical_damage_avg;
    
	 public String getType_well() {
		return type_well;
	}
	public void setType_well(String type_well) {
		this.type_well = type_well;
	}
	


    public void setTD(double TD) {
        this.TD = TD;
        set("TD", TD);
	}
    public double getTD(){
        this.TD = getDouble("TD");
        return TD;
    }
    
    public void setRw(double rw) {
        this.rw = rw;
        set("rw", rw);
	}
    public double getRw(){
        this.rw = getDouble("rw");
        return rw;
    }
    
    public void setRe(double re) {
        this.re = re;
        set("re", re);
	}
    public double getRe(){
        this.re = getDouble("re");
        return re;
    }
    
    public void setTop(double top) {
        this.top = top;
        set("top", top);
	}
    public double getTop(){
        this.top = getDouble("top");
        return top;
    }
    
    public void setNetpay(double netpay) {
        this.netpay = netpay;
        set("netpay", netpay);
	}
    public double getNetpay(){
        this.netpay = getDouble("netpay");
        return netpay;
    }
    
    public void setPorosity(double porosity) {
        this.porosity = porosity;
        set("porosity", porosity);
	}
    public double getPorosity(){
        this.porosity = getDouble("porosity");
        return porosity;
    }
    
    public void setPermeability(double permeability) {
        this.permeability = permeability;
        set("permeability", permeability);
	}
    public double getPermeability(){
        this.permeability = getDouble("permeability");
        return permeability;
    }
    
    public void setKc(double kc) {
        this.kc = kc;
        set("kc", kc);
	}
    public double getKc(){
        this.kc = getDouble("kc");
        return kc;
    }
    
    public void setReservoir_pressure(double reservoir_pressure) {
        this.reservoir_pressure = reservoir_pressure;
        set("reservoir_pressure", reservoir_pressure);
	}
    public double getReservoir_pressure(){
        this.reservoir_pressure = getDouble("reservoir_pressure");
        return reservoir_pressure;
    }
    
    public void setSaturation_pressure(double saturation_pressure) {
        this.saturation_pressure = saturation_pressure;
        set("saturation_pressure", saturation_pressure);
	}
    public double getSaturation_pressure(){
        this.saturation_pressure = getDouble("saturation_pressure");
        return saturation_pressure;
    }
    
    public void setPc_mineral_scales(double pc_mineral_scales) {
        this.pc_mineral_scales = pc_mineral_scales;
        set("pc_mineral_scales", pc_mineral_scales);
	}
    public double getPc_mineral_scales(){
        this.pc_mineral_scales = getDouble("pc_mineral_scales");
        return pc_mineral_scales;
    }
    
    public void setPc_organic_scales(double pc_organic_scales) {
        this.pc_organic_scales = pc_organic_scales;
        set("pc_organic_scales", pc_organic_scales);
	}
    public double getPc_organic_scales(){
        this.pc_organic_scales = getDouble("pc_organic_scales");
        return pc_organic_scales;
    }
    
    public void setQo(double qo) {
        this.qo = qo;
        set("qo", qo);
	}
    public double getQo(){
        this.qo = getDouble("qo");
        return qo;
    }
    
    public void setQg(double qg) {
        this.qg = qg;
        set("qg", qg);
	}
    public double getQg(){
        this.qg = getDouble("qg");
        return qg;
    }
    
    public void setBhp(double bhp) {
        this.bhp = bhp;
        set("bhp", bhp);
	}
    public double getBhp(){
        this.bhp = getDouble("bhp");
        return bhp;
    }
    
    public void setUo(double uo) {
        this.uo = uo;
        set("uo", uo);
	}
    public double getUo(){
        this.uo = getDouble("uo");
        return uo;
    }
    
    public void setUg(double ug) {
        this.ug = ug;
        set("ug", ug);
	}
    public double getUg(){
        this.ug = getDouble("ug");
        return ug;
    }
    
    public void setBo(double bo) {
        this.bo = bo;
        set("bo", bo);
	}
    public double getBo(){
        this.bo = getDouble("bo");
        return bo;
    }
    
    public void setBg(double bg) {
        this.bg = bg;
        set("bg", bg);
	}
    public double getBg(){
        this.bg = getDouble("bg");
        return bg;
    }
    
    public void setMineral_scales(double mineral_scales) {
        this.mineral_scales = mineral_scales;
        set("mineral_scales", mineral_scales);
	}
    public double getMineral_scales(){
        this.mineral_scales = getDouble("mineral_scales");
        return mineral_scales;
    }
    
    public void setFines_blockage(double fines_blockage) {
        this.fines_blockage = fines_blockage;
        set("fines_blockage", fines_blockage);
	}
    public double getFines_blockage(){
        this.fines_blockage = getDouble("fines_blockage");
        return fines_blockage;
    }
    
    public void setOrganic_scales(double organic_scales) {
        this.organic_scales = organic_scales;
        set("organic_scales", organic_scales);
	}
    public double getOrganic_scales(){
        this.organic_scales = getDouble("organic_scales");
        return organic_scales;
    }
    
    public void setRelative_permeability(double relative_permeability) {
        this.relative_permeability = relative_permeability;
        set("relative_permeability", relative_permeability);
	}
    public double getRelative_permeability(){
        this.relative_permeability = getDouble("relative_permeability");
        return relative_permeability;
    }
    
    public void setInduced_damage(double induced_damage) {
        this.induced_damage = induced_damage;
        set("induced_damage", induced_damage);
	}
    public double getInduced_damage(){
        this.induced_damage = getDouble("induced_damage");
        return induced_damage;
    }
    
    public void setGeomechanical_damage(double geomechanical_damage) {
        this.geomechanical_damage = geomechanical_damage;
        set("geomechanical_damage", geomechanical_damage);
	}
    public double getGeomechanical_damage(){
        this.geomechanical_damage = getDouble("geomechanical_damage");
        return geomechanical_damage;
    }
    
    public void setMsp1_p10(double msp1_p10) {
        this.msp1_p10 = msp1_p10;
        set("msp1_p10", msp1_p10);
	}
    public double getMsp1_p10(){
        this.msp1_p10 = getDouble("msp1_p10");
        return msp1_p10;
    }
    
    public void setMsp1_p90(double msp1_p90) {
        this.msp1_p90 = msp1_p90;
        set("msp1_p90", msp1_p90);
	}
    public double getMsp1_p90(){
        this.msp1_p90 = getDouble("msp1_p90");
        return msp1_p90;
    }
    
    public void setMsp1_valor(double msp1_valor) {
        this.msp1_valor = msp1_valor;
        set("msp1_valor", msp1_valor);
	}
    public double getMsp1_valor(){
        this.msp1_valor = getDouble("msp1_valor");
        return msp1_valor;
    }
    
    public void setFbp1_p10(double fbp1_p10) {
        this.fbp1_p10 = fbp1_p10;
        set("fbp1_p10", fbp1_p10);
	}
    public double getFbp1_p10(){
        this.fbp1_p10 = getDouble("fbp1_p10");
        return fbp1_p10;
    }
    
    public void setFbp1_p90(double fbp1_p90) {
        this.fbp1_p90 = fbp1_p90;
        set("fbp1_p90", fbp1_p90);
	}
    public double getFbp1_p90(){
        this.fbp1_p90 = getDouble("fbp1_p90");
        return fbp1_p90;
    }
    
    public void setFbp1_valor(double fbp1_valor) {
        this.fbp1_valor = fbp1_valor;
        set("fbp1_valor", fbp1_valor);
	}
    public double getFbp1_valor(){
        this.fbp1_valor = getDouble("fbp1_valor");
        return fbp1_valor;
    }
    
    public void setOsp1_p10(double osp1_p10) {
        this.osp1_p10 = osp1_p10;
        set("osp1_p10", osp1_p10);
	}
    public double getOsp1_p10(){
        this.osp1_p10 = getDouble("osp1_p10");
        return osp1_p10;
    }
    
    public void setOsp1_p90(double osp1_p90) {
        this.osp1_p90 = osp1_p90;
        set("osp1_p90", osp1_p90);
	}
    public double getOsp1_p90(){
        this.osp1_p90 = getDouble("osp1_p90");
        return osp1_p90;
    }
    
    public void setOsp1_valor(double osp1_valor) {
        this.osp1_valor = osp1_valor;
        set("osp1_valor", osp1_valor);
	}
    public double getOsp1_valor(){
        this.osp1_valor = getDouble("osp1_valor");
        return osp1_valor;
    }
    
    public void setKrp1_p10(double krp1_p10) {
        this.krp1_p10 = krp1_p10;
        set("krp1_p10", krp1_p10);
	}
    public double getKrp1_p10(){
        this.krp1_p10 = getDouble("krp1_p10");
        return krp1_p10;
    }
    
    public void setKrp1_p90(double krp1_p90) {
        this.krp1_p90 = krp1_p90;
        set("krp1_p90", krp1_p90);
	}
    public double getKrp1_p90(){
        this.krp1_p90 = getDouble("krp1_p90");
        return krp1_p90;
    }
    
    public void setKrp1_valor(double krp1_valor) {
        this.krp1_valor = krp1_valor;
        set("krp1_valor", krp1_valor);
	}
    public double getKrp1_valor(){
        this.krp1_valor = getDouble("krp1_valor");
        return krp1_valor;
    }
    
    public void setIdp1_p10(double idp1_p10) {
        this.idp1_p10 = idp1_p10;
        set("idp1_p10", idp1_p10);
	}
    public double getIdp1_p10(){
        this.idp1_p10 = getDouble("idp1_p10");
        return idp1_p10;
    }
    
    public void setIdp1_p90(double idp1_p90) {
        this.idp1_p90 = idp1_p90;
        set("idp1_p90", idp1_p90);
	}
    public double getIdp1_p90(){
        this.idp1_p90 = getDouble("idp1_p90");
        return idp1_p90;
    }
    
    public void setIdp1_valor(double idp1_valor) {
        this.idp1_valor = idp1_valor;
        set("idp1_valor", idp1_valor);
	}
    public double getIdp1_valor(){
        this.idp1_valor = getDouble("idp1_valor");
        return idp1_valor;
    }
    
    public void setGdp1_p10(double gdp1_p10) {
        this.gdp1_p10 = gdp1_p10;
        set("gdp1_p10", gdp1_p10);
	}
    public double getGdp1_p10(){
        this.gdp1_p10 = getDouble("gdp1_p10");
        return gdp1_p10;
    }
    
    public void setGdp1_p90(double gdp1_p90) {
        this.gdp1_p90 = gdp1_p90;
        set("gdp1_p90", gdp1_p90);
	}
    public double getGdp1_p90(){
        this.gdp1_p90 = getDouble("gdp1_p90");
        return gdp1_p90;
    }
    
    public void setGdp1_valor(double gdp1_valor) {
        this.gdp1_valor = gdp1_valor;
        set("gdp1_valor", gdp1_valor);
	}
    public double getGdp1_valor(){
        this.gdp1_valor = getDouble("gdp1_valor");
        return gdp1_valor;
    }
    
    public void setMsp2_p10(double msp2_p10) {
        this.msp2_p10 = msp2_p10;
        set("msp2_p10", msp2_p10);
	}
    public double getMsp2_p10(){
        this.msp2_p10 = getDouble("msp2_p10");
        return msp2_p10;
    }
    
    public void setMsp2_p90(double msp2_p90) {
        this.msp2_p90 = msp2_p90;
        set("msp2_p90", msp2_p90);
	}
    public double getMsp2_p90(){
        this.msp2_p90 = getDouble("msp2_p90");
        return msp2_p90;
    }
    
    public void setMsp2_valor(double msp2_valor) {
        this.msp2_valor = msp2_valor;
        set("msp2_valor", msp2_valor);
	}
    public double getMsp2_valor(){
        this.msp2_valor = getDouble("msp2_valor");
        return msp2_valor;
    }
    
    public void setFbp2_p10(double fbp2_p10) {
        this.fbp2_p10 = fbp2_p10;
        set("fbp2_p10", fbp2_p10);
	}
    public double getFbp2_p10(){
        this.fbp2_p10 = getDouble("fbp2_p10");
        return fbp2_p10;
    }
    
    public void setFbp2_p90(double fbp2_p90) {
        this.fbp2_p90 = fbp2_p90;
        set("fbp2_p90", fbp2_p90);
	}
    public double getFbp2_p90(){
        this.fbp2_p90 = getDouble("fbp2_p90");
        return fbp2_p90;
    }
    
    public void setFbp2_valor(double fbp2_valor) {
        this.fbp2_valor = fbp2_valor;
        set("fbp2_valor", fbp2_valor);
	}
    public double getFbp2_valor(){
        this.fbp2_valor = getDouble("fbp2_valor");
        return fbp2_valor;
    }
    
    public void setOsp2_p10(double osp2_p10) {
        this.osp2_p10 = osp2_p10;
        set("osp2_p10", osp2_p10);
	}
    public double getOsp2_p10(){
        this.osp2_p10 = getDouble("osp2_p10");
        return osp2_p10;
    }
    
    public void setOsp2_p90(double osp2_p90) {
        this.osp2_p90 = osp2_p90;
        set("osp2_p90", osp2_p90);
	}
    public double getOsp2_p90(){
        this.osp2_p90 = getDouble("osp2_p90");
        return osp2_p90;
    }
    
    public void setOsp2_valor(double osp2_valor) {
        this.osp2_valor = osp2_valor;
        set("osp2_valor", osp2_valor);
	}
    public double getOsp2_valor(){
        this.osp2_valor = getDouble("osp2_valor");
        return osp2_valor;
    }
    
    public void setKrp2_p10(double krp2_p10) {
        this.krp2_p10 = krp2_p10;
        set("krp2_p10", krp2_p10);
	}
    public double getKrp2_p10(){
        this.krp2_p10 = getDouble("krp2_p10");
        return krp2_p10;
    }
    
    public void setKrp2_p90(double krp2_p90) {
        this.krp2_p90 = krp2_p90;
        set("krp2_p90", krp2_p90);
	}
    public double getKrp2_p90(){
        this.krp2_p90 = getDouble("krp2_p90");
        return krp2_p90;
    }
    
    public void setKrp2_valor(double krp2_valor) {
        this.krp2_valor = krp2_valor;
        set("krp2_valor", krp2_valor);
	}
    public double getKrp2_valor(){
        this.krp2_valor = getDouble("krp2_valor");
        return krp2_valor;
    }
    
    public void setIdp2_p10(double idp2_p10) {
        this.idp2_p10 = idp2_p10;
        set("idp2_p10", idp2_p10);
	}
    public double getIdp2_p10(){
        this.idp2_p10 = getDouble("idp2_p10");
        return idp2_p10;
    }
    
    public void setIdp2_p90(double idp2_p90) {
        this.idp2_p90 = idp2_p90;
        set("idp2_p90", idp2_p90);
	}
    public double getIdp2_p90(){
        this.idp2_p90 = getDouble("idp2_p90");
        return idp2_p90;
    }
    
    public void setIdp2_valor(double idp2_valor) {
        this.idp2_valor = idp2_valor;
        set("idp2_valor", idp2_valor);
	}
    public double getIdp2_valor(){
        this.idp2_valor = getDouble("idp2_valor");
        return idp2_valor;
    }
    
    public void setGdp2_p10(double gdp2_p10) {
        this.gdp2_p10 = gdp2_p10;
        set("gdp2_p10", gdp2_p10);
	}
    public double getGdp2_p10(){
        this.gdp2_p10 = getDouble("gdp2_p10");
        return gdp2_p10;
    }
    
    public void setGdp2_p90(double gdp2_p90) {
        this.gdp2_p90 = gdp2_p90;
        set("gdp2_p90", gdp2_p90);
	}
    public double getGdp2_p90(){
        this.gdp2_p90 = getDouble("gdp2_p90");
        return gdp2_p90;
    }
    
    public void setGdp2_valor(double gdp2_valor) {
        this.gdp2_valor = gdp2_valor;
        set("gdp2_valor", gdp2_valor);
	}
    public double getGdp2_valor(){
        this.gdp2_valor = getDouble("gdp2_valor");
        return gdp2_valor;
    }
    
    public void setMsp3_p10(double msp3_p10) {
        this.msp3_p10 = msp3_p10;
        set("msp3_p10", msp3_p10);
	}
    public double getMsp3_p10(){
        this.msp3_p10 = getDouble("msp3_p10");
        return msp3_p10;
    }
    
    public void setMsp3_p90(double msp3_p90) {
        this.msp3_p90 = msp3_p90;
        set("msp3_p90", msp3_p90);
	}
    public double getMsp3_p90(){
        this.msp3_p90 = getDouble("msp3_p90");
        return msp3_p90;
    }
    
    public void setMsp3_valor(double msp3_valor) {
        this.msp3_valor = msp3_valor;
        set("msp3_valor", msp3_valor);
	}
    public double getMsp3_valor(){
        this.msp3_valor = getDouble("msp3_valor");
        return msp3_valor;
    }
    
    public void setFbp3_p10(double fbp3_p10) {
        this.fbp3_p10 = fbp3_p10;
        set("fbp3_p10", fbp3_p10);
	}
    public double getFbp3_p10(){
        this.fbp3_p10 = getDouble("fbp3_p10");
        return fbp3_p10;
    }
    
    public void setFbp3_p90(double fbp3_p90) {
        this.fbp3_p90 = fbp3_p90;
        set("fbp3_p90", fbp3_p90);
	}
    public double getFbp3_p90(){
        this.fbp3_p90 = getDouble("fbp3_p90");
        return fbp3_p90;
    }
    
    public void setFbp3_valor(double fbp3_valor) {
        this.fbp3_valor = fbp3_valor;
        set("fbp3_valor", fbp3_valor);
	}
    public double getFbp3_valor(){
        this.fbp3_valor = getDouble("fbp3_valor");
        return fbp3_valor;
    }
    
    public void setOsp3_p10(double osp3_p10) {
        this.osp3_p10 = osp3_p10;
        set("osp3_p10", osp3_p10);
	}
    public double getOsp3_p10(){
        this.osp3_p10 = getDouble("osp3_p10");
        return osp3_p10;
    }
    
    public void setOsp3_p90(double osp3_p90) {
        this.osp3_p90 = osp3_p90;
        set("osp3_p90", osp3_p90);
	}
    public double getOsp3_p90(){
        this.osp3_p90 = getDouble("osp3_p90");
        return osp3_p90;
    }
    
    public void setOsp3_valor(double osp3_valor) {
        this.osp3_valor = osp3_valor;
        set("osp3_valor", osp3_valor);
	}
    public double getOsp3_valor(){
        this.osp3_valor = getDouble("osp3_valor");
        return osp3_valor;
    }
    
    public void setKrp3_p10(double krp3_p10) {
        this.krp3_p10 = krp3_p10;
        set("krp3_p10", krp3_p10);
	}
    public double getKrp3_p10(){
        this.krp3_p10 = getDouble("krp3_p10");
        return krp3_p10;
    }
    
    public void setKrp3_p90(double krp3_p90) {
        this.krp3_p90 = krp3_p90;
        set("krp3_p90", krp3_p90);
	}
    public double getKrp3_p90(){
        this.krp3_p90 = getDouble("krp3_p90");
        return krp3_p90;
    }
    
    public void setKrp3_valor(double krp3_valor) {
        this.krp3_valor = krp3_valor;
        set("krp3_valor", krp3_valor);
	}
    public double getKrp3_valor(){
        this.krp3_valor = getDouble("krp3_valor");
        return krp3_valor;
    }
    
    public void setIdp3_p10(double idp3_p10) {
        this.idp3_p10 = idp3_p10;
        set("idp3_p10", idp3_p10);
	}
    public double getIdp3_p10(){
        this.idp3_p10 = getDouble("idp3_p10");
        return idp3_p10;
    }
    
    public void setIdp3_p90(double idp3_p90) {
        this.idp3_p90 = idp3_p90;
        set("idp3_p90", idp3_p90);
	}
    public double getIdp3_p90(){
        this.idp3_p90 = getDouble("idp3_p90");
        return idp3_p90;
    }
    
    public void setIdp3_valor(double idp3_valor) {
        this.idp3_valor = idp3_valor;
        set("idp3_valor", idp3_valor);
	}
    public double getIdp3_valor(){
        this.idp3_valor = getDouble("idp3_valor");
        return idp3_valor;
    }
    
    public void setGdp3_p10(double gdp3_p10) {
        this.gdp3_p10 = gdp3_p10;
        set("gdp3_p10", gdp3_p10);
	}
    public double getGdp3_p10(){
        this.gdp3_p10 = getDouble("gdp3_p10");
        return gdp3_p10;
    }
    
    public void setGdp3_p90(double gdp3_p90) {
        this.gdp3_p90 = gdp3_p90;
        set("gdp3_p90", gdp3_p90);
	}
    public double getGdp3_p90(){
        this.gdp3_p90 = getDouble("gdp3_p90");
        return gdp3_p90;
    }
    
    public void setGdp3_valor(double gdp3_valor) {
        this.gdp3_valor = gdp3_valor;
        set("gdp3_valor", gdp3_valor);
	}
    public double getGdp3_valor(){
        this.gdp3_valor = getDouble("gdp3_valor");
        return gdp3_valor;
    }
    
    public void setMsp4_p10(double msp4_p10) {
        this.msp4_p10 = msp4_p10;
        set("msp4_p10", msp4_p10);
	}
    public double getMsp4_p10(){
        this.msp4_p10 = getDouble("msp4_p10");
        return msp4_p10;
    }
    
    public void setMsp4_p90(double msp4_p90) {
        this.msp4_p90 = msp4_p90;
        set("msp4_p90", msp4_p90);
	}
    public double getMsp4_p90(){
        this.msp4_p90 = getDouble("msp4_p90");
        return msp4_p90;
    }
    
    public void setMsp4_valor(double msp4_valor) {
        this.msp4_valor = msp4_valor;
        set("msp4_valor", msp4_valor);
	}
    public double getMsp4_valor(){
        this.msp4_valor = getDouble("msp4_valor");
        return msp4_valor;
    }
    
    public void setFbp4_p10(double fbp4_p10) {
        this.fbp4_p10 = fbp4_p10;
        set("fbp4_p10", fbp4_p10);
	}
    public double getFbp4_p10(){
        this.fbp4_p10 = getDouble("fbp4_p10");
        return fbp4_p10;
    }
    
    public void setFbp4_p90(double fbp4_p90) {
        this.fbp4_p90 = fbp4_p90;
        set("fbp4_p90", fbp4_p90);
	}
    public double getFbp4_p90(){
        this.fbp4_p90 = getDouble("fbp4_p90");
        return fbp4_p90;
    }
    
    public void setFbp4_valor(double fbp4_valor) {
        this.fbp4_valor = fbp4_valor;
        set("fbp4_valor", fbp4_valor);
	}
    public double getFbp4_valor(){
        this.fbp4_valor = getDouble("fbp4_valor");
        return fbp4_valor;
    }
    
    public void setOsp4_p10(double osp4_p10) {
        this.osp4_p10 = osp4_p10;
        set("osp4_p10", osp4_p10);
	}
    public double getOsp4_p10(){
        this.osp4_p10 = getDouble("osp4_p10");
        return osp4_p10;
    }
    
    public void setOsp4_p90(double osp4_p90) {
        this.osp4_p90 = osp4_p90;
        set("osp4_p90", osp4_p90);
	}
    public double getOsp4_p90(){
        this.osp4_p90 = getDouble("osp4_p90");
        return osp4_p90;
    }
    
    public void setOsp4_valor(double osp4_valor) {
        this.osp4_valor = osp4_valor;
        set("osp4_valor", osp4_valor);
	}
    public double getOsp4_valor(){
        this.osp4_valor = getDouble("osp4_valor");
        return osp4_valor;
    }
    
    public void setKrp4_p10(double krp4_p10) {
        this.krp4_p10 = krp4_p10;
        set("krp4_p10", krp4_p10);
	}
    public double getKrp4_p10(){
        this.krp4_p10 = getDouble("krp4_p10");
        return krp4_p10;
    }
    
    public void setKrp4_p90(double krp4_p90) {
        this.krp4_p90 = krp4_p90;
        set("krp4_p90", krp4_p90);
	}
    public double getKrp4_p90(){
        this.krp4_p90 = getDouble("krp4_p90");
        return krp4_p90;
    }
    
    public void setKrp4_valor(double krp4_valor) {
        this.krp4_valor = krp4_valor;
        set("krp4_valor", krp4_valor);
	}
    public double getKrp4_valor(){
        this.krp4_valor = getDouble("krp4_valor");
        return krp4_valor;
    }
    
    public void setIdp4_p10(double idp4_p10) {
        this.idp4_p10 = idp4_p10;
        set("idp4_p10", idp4_p10);
	}
    public double getIdp4_p10(){
        this.idp4_p10 = getDouble("idp4_p10");
        return idp4_p10;
    }
    
    public void setIdp4_p90(double idp4_p90) {
        this.idp4_p90 = idp4_p90;
        set("idp4_p90", idp4_p90);
	}
    public double getIdp4_p90(){
        this.idp4_p90 = getDouble("idp4_p90");
        return idp4_p90;
    }
    
    public void setIdp4_valor(double idp4_valor) {
        this.idp4_valor = idp4_valor;
        set("idp4_valor", idp4_valor);
	}
    public double getIdp4_valor(){
        this.idp4_valor = getDouble("idp4_valor");
        return idp4_valor;
    }
    
    public void setGdp4_p10(double gdp4_p10) {
        this.gdp4_p10 = gdp4_p10;
        set("gdp4_p10", gdp4_p10);
	}
    public double getGdp4_p10(){
        this.gdp4_p10 = getDouble("gdp4_p10");
        return gdp4_p10;
    }
    
    public void setGdp4_p90(double gdp4_p90) {
        this.gdp4_p90 = gdp4_p90;
        set("gdp4_p90", gdp4_p90);
	}
    public double getGdp4_p90(){
        this.gdp4_p90 = getDouble("gdp4_p90");
        return gdp4_p90;
    }
    
    public void setGdp4_valor(double gdp4_valor) {
        this.gdp4_valor = gdp4_valor;
        set("gdp4_valor", gdp4_valor);
	}
    public double getGdp4_valor(){
        this.gdp4_valor = getDouble("gdp4_valor");
        return gdp4_valor;
    }
    
    public void setMsp5_p10(double msp5_p10) {
        this.msp5_p10 = msp5_p10;
        set("msp5_p10", msp5_p10);
	}
    public double getMsp5_p10(){
        this.msp5_p10 = getDouble("msp5_p10");
        return msp5_p10;
    }
    
    public void setMsp5_p90(double msp5_p90) {
        this.msp5_p90 = msp5_p90;
        set("msp5_p90", msp5_p90);
	}
    public double getMsp5_p90(){
        this.msp5_p90 = getDouble("msp5_p90");
        return msp5_p90;
    }
    
    public void setMsp5_valor(double msp5_valor) {
        this.msp5_valor = msp5_valor;
        set("msp5_valor", msp5_valor);
	}
    public double getMsp5_valor(){
        this.msp5_valor = getDouble("msp5_valor");
        return msp5_valor;
    }
    
    public void setFbp5_p10(double fbp5_p10) {
        this.fbp5_p10 = fbp5_p10;
        set("fbp5_p10", fbp5_p10);
	}
    public double getFbp5_p10(){
        this.fbp5_p10 = getDouble("fbp5_p10");
        return fbp5_p10;
    }
    
    public void setFbp5_p90(double fbp5_p90) {
        this.fbp5_p90 = fbp5_p90;
        set("fbp5_p90", fbp5_p90);
	}
    public double getFbp5_p90(){
        this.fbp5_p90 = getDouble("fbp5_p90");
        return fbp5_p90;
    }
    
    public void setFbp5_valor(double fbp5_valor) {
        this.fbp5_valor = fbp5_valor;
        set("fbp5_valor", fbp5_valor);
	}
    public double getFbp5_valor(){
        this.fbp5_valor = getDouble("fbp5_valor");
        return fbp5_valor;
    }
    
    public void setOsp5_p10(double osp5_p10) {
        this.osp5_p10 = osp5_p10;
        set("osp5_p10", osp5_p10);
	}
    public double getOsp5_p10(){
        this.osp5_p10 = getDouble("osp5_p10");
        return osp5_p10;
    }
    
    public void setOsp5_p90(double osp5_p90) {
        this.osp5_p90 = osp5_p90;
        set("osp5_p90", osp5_p90);
	}
    public double getOsp5_p90(){
        this.osp5_p90 = getDouble("osp5_p90");
        return osp5_p90;
    }
    
    public void setOsp5_valor(double osp5_valor) {
        this.osp5_valor = osp5_valor;
        set("osp5_valor", osp5_valor);
	}
    public double getOsp5_valor(){
        this.osp5_valor = getDouble("osp5_valor");
        return osp5_valor;
    }
    
    public void setKrp5_p10(double krp5_p10) {
        this.krp5_p10 = krp5_p10;
        set("krp5_p10", krp5_p10);
	}
    public double getKrp5_p10(){
        this.krp5_p10 = getDouble("krp5_p10");
        return krp5_p10;
    }
    
    public void setKrp5_p90(double krp5_p90) {
        this.krp5_p90 = krp5_p90;
        set("krp5_p90", krp5_p90);
	}
    public double getKrp5_p90(){
        this.krp5_p90 = getDouble("krp5_p90");
        return krp5_p90;
    }
    
    public void setKrp5_valor(double krp5_valor) {
        this.krp5_valor = krp5_valor;
        set("krp5_valor", krp5_valor);
	}
    public double getKrp5_valor(){
        this.krp5_valor = getDouble("krp5_valor");
        return krp5_valor;
    }
    
    public void setMsp1(double msp1) {
        this.msp1 = msp1;
        set("msp1", msp1);
	}
    public double getMsp1(){
        this.msp1 = getDouble("msp1");
        return msp1;
    }
    
    public void setMsp2(double msp2) {
        this.msp2 = msp2;
        set("msp2", msp2);
	}
    public double getMsp2(){
        this.msp2 = getDouble("msp2");
        return msp2;
    }
    
    public void setMsp3(double msp3) {
        this.msp3 = msp3;
        set("msp3", msp3);
	}
    public double getMsp3(){
        this.msp3 = getDouble("msp3");
        return msp3;
    }
    
    public void setMsp4(double msp4) {
        this.msp4 = msp4;
        set("msp4", msp4);
	}
    public double getMsp4(){
        this.msp4 = getDouble("msp4");
        return msp4;
    }
    
    public void setMsp5(double msp5) {
        this.msp5 = msp5;
        set("msp5", msp5);
	}
    public double getMsp5(){
        this.msp5 = getDouble("msp5");
        return msp5;
    }
    
    public void setFbp1(double fbp1) {
        this.fbp1 = fbp1;
        set("fbp1", fbp1);
	}
    public double getFbp1(){
        this.fbp1 = getDouble("fbp1");
        return fbp1;
    }
    
    public void setFbp2(double fbp2) {
        this.fbp2 = fbp2;
        set("fbp2", fbp2);
	}
    public double getFbp2(){
        this.fbp2 = getDouble("fbp2");
        return fbp2;
    }
    
    public void setFbp3(double fbp3) {
        this.fbp3 = fbp3;
        set("fbp3", fbp3);
	}
    public double getFbp3(){
        this.fbp3 = getDouble("fbp3");
        return fbp3;
    }
    
    public void setFbp4(double fbp4) {
        this.fbp4 = fbp4;
        set("fbp4", fbp4);
	}
    public double getFbp4(){
        this.fbp4 = getDouble("fbp4");
        return fbp4;
    }
    
    public void setFbp5(double fbp5) {
        this.fbp5 = fbp5;
        set("fbp5", fbp5);
	}
    public double getFbp5(){
        this.fbp5 = getDouble("fbp5");
        return fbp5;
    }
    
    public void setOsp1(double osp1) {
        this.osp1 = osp1;
        set("osp1", osp1);
	}
    public double getOsp1(){
        this.osp1 = getDouble("osp1");
        return osp1;
    }
    
    public void setOsp2(double osp2) {
        this.osp2 = osp2;
        set("osp2", osp2);
	}
    public double getOsp2(){
        this.osp2 = getDouble("osp2");
        return osp2;
    }
    
    public void setOsp3(double osp3) {
        this.osp3 = osp3;
        set("osp3", osp3);
	}
    public double getOsp3(){
        this.osp3 = getDouble("osp3");
        return osp3;
    }
    
    public void setOsp4(double osp4) {
        this.osp4 = osp4;
        set("osp4", osp4);
	}
    public double getOsp4(){
        this.osp4 = getDouble("osp4");
        return osp4;
    }
    
    public void setOsp5(double osp5) {
        this.osp5 = osp5;
        set("osp5", osp5);
	}
    public double getOsp5(){
        this.osp5 = getDouble("osp5");
        return osp5;
    }
    
    public void setKrp1(double krp1) {
        this.krp1 = krp1;
        set("krp1", krp1);
	}
    public double getKrp1(){
        this.krp1 = getDouble("krp1");
        return krp1;
    }
    
    public void setKrp2(double krp2) {
        this.krp2 = krp2;
        set("krp2", krp2);
	}
    public double getKrp2(){
        this.krp2 = getDouble("krp2");
        return krp2;
    }
    
    public void setKrp3(double krp3) {
        this.krp3 = krp3;
        set("krp3", krp3);
	}
    public double getKrp3(){
        this.krp3 = getDouble("krp3");
        return krp3;
    }
    
    public void setKrp4(double krp4) {
        this.krp4 = krp4;
        set("krp4", krp4);
	}
    public double getKrp4(){
        this.krp4 = getDouble("krp4");
        return krp4;
    }
    
    public void setKrp5(double krp5) {
        this.krp5 = krp5;
        set("krp5", krp5);
	}
    public double getKrp5(){
        this.krp5 = getDouble("krp5");
        return krp5;
    }
    
    public void setIdp1(double idp1) {
        this.idp1 = idp1;
        set("idp1", idp1);
	}
    public double getIdp1(){
        this.idp1 = getDouble("idp1");
        return idp1;
    }
    
    public void setIdp2(double idp2) {
        this.idp2 = idp2;
        set("idp2", idp2);
	}
    public double getIdp2(){
        this.idp2 = getDouble("idp2");
        return idp2;
    }
    
    public void setIdp3(double idp3) {
        this.idp3 = idp3;
        set("idp3", idp3);
	}
    public double getIdp3(){
        this.idp3 = getDouble("idp3");
        return idp3;
    }
    
    public void setIdp4(double idp4) {
        this.idp4 = idp4;
        set("idp4", idp4);
	}
    public double getIdp4(){
        this.idp4 = getDouble("idp4");
        return idp4;
    }
    
    public void setGdp1(double gdp1) {
        this.gdp1 = gdp1;
        set("gdp1", gdp1);
	}
    public double getGdp1(){
        this.gdp1 = getDouble("gdp1");
        return gdp1;
    }
    
    public void setGdp2(double gdp2) {
        this.gdp2 = gdp2;
        set("gdp2", gdp2);
	}
    public double getGdp2(){
        this.gdp2 = getDouble("gdp2");
        return gdp2;
    }
    
    public void setGdp3(double gdp3) {
        this.gdp3 = gdp3;
        set("gdp3", gdp3);
	}
    public double getGdp3(){
        this.gdp3 = getDouble("gdp3");
        return gdp3;
    }
    
    public void setGdp4(double gdp4) {
        this.gdp4 = gdp4;
        set("gdp4", gdp4);
	}
    public double getGdp4(){
        this.gdp4 = getDouble("gdp4");
        return gdp4;
    }
    
    public void setStatistical_mineral_scales(double statistical_mineral_scales) {
        this.statistical_mineral_scales = statistical_mineral_scales;
        set("statistical_mineral_scales", statistical_mineral_scales);
	}
    public double getStatistical_mineral_scales(){
        this.statistical_mineral_scales = getDouble("statistical_mineral_scales");
        return statistical_mineral_scales;
    }
    
    public void setStatistical_fines_blockage(double statistical_fines_blockage) {
        this.statistical_fines_blockage = statistical_fines_blockage;
        set("statistical_fines_blockage", statistical_fines_blockage);
	}
    public double getStatistical_fines_blockage(){
        this.statistical_fines_blockage = getDouble("statistical_fines_blockage");
        return statistical_fines_blockage;
    }
    
    public void setStatistical_organic_scales(double statistical_organic_scales) {
        this.statistical_organic_scales = statistical_organic_scales;
        set("statistical_organic_scales", statistical_organic_scales);
	}
    public double getStatistical_organic_scales(){
        this.statistical_organic_scales = getDouble("statistical_organic_scales");
        return statistical_organic_scales;
    }
    
    public void setStatistical_relative_permeability(double statistical_relative_permeability) {
        this.statistical_relative_permeability = statistical_relative_permeability;
        set("statistical_relative_permeability", statistical_relative_permeability);
	}
    public double getStatistical_relative_permeability(){
        this.statistical_relative_permeability = getDouble("statistical_relative_permeability");
        return statistical_relative_permeability;
    }
    
    public void setStatistical_induced_damage(double statistical_induced_damage) {
        this.statistical_induced_damage = statistical_induced_damage;
        set("statistical_induced_damage", statistical_induced_damage);
	}
    public double getStatistical_induced_damage(){
        this.statistical_induced_damage = getDouble("statistical_induced_damage");
        return statistical_induced_damage;
    }
    
    public void setStatistical_geomechanical_damage(double statistical_geomechanical_damage) {
        this.statistical_geomechanical_damage = statistical_geomechanical_damage;
        set("statistical_geomechanical_damage", statistical_geomechanical_damage);
	}
    public double getStatistical_geomechanical_damage(){
        this.statistical_geomechanical_damage = getDouble("statistical_geomechanical_damage");
        return statistical_geomechanical_damage;
    }
    
    public void setStatistical_total(double statistical_total) {
        this.statistical_total = statistical_total;
        set("statistical_total", statistical_total);
	}
    public double getStatistical_total(){
        this.statistical_total = getDouble("statistical_total");
        return statistical_total;
    }
    
    public void setStatistical_mineral_scales_avg(double statistical_mineral_scales_avg) {
        this.statistical_mineral_scales_avg = statistical_mineral_scales_avg;
        set("statistical_mineral_scales_avg", statistical_mineral_scales_avg);
	}
    public double getStatistical_mineral_scales_avg(){
        this.statistical_mineral_scales_avg = getDouble("statistical_mineral_scales_avg");
        return statistical_mineral_scales_avg;
    }
    
    public void setStatistical_fines_blockage_avg(double statistical_fines_blockage_avg) {
        this.statistical_fines_blockage_avg = statistical_fines_blockage_avg;
        set("statistical_fines_blockage_avg", statistical_fines_blockage_avg);
	}
    public double getStatistical_fines_blockage_avg(){
        this.statistical_fines_blockage_avg = getDouble("statistical_fines_blockage_avg");
        return statistical_fines_blockage_avg;
    }
    
    public void setStatistical_organic_scales_avg(double statistical_organic_scales_avg) {
        this.statistical_organic_scales_avg = statistical_organic_scales_avg;
        set("statistical_organic_scales_avg", statistical_organic_scales_avg);
	}
    public double getStatistical_organic_scales_avg(){
        this.statistical_organic_scales_avg = getDouble("statistical_organic_scales_avg");
        return statistical_organic_scales_avg;
    }
    
    public void setStatistical_relative_permeability_avg(double statistical_relative_permeability_avg) {
        this.statistical_relative_permeability_avg = statistical_relative_permeability_avg;
        set("statistical_relative_permeability_avg", statistical_relative_permeability_avg);
	}
    public double getStatistical_relative_permeability_avg(){
        this.statistical_relative_permeability_avg = getDouble("statistical_relative_permeability_avg");
        return statistical_relative_permeability_avg;
    }
    
    public void setStatistical_induced_damage_avg(double statistical_induced_damage_avg) {
        this.statistical_induced_damage_avg = statistical_induced_damage_avg;
        set("statistical_induced_damage_avg", statistical_induced_damage_avg);
	}
    public double getStatistical_induced_damage_avg(){
        this.statistical_induced_damage_avg = getDouble("statistical_induced_damage_avg");
        return statistical_induced_damage_avg;
    }
    
    public void setStatistical_geomechanical_damage_avg(double statistical_geomechanical_damage_avg) {
        this.statistical_geomechanical_damage_avg = statistical_geomechanical_damage_avg;
        set("statistical_geomechanical_damage_avg", statistical_geomechanical_damage_avg);
	}
    public double getStatistical_geomechanical_damage_avg(){
        this.statistical_geomechanical_damage_avg = getDouble("statistical_geomechanical_damage_avg");
        return statistical_geomechanical_damage_avg;
    }
    
    public void setSkin_mineral_scales(double skin_mineral_scales) {
        this.skin_mineral_scales = skin_mineral_scales;
        set("skin_mineral_scales", skin_mineral_scales);
	}
    public double getSkin_mineral_scales(){
        this.skin_mineral_scales = getDouble("skin_mineral_scales");
        return skin_mineral_scales;
    }
    
    public void setSkin_fines_blockage(double skin_fines_blockage) {
        this.skin_fines_blockage = skin_fines_blockage;
        set("skin_fines_blockage", skin_fines_blockage);
	}
    public double getSkin_fines_blockage(){
        this.skin_fines_blockage = getDouble("skin_fines_blockage");
        return skin_fines_blockage;
    }
    
    public void setSkin_organic_scales(double skin_organic_scales) {
        this.skin_organic_scales = skin_organic_scales;
        set("skin_organic_scales", skin_organic_scales);
	}
    public double getSkin_organic_scales(){
        this.skin_organic_scales = getDouble("skin_organic_scales");
        return skin_organic_scales;
    }
    
    public void setSkin_relative_permeability(double skin_relative_permeability) {
        this.skin_relative_permeability = skin_relative_permeability;
        set("skin_relative_permeability", skin_relative_permeability);
	}
    public double getSkin_relative_permeability(){
        this.skin_relative_permeability = getDouble("skin_relative_permeability");
        return skin_relative_permeability;
    }
    
    public void setSkin_induced_damage(double skin_induced_damage) {
        this.skin_induced_damage = skin_induced_damage;
        set("skin_induced_damage", skin_induced_damage);
	}
    public double getSkin_induced_damage(){
        this.skin_induced_damage = getDouble("skin_induced_damage");
        return skin_induced_damage;
    }
    
    public void setSkin_geomechanical_damage(double skin_geomechanical_damage) {
        this.skin_geomechanical_damage = skin_geomechanical_damage;
        set("skin_geomechanical_damage", skin_geomechanical_damage);
	}
    public double getSkin_geomechanical_damage(){
        this.skin_geomechanical_damage = getDouble("skin_geomechanical_damage");
        return skin_geomechanical_damage;
    }
    
    public void setSkin_total(double skin_total) {
        this.skin_total = skin_total;
        set("skin_total", skin_total);
	}
    public double getSkin_total(){
        this.skin_total = getDouble("skin_total");
        return skin_total;
    }
    
    public void setSkin_mineral_scales_avg(double skin_mineral_scales_avg) {
        this.skin_mineral_scales_avg = skin_mineral_scales_avg;
        set("skin_mineral_scales_avg", skin_mineral_scales_avg);
	}
    public double getSkin_mineral_scales_avg(){
        this.skin_mineral_scales_avg = getDouble("skin_mineral_scales_avg");
        return skin_mineral_scales_avg;
    }
    
    public void setSkin_fines_blockage_avg(double skin_fines_blockage_avg) {
        this.skin_fines_blockage_avg = skin_fines_blockage_avg;
        set("skin_fines_blockage_avg", skin_fines_blockage_avg);
	}
    public double getSkin_fines_blockage_avg(){
        this.skin_fines_blockage_avg = getDouble("skin_fines_blockage_avg");
        return skin_fines_blockage_avg;
    }
    
    public void setSkin_organic_scales_avg(double skin_organic_scales_avg) {
        this.skin_organic_scales_avg = skin_organic_scales_avg;
        set("skin_organic_scales_avg", skin_organic_scales_avg);
	}
    public double getSkin_organic_scales_avg(){
        this.skin_organic_scales_avg = getDouble("skin_organic_scales_avg");
        return skin_organic_scales_avg;
    }
    
    public void setSkin_relative_permeability_avg(double skin_relative_permeability_avg) {
        this.skin_relative_permeability_avg = skin_relative_permeability_avg;
        set("skin_relative_permeability_avg", skin_relative_permeability_avg);
	}
    public double getSkin_relative_permeability_avg(){
        this.skin_relative_permeability_avg = getDouble("skin_relative_permeability_avg");
        return skin_relative_permeability_avg;
    }
    
    public void setSkin_induced_damage_avg(double skin_induced_damage_avg) {
        this.skin_induced_damage_avg = skin_induced_damage_avg;
        set("skin_induced_damage_avg", skin_induced_damage_avg);
	}
    public double getSkin_induced_damage_avg(){
        this.skin_induced_damage_avg = getDouble("skin_induced_damage_avg");
        return skin_induced_damage_avg;
    }
    
    public void setSkin_geomechanical_damage_avg(double skin_geomechanical_damage_avg) {
        this.skin_geomechanical_damage_avg = skin_geomechanical_damage_avg;
        set("skin_geomechanical_damage_avg", skin_geomechanical_damage_avg);
	}
    public double getSkin_geomechanical_damage_avg(){
        this.skin_geomechanical_damage_avg = getDouble("skin_geomechanical_damage_avg");
        return skin_geomechanical_damage_avg;
    }
    
    public void setMineral_scales_avg(double mineral_scales_avg) {
        this.mineral_scales_avg = mineral_scales_avg;
        set("mineral_scales_avg", mineral_scales_avg);
	}
    public double getMineral_scales_avg(){
        this.mineral_scales_avg = getDouble("mineral_scales_avg");
        return mineral_scales_avg;
    }
    
    public void setFines_blockage_avg(double fines_blockage_avg) {
        this.fines_blockage_avg = fines_blockage_avg;
        set("fines_blockage_avg", fines_blockage_avg);
	}
    public double getFines_blockage_avg(){
        this.fines_blockage_avg = getDouble("fines_blockage_avg");
        return fines_blockage_avg;
    }
    
    public void setOrganic_scales_avg(double organic_scales_avg) {
        this.organic_scales_avg = organic_scales_avg;
        set("organic_scales_avg", organic_scales_avg);
	}
    public double getOrganic_scales_avg(){
        this.organic_scales_avg = getDouble("organic_scales_avg");
        return organic_scales_avg;
    }
    
    public void setRelative_permeability_avg(double relative_permeability_avg) {
        this.relative_permeability_avg = relative_permeability_avg;
        set("relative_permeability_avg", relative_permeability_avg);
	}
    public double getRelative_permeability_avg(){
        this.relative_permeability_avg = getDouble("relative_permeability_avg");
        return relative_permeability_avg;
    }
    
    public void setInduced_damage_avg(double induced_damage_avg) {
        this.induced_damage_avg = induced_damage_avg;
        set("induced_damage_avg", induced_damage_avg);
	}
    public double getInduced_damage_avg(){
        this.induced_damage_avg = getDouble("induced_damage_avg");
        return induced_damage_avg;
    }
    
    public void setGeomechanical_damage_avg(double geomechanical_damage_avg) {
        this.geomechanical_damage_avg = geomechanical_damage_avg;
        set("geomechanical_damage_avg", geomechanical_damage_avg);
	}
    public double getGeomechanical_damage_avg(){
        this.geomechanical_damage_avg = getDouble("geomechanical_damage_avg");
        return geomechanical_damage_avg;
    }
    


	
	// LOGIC

	 public void statistical_mineral_scales() {
	        msp1 = (this.msp1_valor - this.msp1_p10)/(this.msp1_p90-this.msp1_p10);
	        msp2 = (this.msp2_valor - this.msp2_p10)/(this.msp2_p90-this.msp2_p10);
	        msp3 = (this.msp3_valor - this.msp3_p10)/(this.msp3_p90-this.msp3_p10);
	        msp4 = (this.msp4_valor - this.msp4_p10)/(this.msp4_p90-this.msp4_p10);
	        msp5 = (this.msp5_valor - this.msp5_p10)/(this.msp5_p90-this.msp5_p10);        
	        
	        statistical_mineral_scales = (msp1 + msp2 + msp3 + msp4 + msp5)/5;        
	    }

	    public void statistical_fines_blockage() {
	        fbp1 = (this.fbp1_valor - this.fbp1_p10)/(this.fbp1_p90-this.fbp1_p10);
	        fbp2 = (this.fbp2_valor - this.fbp2_p10)/(this.fbp2_p90-this.fbp2_p10);
	        fbp3 = (this.fbp3_valor - this.fbp3_p10)/(this.fbp3_p90-this.fbp3_p10);
	        fbp4 = (this.fbp4_valor - this.fbp4_p10)/(this.fbp4_p90-this.fbp4_p10);
	        fbp5 = (this.fbp5_valor - this.fbp5_p10)/(this.fbp5_p90-this.fbp5_p10);
	        
	        statistical_fines_blockage = (fbp1 + fbp2 + fbp3 + fbp4 + fbp5)/5;
	    }

	    public void statistical_organic_scales() {
	        osp1 = (this.osp1_valor - this.osp1_p10)/(this.osp1_p90-this.osp1_p10);
	        osp2 = (this.osp2_valor - this.osp2_p10)/(this.osp2_p90-this.osp2_p10);
	        osp3 = (this.osp3_valor - this.osp3_p10)/(this.osp3_p90-this.osp3_p10);
	        osp4 = (this.osp4_valor - this.osp4_p10)/(this.osp4_p90-this.osp4_p10);
	        osp5 = (this.osp5_valor - this.osp5_p10)/(this.osp5_p90-this.osp5_p10);
	        
	        statistical_organic_scales = ((osp1 + osp2 + osp3 + osp4 + osp5)/5)*osp5;
	    }

	    public void statistical_relative_permeability() {
	        krp1 = (this.krp1_valor - this.krp1_p10)/(this.krp1_p90-this.krp1_p10);
	        krp2 = (this.krp2_valor - this.krp2_p10)/(this.krp2_p90-this.krp2_p10);
	        krp3 = (this.krp3_valor - this.krp3_p10)/(this.krp3_p90-this.krp3_p10);
	        krp4 = (this.krp4_valor - this.krp4_p10)/(this.krp4_p90-this.krp4_p10);
	        krp5 = (this.krp5_valor - this.krp5_p10)/(this.krp5_p90-this.krp5_p10);
	        
	        statistical_relative_permeability = ((krp1 + krp2 + krp3 + krp4)/4)*krp5;
	    }

	    public void statistical_induced_damage() {
	        idp1 = (this.idp1_valor - this.idp1_p10)/(this.idp1_p90-this.idp1_p10);
	        idp2 = (this.idp2_valor - this.idp2_p10)/(this.idp2_p90-this.idp2_p10);
	        idp3 = (this.idp3_valor - this.idp3_p10)/(this.idp3_p90-this.idp3_p10);
	        idp4 = (this.idp4_valor - this.idp4_p10)/(this.idp4_p90-this.idp4_p10);

	        statistical_induced_damage = (idp1 + idp2 + idp3 + idp4)/4;
	    }

	    public void statistical_geomechanical_damage() {
	        gdp1 = (this.gdp1_valor - this.gdp1_p10)/(this.gdp1_p90-this.gdp1_p10);
	        gdp2 = (this.gdp2_valor - this.gdp2_p10)/(this.gdp2_p90-this.gdp2_p10);
	        gdp3 = (this.gdp3_valor - this.gdp3_p10)/(this.gdp3_p90-this.gdp3_p10);
	        gdp4 = (this.gdp4_valor - this.gdp4_p10)/(this.gdp4_p90-this.gdp4_p10);

	        statistical_geomechanical_damage = (gdp1 + gdp2 + gdp3 + gdp4)/4;
	    }

	    public void statistical_averages() {
	        statistical_total =     statistical_mineral_scales + statistical_fines_blockage + statistical_organic_scales + statistical_relative_permeability + statistical_induced_damage + statistical_geomechanical_damage;

	        statistical_mineral_scales_avg = (statistical_mineral_scales/statistical_total)*100;
	        statistical_fines_blockage_avg = (statistical_fines_blockage/statistical_total)*100;
	        statistical_organic_scales_avg = (statistical_organic_scales/statistical_total)*100;
	        statistical_relative_permeability_avg = (statistical_relative_permeability/statistical_total)*100;
	        statistical_induced_damage_avg = (statistical_induced_damage/statistical_total)*100;
	        statistical_geomechanical_damage_avg = (statistical_geomechanical_damage/statistical_total)*100;
	    }

	    public void skin_calculations() {
	        
	        double Pr=this.bhp;
	        double Pr_Pc = Pr - this.pc_mineral_scales;
	        double radius = this.rw;
	        
	        while (Pr_Pc < 0 && radius < this.re){
	            radius = radius + 0.05;
	            radius = redondear( radius, 2);
	            if(this.type_well.equals("Oil")){
	                Pr = this.bhp + (((141.2*this.qo*this.uo*this.bo)/(this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }else{
	                Pr = this.bhp + (((141.2*this.qg*(1000000)*this.ug*this.bg)/(5.615*this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }
	            Pr_Pc = Pr - this.pc_mineral_scales;
	        }
	        //Cuando pasa a positivo se pasa un paso
	        radius = radius - 0.05;
	        
	        double rd1 = radius + rw;
	        skin_mineral_scales = (permeability/(mineral_scales*permeability)-1)*Math.log(rd1/rw);
	        
	        double rd2 = this.fbp3_valor + rw;
	        skin_fines_blockage = (permeability/(fines_blockage*permeability)-1)*Math.log(rd2/rw);
	        
	        Pr=this.bhp;
	        Pr_Pc = Pr - this.pc_organic_scales;
	        radius = this.rw;
	        
	        while (Pr_Pc < 0 && radius<this.re){
	            radius = radius + 0.05;
	            radius = redondear( radius, 2);
	            if(this.type_well.equals("Oil")){
	                Pr = this.bhp + (((141.2*this.qo*this.uo*this.bo)/(this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }else{
	                Pr = this.bhp + (((141.2*this.qg*(1000000)*this.ug*this.bg)/(5.615*this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }
	            Pr_Pc = Pr - this.pc_organic_scales;
	        }
	        //Cuando pasa a positivo se pasa un paso
	        radius = radius - 0.05;
	        
	        double rd3 = radius + rw;
	        skin_organic_scales =  (permeability/(organic_scales*permeability)-1)*Math.log(rd3/rw);

	        
	        if(this.reservoir_pressure<this.saturation_pressure){
	            radius = this.re;
	        } else {
	            Pr=this.bhp;
	            Pr_Pc = Pr - this.saturation_pressure;
	            radius = this.rw;

	            while (Pr_Pc < 0 && radius<this.re){
	                radius = radius + 0.05;
	                radius = redondear( radius, 2);

	                if(this.type_well.equals("Oil")){
	                    Pr = this.bhp + (((141.2*this.qo*this.uo*this.bo)/(this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	                }else{
	                    Pr = this.bhp + (((141.2*this.qg*(1000000)*this.ug*this.bg)/(5.615*this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	                }
	                Pr_Pc = Pr - this.saturation_pressure;
	            }
	            //Cuando pasa a positivo se pasa un paso
	            radius = radius - 0.05;
	        }
	        
	        double rd4 = radius + rw; 
	        skin_relative_permeability = (permeability/(relative_permeability*permeability)-1)*Math.log(rd4/rw);
	        
	        double rd5 = Math.sqrt(this.idp3_valor/(Math.PI*this.netpay*(this.porosity/100)))+rw;
	        skin_induced_damage =  (permeability/(induced_damage*permeability)-1)*Math.log(rd5/rw);
	        
	        double Pr_min = this.bhp;
	        double Pr_max = 0;
	        radius = this.re;
	        if(this.type_well.equals("Oil")){
	            Pr_max = this.bhp + (((141.2*this.qo*this.uo*this.bo)/(this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	        }else{
	            Pr_max = this.bhp + (((141.2*this.qg*(1000000)*this.ug*this.bg)/(5.615*this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	        }
	        
	        Pr=this.bhp;
	        radius = this.rw;
	        radius = redondear( radius, 2);
	        double cummulative_dd = 0;

	        while (radius<this.re && cummulative_dd < 0.25){
	            radius = radius + 0.05;
	            radius = redondear( radius, 2);
	            
	            if(this.type_well.equals("Oil")){
	                Pr = this.bhp + (((141.2*this.qo*this.uo*this.bo)/(this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }else{
	                Pr = this.bhp + (((141.2*this.qg*(1000000)*this.ug*this.bg)/(5.615*this.netpay*this.kc))*Math.log(radius/this.rw))-(0.5*(radius/Math.pow(this.re, 2)));
	            }
	            
	            cummulative_dd = 1 - (Pr_max - Pr)/(Pr_max - Pr_min);
	        }
	        radius = radius - 0.05;
	        
	        double rd6 = radius + rw;
	        skin_geomechanical_damage = (permeability/(geomechanical_damage*permeability)-1)*Math.log(rd6/rw);        
	    }


	    public void skin_averages() {
	        skin_total = skin_mineral_scales + skin_fines_blockage + skin_organic_scales + skin_relative_permeability + skin_induced_damage + skin_geomechanical_damage;

	        skin_mineral_scales_avg = (skin_mineral_scales/skin_total)*100;
	        skin_fines_blockage_avg = (skin_fines_blockage/skin_total)*100;
	        skin_organic_scales_avg = (skin_organic_scales/skin_total)*100;
	        skin_relative_permeability_avg = (skin_relative_permeability/skin_total)*100;
	        skin_induced_damage_avg = (skin_induced_damage/skin_total)*100;
	        skin_geomechanical_damage_avg = (skin_geomechanical_damage/skin_total)*100;
	    }

	    public void avegares() {
	        mineral_scales_avg = (statistical_mineral_scales_avg + skin_mineral_scales_avg)/2;
	        fines_blockage_avg = (statistical_fines_blockage_avg + skin_fines_blockage_avg)/2;
	        organic_scales_avg = (statistical_organic_scales_avg + skin_organic_scales_avg)/2;
	        relative_permeability_avg = (statistical_relative_permeability_avg + skin_relative_permeability_avg)/2;
	        induced_damage_avg = (statistical_induced_damage_avg + skin_induced_damage_avg)/2;
	        geomechanical_damage_avg = (statistical_geomechanical_damage_avg + skin_geomechanical_damage_avg)/2;
	        
	    }

	    public void spiderchart(SwingNode swingNode1, Tab tab_SkinDiagram) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "MG_UZ";
	        
	        datast.addValue(mineral_scales_avg, series1, "MINERAL SCALES (MSP)");
	        datast.addValue(fines_blockage_avg, series1, "FINES BLOCKAGE (FBP)");
	        datast.addValue(organic_scales_avg, series1, "ORGANIC SCALES (OSP)");
	        datast.addValue(relative_permeability_avg, series1, "RELATIVE PERMEABILITY (KrP)");
	        datast.addValue(induced_damage_avg, series1, "INDUCED DAMAGE (IDP)");
	        datast.addValue(geomechanical_damage_avg, series1, "GEOMECHANICAL DAMAGE (GDP)");                                                                                                                                                       

	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2) + "%";
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Multiparametric Characterization Diagram",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode1.setContent(frame);    
	        tab_SkinDiagram.setDisable(false);        
	    }

	    public void spiderchart_parameter1(SwingNode swingNode2, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_mineral_scales";
	        
	        datast.addValue(msp1, series1, "MSP1");
	        datast.addValue(msp2, series1, "MSP2");
	        datast.addValue(msp3, series1, "MSP3");
	        datast.addValue(msp4, series1, "MSP4");
	        datast.addValue(msp5, series1, "MSP5");

	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Statistical Mineral Scales",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode2.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public void spiderchart_parameter2(SwingNode swingNode3, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_fines_blockage";
	        
	        datast.addValue(fbp1, series1, "FBP1");
	        datast.addValue(fbp2, series1, "FBP2");
	        datast.addValue(fbp3, series1, "FBP3");
	        datast.addValue(fbp4, series1, "FBP4");
	        datast.addValue(fbp5, series1, "FBP5");
	        
	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Statistical Fines Blockage",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode3.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public void spiderchart_parameter3(SwingNode swingNode4, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_organic_scales";
	        
	        datast.addValue(osp1, series1, "OSP1");
	        datast.addValue(osp2, series1, "OSP2");
	        datast.addValue(osp3, series1, "OSP3");
	        datast.addValue(osp4, series1, "OSP4");
	        datast.addValue(osp5, series1, "OSP5");
	        
	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Statistical Organic Scales",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode4.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public void spiderchart_parameter4(SwingNode swingNode5, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_relative_permeability";
	        
	        datast.addValue(krp1, series1, "KRP1");
	        datast.addValue(krp2, series1, "KRP2");
	        datast.addValue(krp3, series1, "KRP3");
	        datast.addValue(krp4, series1, "KRP4");
	        datast.addValue(krp5, series1, "KRP5");
	        
	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Statistical Relative Permeability",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode5.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public void spiderchart_parameter5(SwingNode swingNode6, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_induced_damage";
	        
	        datast.addValue(idp1, series1, "IDP1");
	        datast.addValue(idp2, series1, "IDP2");
	        datast.addValue(idp3, series1, "IDP3");
	        datast.addValue(idp4, series1, "IDP4");
	        
	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });
	        

	        JFreeChart chart = new JFreeChart("Statistical Induced Damage",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode6.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public void spiderchart_parameter6(SwingNode swingNode7, Tab tab_SkinParameters) {
	        DefaultCategoryDataset datast = new DefaultCategoryDataset();
	        String series1 = "statistical_geomechanical_damage";
	        
	        datast.addValue(gdp1, series1, "GDP1");
	        datast.addValue(gdp2, series1, "GDP2");
	        datast.addValue(gdp3, series1, "GDP3");
	        datast.addValue(gdp4, series1, "GDP4");
	        
	        SpiderWebPlot webPlot = new SpiderWebPlot(datast);
	        Font labelFont = new Font("Arial", Font.BOLD, 10);
	        webPlot.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        webPlot.setOutlineVisible(false);
	        webPlot.setLabelFont(labelFont);
	        webPlot.setSeriesPaint(0, java.awt.Color.red);
	        webPlot.setLabelGenerator(new StandardCategoryItemLabelGenerator() {
	            @Override
	            public String generateColumnLabel(CategoryDataset dataset, int col) {
	                return dataset.getColumnKey(col) + ": " + redondear( dataset.getValue(0, col).doubleValue(), 2);
	            }
	        });

	        JFreeChart chart = new JFreeChart("Statistical Geomechanical Damage",null/* JFreeChart.DEFAULT_TITLE_FONT */,webPlot,false);
	        chart.setBackgroundPaint(java.awt.Color.decode("#f4f4f4"));
	        chart.setBorderVisible(false);
	        
	        ChartPanel frame = new ChartPanel(chart);
	        swingNode7.setContent(frame);    
	        tab_SkinParameters.setDisable(false);        
	    }
	    
	    public double redondear( double numero, int decimales ) {
	        return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	    }    
}
