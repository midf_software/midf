package application.models;

import org.javalite.activejdbc.annotations.Table;

@Table("krs_gas_values")
public class KrsGasValue extends BaseModel{
	private double sg;
	private double pcog;
	private double krg;
	private double kro;
	
	public KrsGasValue() {
		super();
	}
	
	public KrsGasValue(double sg, double pcog, double krg, double kro){
		super();
		this.setSg(sg);
		this.setPcog(pcog);
		this.setKrg(krg);
		this.setKro(kro);
	}
	public double getSg() {
		sg = getDouble("sg");
		return sg;
	}
	public void setSg(double sg) {
		this.sg = sg;
		set("sg", sg);
	}
	public double getPcog() {
		pcog = getDouble("pcog");
		return pcog;
	}
	public void setPcog(double pcog) {
		this.pcog = pcog;
		set("pcog", pcog);
	}
	public double getKrg() {
		krg = getDouble("krg");
		return krg;
	}
	public void setKrg(double krg) {
		this.krg = krg;
		set("krg", krg);
	}
	public double getKro() {
		kro = getDouble("kro");
		return kro;
	}
	public void setKro(double kro) {
		this.kro = kro;
		set("kro", kro);
	}
}
