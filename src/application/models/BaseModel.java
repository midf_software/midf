package application.models;
import java.util.ArrayList;
import java.util.List;

import org.javalite.activejdbc.Model;

public abstract class BaseModel extends Model{
	
	/**
	 * Si obj es nulo devuelve el modelo relacionado almacenado en la BD. de lo contrario devuelve obj. 
	 * @param c: Clase del modelo relacionado
	 * @param obj: instancia actual del modelo relacionado
	 * @param parent: True si el modelo relacionado es padre de la instancia actual.
	 * @return
	 */
	public <T extends Model> T getRelatedModel(Class<T> c, Object obj, boolean parent) {
		if (obj != null) {
			return (T) obj;
		}
		
		if(parent) {
			return (T) parent(c);
		}
		else {
			List<T> list = getAll(c);
			if (list.size() == 0) {
				return null;
			}
			return (T) list.get(0);
		}
		
	}
	
	public <T extends Model> T getRelatedModel(Class<T> c, Object obj) {
		return this.getRelatedModel(c, obj, false);
	}
	
	public void addRelatedModel(Model o) {
		//TODO delete old value?
		
		add(o);
	}
	
	public <T extends Model> List<T> getAllRelatedModels(List<T> list, Class<T> clazz){
		if(list == null || list.isEmpty()){
			return new ArrayList<T>(getAll(clazz));
		}
		return list;
	}
	
	
	public Integer getPk(){
		   return getInteger("id");
		}
}
