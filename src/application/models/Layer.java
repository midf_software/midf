package application.models;

import java.util.ArrayList;
import java.util.List;

public class Layer extends BaseModel {

	private GeologicalUnit gu;
	private int layerNumber;
	private List<InitialCondition> initialConditions = new ArrayList<InitialCondition>();
	private List<FormationCondition> formationConditions = new ArrayList<FormationCondition>();
	private List<PerforationCondition> perforationConditions = new ArrayList<PerforationCondition>();

	private double thickness;

	public int getLayerNumber() {
		layerNumber = getInteger("layernum");
		return layerNumber;
	}

	public void setLayerNumber(int layerNumber) {
		this.layerNumber = layerNumber;
		set("layernum", layerNumber);
	}

	public GeologicalUnit getGu() {
		gu = getRelatedModel(GeologicalUnit.class, gu, true);
		return gu;
	}

	public void setGu(GeologicalUnit gu) {
		this.gu = gu;
		setParent(gu); // TODO check this
		saveIt();
	}

	public List<FormationCondition> getFormationConditions() {

		List<FormationCondition> list = getAllRelatedModels(
				formationConditions, FormationCondition.class);
		formationConditions = list;
		return formationConditions;
	}

	@Deprecated
	public void setFormationConditions(
			List<FormationCondition> formationConditions) {
		this.formationConditions = formationConditions;
	}

	public List<InitialCondition> getInitialConditions() {
		List<InitialCondition> list = getAllRelatedModels(initialConditions,
				InitialCondition.class);
		initialConditions = list;
		return initialConditions;
	}

	@Deprecated
	public void setInitialConditions(List<InitialCondition> initialConditions) {
		this.initialConditions = initialConditions;
	}

	public void addInitialCondition(InitialCondition ic) {
		ic.setLayer(this);
		this.initialConditions.add(ic);
		add(ic); // orm add
	}

	public void addFormationCondition(FormationCondition fc) {
		fc.setLayer(this);
		this.formationConditions.add(fc);
		add(fc); // orm add
	}

	public void removeInitialCondition(InitialCondition ic) {
		this.initialConditions.remove(ic);
		// TODO orm this
	}

	public void removeFormationCondition(FormationCondition fc) {
		this.formationConditions.remove(fc);
		// TODO orm this
	}

	public double getThickness() {
		thickness = getDouble("thickness");
		return thickness;
	}

	public void setThickness(double thickness) {
		this.thickness = thickness;
		set("thickness", thickness);
	}

	public List<PerforationCondition> getPerforationConditions() {
		List<PerforationCondition> list = getAllRelatedModels(perforationConditions,
				PerforationCondition.class);
		perforationConditions = list;
		return perforationConditions;
	}

	public void addPerforationCondition(PerforationCondition pc) {
		pc.setLayer(this);
		this.perforationConditions.add(pc); // TODO orm
		add(pc);
	}

	public void setPerforationConditions(
			List<PerforationCondition> perforationConditions) {
		this.perforationConditions = perforationConditions;
		// for (PerforationCondition pc : perforationConditions) {
		// add(pc); // orm list
		// }
	}
}