package application.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Malla que discretiza la formacion del yacimiento.
 * 
 * @author Jeyson Molina.
 *
 */
public class Grid extends BaseModel {

	private double radialNumblocks; // Num. De bloques en dirección radial
	private double angularNumblocks; // Num. bloques en direccion angular
	private double verticalNumblocks; // Num. De bloques en dirección vertical
	private List<GeologicalUnit> guList = new ArrayList<GeologicalUnit>(); // Lista
																			// de
																			// unidades
																			// geologicas
	private List<Layer> layerList = new ArrayList<Layer>(); // lista de capas
															// verticales
	private BaseData bd;

	public double getRadialNumblocks() {
		radialNumblocks = getDouble("radial_numblocks");
		return radialNumblocks;
	}

	public void setRadialNumblocks(double radialNumblocks) {
		this.radialNumblocks = radialNumblocks;
		set("radial_numblocks", radialNumblocks);
	}

	public double getAngularNumblocks() {
		angularNumblocks = getDouble("angular_numblocks");
		return angularNumblocks;
	}

	public void setAngularNumblocks(double angularNumblocks) {
		this.angularNumblocks = angularNumblocks;
		set("angular_numblocks", angularNumblocks);
	}

	public double getVerticalNumblocks() {
		this.verticalNumblocks = getDouble("vertical_numblocks");
		return verticalNumblocks;
	}

	public void setVerticalNumblocks(double verticalNumblocks) {
		this.verticalNumblocks = verticalNumblocks;
		set("vertical_numblocks", verticalNumblocks);
	}

	// TODO No usar porque es peligroso con el orm. eliminar
	@Deprecated
	public void setGuList(List<GeologicalUnit> guList) {
		this.guList = guList;
	}

	public List<GeologicalUnit> getGuList() {
		List<GeologicalUnit> list = getAllRelatedModels(guList,
				GeologicalUnit.class);
		guList = list; // TODO list y guList siempre son iguales?
		
		// keep data consistency
		for(GeologicalUnit gu : guList){
			gu.setGrid(this);
		}
		
		return guList;
	}

	public void addGU(GeologicalUnit gu) {
		this.guList.add(gu);
		addRelatedModel(gu); // TODO doble add? no es redundante esto?
	}

	public void removeGU(int index) {
		this.guList.remove(index);
		// TODO orm this
	}
	public void removeGU(GeologicalUnit ug) {
		this.guList.remove(ug);
		// TODO orm this
		this.remove(ug);
	}
	public void addLayer(Layer ly) {
		this.layerList.add(ly);
		addRelatedModel(ly);
	}

	public void removeLayer(int index) {
		this.layerList.remove(index);
		// TODO orm this
		
	}

	public void removeLayer(Layer ly) {
		this.layerList.remove(ly);
		// TODO orm this
		this.remove(ly);
	}

	/**
	 * Devuelve la cantidad bloques de la malla
	 * 
	 * @return
	 */
	public int getTotalBlocksCount() {
		return (int) (this.getRadialNumblocks() * this.getAngularNumblocks() * this
				.getVerticalNumblocks());
	}

	/**
	 * 
	 * @return
	 */
	public int getBlocksPerLayer() {
		return (int) (this.getRadialNumblocks() * this.getAngularNumblocks());
	}

	/**
	 * Actualiza los espesores de las capas que NO pertenecen a una UG
	 */
	public void updateLayersThickness() {

		// Obtener espesor ocupado por las UG
		double ugThickness = 0;
		double remainThickness = 0; // espesor que sobra

		for (GeologicalUnit gu : this.getGuList()) {
			ugThickness += gu.getThickness();
		}
		remainThickness = this.getBd().getReservoir().getFormationThickness()
				- ugThickness;
		// obtener el ultimo layer de la ultima ug
		List<Layer> lastlys = this.guList.get(this.guList.size() - 1)
				.getLayers();
		if (lastlys.size() == 0) {
			return;
		}
		Layer last = lastlys.get(lastlys.size() - 1);

		double den = this.verticalNumblocks - last.getLayerNumber();
		if (den == 0) {
			return; // No hay capas sin UG
		}

		for (Layer ly : this.getLayerList()) {
			if (ly.getGu() == null) {
				ly.setThickness(remainThickness / den);
				ly.saveIt();
			}
		}
	}

	/**
	 * Devuelve los espesores de cada capa vertical de la malla
	 * 
	 * @return
	 */
	public double[] getLayersThickness() {
		double[] data = new double[(int) this.verticalNumblocks];
		int cnt = 0;
		this.updateLayersThickness(); // Actualizar espesores por si es
										// necesario
		for (Layer ly : this.getLayerList()) {
			data[cnt] = ly.getThickness();
			cnt++;
		}
		return data;
	}

	public List<Layer> getLayerList() {
		List<Layer> list = new ArrayList<Layer>(getAll(Layer.class));
		layerList = list;
		return layerList;
	}

	// TODO no usar
	@Deprecated
	public void setLayerList(List<Layer> layerList) {
		this.layerList = layerList;
	}

	public BaseData getBd() {
		bd = getRelatedModel(BaseData.class, bd, true);
		return bd;
	}

	public void setBd(BaseData bd) {
		this.bd = bd;
		setParent(bd); // TODO check
	}
	
	/**
	 * Devuelve las capas perforadas
	 * 
	 * @return
	 */
	public double[] getLayersPerforations() {
		double[] data = new double[(int) this.verticalNumblocks];
		int cnt = 0;
		int cnt_perf = 0;

		for (Layer ly : this.getLayerList()) {
			data[cnt] = ly.getPerforationConditions().get(0).getPerforatedInt();
			if(data[cnt] == 1){
                cnt_perf++;
            }
            cnt++;
		}

		double[] data_perf = new double[cnt_perf];
        int tmp=0;
        for(int i=0; i<cnt; i++) {
            if(data[i] == 1){
                data_perf[tmp] = i + 2; //el +2 es para igualar la indexacion del solver de simulacion cuyos bloques no fantasma empiezan en 2
                tmp++;
            }
        }
		return data_perf;
	}
	public void syncLayersAndUgs(){
		List<Layer> listLayers = this.getLayerList();
		List<GeologicalUnit> listUgs = this.getGuList();
		int cnt = 0;
		int n=0;
		for(GeologicalUnit ug: listUgs) {
			ug.clearLayers(); // limpiar layers viejos
			System.out.println("size" + ug.getLayers().size());
		}
		for(GeologicalUnit ug: listUgs) {
			n = ug.getNumLayers();
			System.out.println("N is "+ n);
			System.out.println("cnt is "+ cnt);
			
			for (int i=0; i< n; i++ ){
				ug.addLayer(listLayers.get(i + cnt));
				listLayers.get(i + cnt).saveIt();
				
			}
			cnt+=n;
			//actualizar thicknes de los layers
			ug.setLayersThickness();
		}
		
		for(int i=cnt; i< listLayers.size(); i++) {
			listLayers.get(i).set("geological_unit_id", null);
			listLayers.get(i).saveIt();
		}
		
	}
}
