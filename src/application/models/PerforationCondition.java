package application.models;

public class PerforationCondition extends LayerCondition {
	private double skin;
	private String perforated;
	private int layerNumber;
	private String guName;
	
	public PerforationCondition() {
		super();
		setPerforated("YES");
		setSkin(0.0);
	}
	public double getSkin() {
		skin = getDouble("skin");
		return skin;
	}
	public void setSkin(double skin) {
		this.skin = skin;
		set("skin", skin);
	}
	public String getPerforated() {
		perforated = getString("perforated");
		return perforated;
	}
	public int getPerforatedInt() {
		if (perforated.toLowerCase().equals("yes") || perforated.toLowerCase().equals("y")) {
			return 1;
		}
		return 0;
	}
	
	public void setPerforated(String perforated) {
	    set("perforated", perforated);
		this.perforated = perforated;
	}
	public int getLayerNumber() {
		return layerNumber;
	}

	public void setLayerNumber(int layerNum) {
		this.layerNumber = layerNum;
	}

	public String getGuName() {
		return guName;
	}

	public void setGuName(String guName) {
		this.guName = guName;
	}
}
