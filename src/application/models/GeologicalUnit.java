package application.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Unidad geologica o estrato definido en la malla del yacimiento (especificando
 * un espesor y numero de capas). A cada UG se le pueden asignar condiciones
 * iniciales y propiedades especificas (condiciones iniciales de presion,
 * saturacion, etc. y propiedades de la roca).
 * 
 * @author Jeyson Molina
 */
public class GeologicalUnit extends BaseModel {
	private String name;
	private double thickness; // Espesor TODO cambiar a double
	private Grid grid;
	private int numLayers; // Numero de capas en la vertical
	private List<Layer> layers = new ArrayList<Layer>();

	public List<Layer> getLayers() {
		List<Layer> list = getAllRelatedModels(layers,
				Layer.class);
		layers = list; // TODO list y guList siempre son iguales?
		return layers;
	}

	public void addLayer(Layer ly) {
		layers.add(ly);
		this.addRelatedModel(ly);
		ly.setGu(this);
	}
	
	public void removeLayer(Layer ly) {
		layers.remove(ly);
		//this.remove(ly);
		
		}
	public void clearLayers() {
		//TODO experimental
		int layersSize = this.getLayers().size();
		for(int i = 0; i < layersSize; i++) {
			this.getLayers().get(i).set("geological_unit_id", null);
			this.getLayers().get(i).saveIt();
		}
		this.layers.clear();
		System.out.println("clear" + this.layers.size());
	}
	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}

	public String getName() {
		name = getString("name");
		return name;
	}

	public void setName(String name) {
		this.name = name;
		set("name", name);
	}

	public double getThickness() {
		thickness = getDouble("thickness");
		return thickness;
	}

	public void setThickness(double thickness) {
		this.thickness = thickness;
		set("thickness", thickness);
		setLayersThickness();
	}

	public int getNumLayers() {
		numLayers = getInteger("num_layers");
		return numLayers;
	}

	public void setNumLayers(int numLayers) {
		int old_layers = this.numLayers;
		this.numLayers = numLayers;
		set("num_layers", numLayers);

		if (numLayers > 0 && !this.getGrid().getLayerList().isEmpty()) {
			// Sincronizar capas
			// TODO sincronizar related models
			
			Grid grid = this.getGrid();
			grid.syncLayersAndUgs();
			/*List<Layer> layerList = grid.getLayerList();
			int prevLayersCount = 0;
			for (int j = 0; j < grid.getGuList().indexOf(this); j++) {
				prevLayersCount += grid.getGuList().get(j).numLayers;
			}
			// Asignar capas correspondientes a numero de capas solicitado y
			// posicion de la UG
			this.layers.clear(); // TODO Esto es peligroso con el orm
			
			for (int i = 0; i < this.numLayers; i++) {
				this.addLayer(layerList.get(prevLayersCount + i));
			}
			setLayersThickness();*/
		}
	}

	/**
	 * Actualiza los espesores de las capas de esta UG deacuerdo a espesor de la
	 * UG
	 */
	public void setLayersThickness() {

		if (thickness > 0 && numLayers > 0) {
			double layerThickness = thickness / numLayers;
			System.out.println("LAyer thicknesss"+ layerThickness);
			for (Layer ly : this.layers) {
				ly.setThickness(layerThickness);
				ly.saveIt();
			}
			grid.updateLayersThickness(); // actualizar espesores de los otros
		}
	}

	public Grid getGrid() {
		grid = this.getRelatedModel(Grid.class, grid);
		return grid;
	}

	public void setGrid(Grid grid) {
		this.grid = grid;
	}

}
