package application.models;

import java.util.ArrayList;
import java.util.List;

import org.javalite.activejdbc.annotations.Table;

import application.utils.PropertiesRulesParser;

/**
 * Datos bases para un escenario. Contiene informacion de la formacion del
 * yacimiento, fluido, interaccion fluido roca, etc.
 * 
 * @author Jeyson Molina
 *
 */

@Table("base_datasets")
public class BaseData extends BaseModel {
	private Reservoir reservoir; // Propieadesd del yacimiento
	private Grid grid; // La malla del sistema
	private List<PvtValue> pvtList = new ArrayList<PvtValue>();

	private PvtDensity pvtDensities;
	private List<KrsGasValue> krsGasList = new ArrayList<KrsGasValue>();
	private List<KrsWaterValue> krsWaterList = new ArrayList<KrsWaterValue>();
	private List<OperationCondition> ocList = new ArrayList<OperationCondition>();
	private List<SkinCondition> skinList = new ArrayList<SkinCondition>();
	
	public Reservoir getReservoir() {
		reservoir = getRelatedModel(Reservoir.class, reservoir);
		return reservoir;
	}

	public void setReservoir(Reservoir reservoir) {
		this.reservoir = reservoir;
		addRelatedModel(reservoir);
	}

	public Grid getGrid() {
		grid = getRelatedModel(Grid.class, grid); // TODO check
		return grid;
	}

	public void setGrid(Grid grid) {
		this.grid = grid;
		addRelatedModel(grid); // TODO check, que pasa con old grid?
		grid.setBd(this);
	}

	/**
	 * Devuelve las unidades geologicas de la malla
	 * 
	 * @return Lista de layers
	 */
	public List<GeologicalUnit> getGUList() {
		return this.getGrid().getGuList();
	}

	/**
	 * Devuelve Los valores de condiciones iniciales para todos los bloques Cada
	 * condicion inicial es un vector de valores (numero flotante): ci[0] =
	 * presion inicial ci[1] = saturacion gas ci[2] = saturacion agua
	 * 
	 * @return Lista de c.i para cada bloque.
	 */
	public List<double[]> getBlocksInitialConditions() {
		List<double[]> data = new ArrayList();
		double[] rp = new double[grid.getTotalBlocksCount()];
		double[] gs = new double[grid.getTotalBlocksCount()];
		double[] ws = new double[grid.getTotalBlocksCount()];

		// LLenar data con ceros
		for (int i = 0; i < this.getGrid().getTotalBlocksCount(); i++) {
			rp[i] = 0;
			gs[i] = 0;
			ws[i] = 0;
		}
		// Parse rules

		for(Layer ly: this.getGrid().getLayerList()){
			for(InitialCondition ic: ly.getInitialConditions()) {
				List<Integer> indices =PropertiesRulesParser.parsePropertieRule(ic.getRule(), ly.getLayerNumber(), grid.getBlocksPerLayer());
				//Actualizar esos datos
				for(int j=0; j<indices.size(); j++){
					//TODO revisar
					rp[indices.get(j)] = ic.getInitialReservoirPressure();
					gs[indices.get(j)] = ic.getInitialGasSaturation();
					ws[indices.get(j)] = ic.getInitialWaterSaturation();
 				}
			}

		}
		data.add(rp);
		data.add(gs);
		data.add(ws);
		return data;
	}

	/**
	 * Devuelve Los valores de propiedades de la roca para todos los bloques
	 * Cada propiedad de la roca (p.r) es un vector de valores (numero
	 * flotante): pr[0] = porosidad pr[1] = permeabilidad i pr[2] =
	 * permeabilidad j pr[3] = permeabilidad k
	 * 
	 * @return Lista de p.r para cada bloque.
	 */
	public List<double[]> getBlocksFormationConditions() {
		List<double[]> data = new ArrayList();

		double[] por = new double[grid.getTotalBlocksCount()];
		double[] pi = new double[grid.getTotalBlocksCount()];
		double[] pj = new double[grid.getTotalBlocksCount()];
		double[] pk = new double[grid.getTotalBlocksCount()];

		// LLenar data con ceros
		for (int i = 0; i < this.getGrid().getTotalBlocksCount(); i++) {
			por[i] = 0;
			pi[i] = 0;
			pj[i] = 0;
			pk[i] = 0;
		}

	    
	 // Parse rules
	 		for(Layer ly: this.getGrid().getLayerList()){
	 			for(FormationCondition fc: ly.getFormationConditions()) {
	 				List<Integer> indices =PropertiesRulesParser.parsePropertieRule(fc.getRule(), ly.getLayerNumber(), grid.getBlocksPerLayer());
	 				//Actualizar esos datos
	 				for(int j=0; j<indices.size(); j++){
	 					//TODO revisar
	 					por[indices.get(j)] = fc.getPorosity();
	 					pi[indices.get(j)] = fc.getPermeabilityX();
	 					pj[indices.get(j)] = fc.getPermeabilityY();
	 					pk[indices.get(j)] = fc.getPermeabilityZ();
	  				}
	 			}
	 			
	 		}
	 	data.add(por);
	 	data.add(pi);
	 	data.add(pj);
	 	data.add(pk);
		return data;
	}

	/**
	 * Returns flattened PVT table
	 * @return
	 */
	public double[] getPvtValues() {
		List<PvtValue> list = this.getPvtList();
		int n = list.size();
		double [] data = new double[n * 9];
		int cnt = 0;
		
		for(PvtValue pval: this.getPvtList()){
			data[cnt] = pval.getPressure();
			data[cnt + n] = pval.getUg();
			data[cnt + 2*n] = pval.getUo();
			data[cnt + 3*n] = pval.getUw();
			data[cnt + 4*n] = pval.getBg();
			data[cnt + 5*n] = pval.getBo();
			data[cnt + 6*n] = pval.getBw();
			data[cnt + 7*n] = pval.getRs();
			data[cnt + 8*n] = pval.getRv();
			cnt = cnt + 1;
		}
		
		return data;
	}
	
	public double[] getKrsGasLiquidValues() {
		List<KrsGasValue> list = getKrsGasList();
		int n = list.size();
		double [] data = new double[n * 4];
		int cnt = 0;
		
		for(KrsGasValue kg: list) {
			data[cnt] = kg.getSg();
			data[cnt + n] = kg.getPcog();
			data[cnt + 2*n] = kg.getKrg();
			data[cnt + 3*n] = kg.getKro();
			cnt = cnt + 1;
		}
		
		return data;
	}
	
	public double[] getKrsWaterOilValues() {
		List<KrsWaterValue> list = getKrsWaterList();
		int n = list.size();
		double [] data = new double[n * 4];
		int cnt = 0;
		
		for(KrsWaterValue kg: list) {
			data[cnt] = kg.getSw();
			data[cnt + n] = kg.getPcwo();
			data[cnt + 2*n] = kg.getKrw();
			data[cnt + 3*n] = kg.getKro();
			cnt = cnt + 1;
		}
		return data;
	}
	
	public List<PvtValue> getPvtList() {
		List<PvtValue> list = this.getAllRelatedModels(pvtList, PvtValue.class);
		pvtList = list;
		return pvtList;
	}

	public void addPvtValue(PvtValue pval) {
		this.pvtList.add(pval);
		addRelatedModel(pval);
	}

	public List<KrsGasValue> getKrsGasList() {
		List<KrsGasValue> list = this.getAllRelatedModels(krsGasList,
				KrsGasValue.class);
		krsGasList = list;
		return krsGasList;
	}

	public void addKrsGasValue(KrsGasValue kgas) {
		this.krsGasList.add(kgas);
		addRelatedModel(kgas);
	}

	public List<KrsWaterValue> getKrsWaterList() {
		List<KrsWaterValue> list = this.getAllRelatedModels(krsWaterList,
				KrsWaterValue.class);
		krsWaterList = list;
		return krsWaterList;
	}

	public void addKrsWaterValue(KrsWaterValue kwater) {
		this.krsWaterList.add(kwater);
		addRelatedModel(kwater);
	}

	public PvtDensity getPvtDensities() {
		pvtDensities = getRelatedModel(PvtDensity.class, pvtDensities); // TODO
																		// check
		return pvtDensities;
	}

	public void setPvtDensities(PvtDensity pvtDensities) {
		this.pvtDensities = pvtDensities;
		addRelatedModel(pvtDensities);
	}

	public void setPvtList(List<PvtValue> pvtList) {
		this.pvtList = pvtList;
		for(PvtValue pvtValue : pvtList){
			add(pvtValue); //orm list
		}
	}

	public void setKrsGasList(List<KrsGasValue> krsGasList) {
		this.krsGasList = krsGasList;
		for(KrsGasValue krsGasValue : krsGasList){
			add(krsGasValue); //orm list
		}
	}

	public void setKrsWaterList(List<KrsWaterValue> krsWaterList) {
		this.krsWaterList = krsWaterList;
		for(KrsWaterValue krsWaterValue : krsWaterList){
			add(krsWaterValue); //orm list
		}
	}

	public List<OperationCondition> getOcList() {
		ocList = this.getAllRelatedModels(ocList, OperationCondition.class);
		
		return ocList;
	}
	
	public double[] getOperationConditionsValues() {
		List<OperationCondition> list =  this.getOcList();
		int n = list.size();
		int cnt = 0;
		double [] data = new double[n * 6];
		
		for(OperationCondition oc: list) {
			data[cnt] = oc.getTime(); 
			data[cnt + n] = oc.getQg();
			data[cnt + 2*n] = oc.getQo();
			data[cnt + 3*n] =  oc.getQw();
			data[cnt + 4*n] =  oc.getBhfp();
			data[cnt + 5*n] =  oc.getDbhfp();
			cnt = cnt + 1;
		}
		return data;
	}
    public double[] getSkinConditionsValues() {
		List<SkinCondition> list =  this.getSkinList();

		int n = list.size();
		int cnt = 0;
		double [] data = new double[n * 3];
		
		for(SkinCondition sk: list) {
			data[cnt] = sk.getTime(); 
			data[cnt + n] = sk.getSkin();
			data[cnt + 2*n] = sk.getSkinRadius();
			cnt = cnt + 1;
		}
		return data;
	}

	@Deprecated
	public void setOcList(List<OperationCondition> ocList) {
		this.ocList = ocList;
		//TODO orm here if used
	}
	
	public void addOperationCondition(OperationCondition oc) {
		addRelatedModel(oc);
		oc.saveIt();
		this.ocList.add(oc);
	}

	public List<SkinCondition> getSkinList() {
		skinList = this.getAllRelatedModels(skinList, SkinCondition.class);
		return skinList;
	}

	public void setSkinList(List<SkinCondition> skinList) {
		this.skinList = skinList;
	}

	public void addSkinCondition(SkinCondition skin) {
		addRelatedModel(skin);
		skin.saveIt();
		this.skinList.add(skin);
	}
}
