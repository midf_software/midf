package application.models;

import java.util.HashMap;
import java.util.Map;

import application.controllers.project.scenary.ScenaryWorkflowBase;
import application.controllers.project.scenary.asphaltenes.AsphaltenesWorkflow;
import application.controllers.project.scenary.basic.BasicSimulationWorkflow;
import application.controllers.project.scenary.preanalysis.ipr.IPRWorkflow;
import application.controllers.project.scenary.preanalysis.multiparametric.MultiparametricWorkflow;

public class ScenarioTypes {
	public static final String BASIC_SIMULATION = "Basic Simulation";
	public static final String MULTIPARAMETRIC = "Multiparametric";
	public static final String IPR = "IPR";
	public static final String ASPHALTENES_DIAGNOSTIC = "Asphaltenes Diagnostic";
	public static final String ASPHALTENES_REMEDIATION = "Asphaltenes Remediation";
	
	public static final Map <String, Class<? extends ModuleDataset>> classes;
	static {
		classes = new HashMap<String, Class<? extends ModuleDataset>> ();
		// Listado de WorkFlows.
		classes.put(ScenarioTypes.BASIC_SIMULATION, null);
		classes.put(ScenarioTypes.MULTIPARAMETRIC, Multiparametric.class);
		classes.put(ScenarioTypes.IPR, IPR.class);
		//TODO completar
		//classes.put(ScenarioTypes.ASPHALTENES_DIAGNOSTIC, Asphaltenes.class);
	}
}
