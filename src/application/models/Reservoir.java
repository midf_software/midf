package application.models;
import org.javalite.activejdbc.annotations.Table;
/**
 * Propieades del yacimiento.
 * TODO cambiar nombre para evitar ambiguedad con actor Yacimiento (Reservoir)
 * @author Jeyson Molina
 */
@Table("reservoir_properties")
public class Reservoir extends BaseModel{

    private double formationThickness;
    private double externalRadius; // Radio externo (dimensiones)
    private double wellboreRadius; // Radio interno del pozo (dimensiones)
    private double formationDepthTop; // Espesor ?
    private double radialPermeability; // deprecada
    private double verticalPermeability; // deperecada 
    private double porosity; // deprecada
    private double rockDensity; // densidad de la roca
    private int phases=1;
    
	public double getFormationThickness() {
		formationThickness = getDouble("formation_thickness");
		return formationThickness;
	}
	public void setFormationThickness(double formation_thickness) {
		this.formationThickness = formation_thickness;
		set("formation_thickness", formationThickness);
	}
	public double getExternalRadius() {
		externalRadius = getDouble("external_radius");
		return externalRadius;
	}
	public void setExternalRadius(double external_radius) {
		this.externalRadius = external_radius;
		set("external_radius", externalRadius);
	}
	public double getWellboreRadius() {
		wellboreRadius = getDouble("wellbore_radius");
		return wellboreRadius;
	}
	
	public void setWellboreRadius(double wellbore_radius) {
		this.wellboreRadius = wellbore_radius;
		set("wellbore_radius", wellboreRadius);
	}
	
	public double getFormationDepthTop() {
		this.formationDepthTop = getDouble("formation_depthtop");
		return formationDepthTop;
	}
	public void setFormationDepthTop(double formation_depth_top) {
		this.formationDepthTop = formation_depth_top;
		set("formation_depthtop", this.formationDepthTop);
	}
	@Deprecated
	public double getRadialPermeability() {
		return radialPermeability;
	}
	@Deprecated
	public void setRadialPermeability(double radial_permeability) {
		this.radialPermeability = radial_permeability;
	}
	@Deprecated
	public double getVerticalPermeability() {
		return verticalPermeability;
	}
	@Deprecated
	public void setVerticalPermeability(double vertical_permeability) {
		this.verticalPermeability = vertical_permeability;
	}
	@Deprecated
	public double getPorosity() {
		return porosity;
	}
	@Deprecated
	public void setPorosity(double porosity) {
		this.porosity = porosity;
	}
	
	
	public double getRockDensity() {
		rockDensity = getDouble("rock_density");
		return rockDensity;
	}
	public void setRockDensity(double rock_density) {
		this.rockDensity = rock_density;
		set("rock_density", rockDensity);
	}
	public int getPhases() {
		return phases;
	}
	public void setPhases(int phases) {
		this.phases = phases;
	}
}
