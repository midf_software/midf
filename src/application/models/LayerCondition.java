package application.models;

public abstract class LayerCondition extends BaseModel {
	private Layer layer;
	private String rule = "*";

	public String getGuName() {
		if (layer != null) {
			return layer.getGu().getName();
		}
		return "";
	}

	public GeologicalUnit getGU() {
		return layer.getGu();
	}

	public int getLayerNumber() {
		if (layer != null) {
			return layer.getLayerNumber();
		}
		return 0;
	}

	public String getRule() {
		rule = getString("rule");
		if(rule == "" || rule == null){
			setRule("*");
		}
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
		set("rule", rule);
	}


	public Layer getLayer() {
		layer = getRelatedModel(Layer.class, layer, true);
		return layer;
	}

	public void setLayer(Layer layer) {
		this.layer = layer;
		setParent(layer);// TODO check this
		saveIt();

	}
}
