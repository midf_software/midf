package application.models;
import org.javalite.activejdbc.annotations.Table;

@Table("krs_water_values")
public class KrsWaterValue extends BaseModel{
	private double sw;
	private double pcwo;
	private double krw;
	private double kro;
	
	public KrsWaterValue() {
		super();
	}
	
	public KrsWaterValue(double sw, double pcwo, double krw, double kro){
		super();
		this.setSw(sw);
		this.setPcwo(pcwo);
		this.setKrw(krw);
		this.setKro(kro);
	}
	
	public double getSw() {
		sw = getDouble("sw");
		return sw;
	}
	public void setSw(double sw) {
		this.sw = sw;
		set("sw", sw);
	}
	public double getPcwo() {
		pcwo = getDouble("pcwo");
		return pcwo;
	}
	public void setPcwo(double pcwo) {
		this.pcwo = pcwo;
		set("pcwo", pcwo);
	}
	public double getKrw() {
		krw = getDouble("krw");
		return krw;
	}
	public void setKrw(double krw) {
		this.krw = krw;
		set("krw", krw);
	}
	public double getKro() {
		kro = getDouble("kro");
		return kro;
	}
	public void setKro(double kro) {
		this.kro = kro;
		set("kro", kro);
	}
}
