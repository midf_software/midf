package application.models;



/**
 * Un Proyecto contiene varios escenarios. Un escenario es un caso particular de analisis que tiene unos datos
 * de entrada, un algoritmo de procesamiento y unos datos de salida.
 * @author Jeyson Molina
 */
public class Scenario extends BaseModel{

	//Informacion META para referencia de la instancia
	private String name;
    private String ref;
    private String type;
    private String date;
    private String description;
    
	private BaseData inputData; // Informacion de entrada base
	
	private Simulation simulation; // Instancia Simulacion de flujo 
	private ModuleDataset module; // Instancia referente a un modelo de un modulo
	
	
	/**
	 * Constructor
	 */
	public Scenario(){
		super();
	}
	
	/**
	 * Copy constructor
	 */
	public Scenario(Scenario anotherScenario){
		this.name = anotherScenario.name;
		this.ref = anotherScenario.ref;
		this.type = anotherScenario.type;
		this.date = anotherScenario.date;
		this.description = anotherScenario.description;
		
		// TODO: OJO problemas... crear los metodos de copy para cada uno
		this.inputData = anotherScenario.getInputData();
		this.simulation = anotherScenario.getSimulation();
	}
	
	public BaseData getInputData() {
		inputData = getRelatedModel(BaseData.class, inputData);
		return inputData;
	}

	public void setInputData(BaseData inputData) {
		this.inputData = inputData;
		add(inputData);
	}

	public Simulation getSimulation() {
		simulation = getRelatedModel(Simulation.class, simulation);
		return simulation;
	}

	public void setSimulation(Simulation simulation) {
		this.simulation = simulation;
		add(simulation);
	}	
	
	public String getName() {
		name = getString("name");
		return name;
	}

	public void setName(String name) {
		this.name = name;
		set("name", name);
	}

	public String getRef() {
		ref = getString("ref");
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
		set("ref", ref);
	}

	public String getType() {
		type = getString("type");
		return type;
	}

	public void setType(String type) {
		this.type = type;
		set("type", type);
	}

	public String getDate() {
		date = getString("date");
		return date;
	}

	public void setDate(String date) {
		this.date = date;
		set("date", date);
	}

	public String getDescription() {
		description = getString("description");
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		set("description", description);
	}

	public ModuleDataset getModule() {
		if(this.type.equals(ScenarioTypes.BASIC_SIMULATION))
			return null;
		module = this.getRelatedModel(ScenarioTypes.classes.get(this.type), module);
		return module;
	}

	public void setModule(ModuleDataset module) {
		if(this.type.equals(ScenarioTypes.BASIC_SIMULATION))
			return;
		
		Integer  pk = module.getPk();
		if(pk == null) {
			System.out.println("Guardando modulo antes de agregar a escenario...");
			module.saveIt();
		}
		//set("module_id", pk);
		add(module);
		this.module = module;
	}


}