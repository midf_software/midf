package application.models;

import org.javalite.activejdbc.annotations.Table;

/**
 * Condiciones de la roca
 * 
 * @author Jeyson Molina
 *
 */
public class FormationCondition extends LayerCondition{
	private double porosity;
	private double permeabilityX;
	private double permeabilityZ;
	private double permeabilityY;
	
	private int layerNumber;
	private String guName;

	
	public FormationCondition() {
		super();
		// default values
		setPorosity(0);
		setPermeabilityX(0);
		setPermeabilityZ(0);
		setPermeabilityY(0);
	}

	public double getPorosity() {
		porosity = getDouble("porosity");
		return porosity;
	}

	public void setPorosity(double porosity) {
		this.porosity = porosity;
        set("porosity", porosity);
	}

	public double getPermeabilityX() {
		permeabilityX = getDouble("permeability_x");
		return permeabilityX;
	}

	public void setPermeabilityX(double permeabilityX) {
		this.permeabilityX = permeabilityX;
		set("permeability_x", permeabilityX);
	}

	public double getPermeabilityZ() {
		permeabilityZ = getDouble("permeability_z");
		return permeabilityZ;
	}

	public void setPermeabilityZ(double permeabilityZ) {
		this.permeabilityZ = permeabilityZ;
		set("permeability_z", permeabilityZ);
	}

	public double getPermeabilityY() {
		permeabilityY = getDouble("permeability_y");
		return permeabilityY;
	}

	public void setPermeabilityY(double permeabilityY) {
		this.permeabilityY = permeabilityY;
		set("permeability_y", permeabilityY);
	}

	public int getLayerNumber() {
		return layerNumber;
	}

	public void setLayerNumber(int layerNum) {
		this.layerNumber = layerNum;
	}

	public String getGuName() {
		return guName;
	}

	public void setGuName(String guName) {
		this.guName = guName;
	}
}
