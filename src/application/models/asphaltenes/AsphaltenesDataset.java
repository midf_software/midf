package application.models.asphaltenes;

public class AsphaltenesDataset {
	//Fraccion pesada
	private double pesoMolecular;
	private double gravedadsEspecifica;
	private double gravedadApi;
	
	
	//Fraccion Sara
	private double porcentSaturados; 
	private double porcentAsfaltenos;
	private double porcentAromaticos;
	private double porcentResinas;
	
	// Propiedades Asfalteno
	
	private double denAsfaltenos; 
	private double vsa; 
	private double presionOnset;
	private double correccionDouble;
	
	// Opciones APS
	private int apsTipo;
	private int calcPresionOnset;
	private double temperatura;
	
	
	public double getPesoMolecular() {
		return pesoMolecular;
	}
	public void setPesoMolecular(double pesoMolecular) {
		this.pesoMolecular = pesoMolecular;
	}
	public double getGravedadsEspecifica() {
		return gravedadsEspecifica;
	}
	public void setGravedadsEspecifica(double gravedadsEspecifica) {
		this.gravedadsEspecifica = gravedadsEspecifica;
	}
	public double getGravedadApi() {
		return gravedadApi;
	}
	public void setGravedadApi(double gravedadApi) {
		this.gravedadApi = gravedadApi;
	}
	public double getPorcentSaturados() {
		return porcentSaturados;
	}
	public void setPorcentSaturados(double porcentSaturados) {
		this.porcentSaturados = porcentSaturados;
	}
	public double getPorcentAsfaltenos() {
		return porcentAsfaltenos;
	}
	public void setPorcentAsfaltenos(double porcentAsfaltenos) {
		this.porcentAsfaltenos = porcentAsfaltenos;
	}
	public double getPorcentAromaticos() {
		return porcentAromaticos;
	}
	public void setPorcentAromaticos(double porcentAromaticos) {
		this.porcentAromaticos = porcentAromaticos;
	}
	public double getPorcentResinas() {
		return porcentResinas;
	}
	public void setPorcentResinas(double porcentResinas) {
		this.porcentResinas = porcentResinas;
	}
	public double getDenAsfaltenos() {
		return denAsfaltenos;
	}
	public void setDenAsfaltenos(double denAsfaltenos) {
		this.denAsfaltenos = denAsfaltenos;
	}
	public double getVsa() {
		return vsa;
	}
	public void setVsa(double vsa) {
		this.vsa = vsa;
	}
	public double getPresionOnset() {
		return presionOnset;
	}
	public void setPresionOnset(double presionOnset) {
		this.presionOnset = presionOnset;
	}
	public double getCorreccionDouble() {
		return correccionDouble;
	}
	public void setCorreccionDouble(double correccionDouble) {
		this.correccionDouble = correccionDouble;
	}
	public int getApsTipo() {
		return apsTipo;
	}
	public void setApsTipo(int apsTipo) {
		this.apsTipo = apsTipo;
	}
	public int getCalcPresionOnset() {
		return calcPresionOnset;
	}
	public void setCalcPresionOnset(int calcPresionOnset) {
		this.calcPresionOnset = calcPresionOnset;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	
}
