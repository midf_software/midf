package application.utils;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.javalite.activejdbc.associations.NotAssociatedException;

import application.models.BaseData;
import application.models.BaseModel;
import application.models.GeologicalUnit;
import application.models.Grid;
import application.models.Layer;

/**
 * Support class used support object cloning using recursively using reflection
 * 
 * @author CV
 *
 */
public class CloneHelper {

	/**
	 * Clones the object originalObject, and returns the identical copy.
	 * 
	 * @param originalObject
	 * @param parent
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> T clone(T originalObject, BaseModel parent)
			throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {
		return CloneHelper.clone(originalObject, parent, null);
	}

	/**
	 * Clones the object originalObject, and returns the identical copy. Checks
	 * already cloned objects to avoid nested relation loops
	 * 
	 * @param originalObject
	 * @param parent
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> T clone(T originalObject, BaseModel parent,
			HashMap<Object, Object> alreadyCloned)
			throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {

		if (alreadyCloned == null) {
			alreadyCloned = new HashMap<>();
		}

		// If the object has already been cloned, return the cloned instance
		if (alreadyCloned.containsKey(originalObject)) {
			return (T) alreadyCloned.get(originalObject);
		}

		if (originalObject != null) {

			Class<T> clazz = (Class<T>) originalObject.getClass();

			T clonedObject = clazz.newInstance();

			// Add the clonedObject to alreadyCloned list, to avoid object
			// nested relation loops
			alreadyCloned.put(originalObject, clonedObject);

			// TODO: Relaci�n bilateral a nivel de objetos (grid/baseData,
			// layers/Gus)
			if (parent != null) {
				if (parent instanceof GeologicalUnit
						&& clonedObject instanceof Layer) {
					((GeologicalUnit) parent).getGrid().add(
							(Layer) clonedObject);
					((Layer) clonedObject).setGu((GeologicalUnit) parent);
					// XXX: avoid cloning Gu again
					((Layer) originalObject).setGu((GeologicalUnit) CloneHelper
							.getKeyByValue(alreadyCloned, parent));
				}
				try {
					parent.add((BaseModel) clonedObject);
				} catch (NotAssociatedException e) {
					// No asociation between parent and cloned object at Model
					// level....
					System.out
							.println("Safe: no asociation between parent and cloned object");
				}
			}

			Field[] fields = clazz.getDeclaredFields();

			List<Class<?>> appModels = getModelsList();

			// copy all fields
			for (int i = 0; i < fields.length; i++) {
				fields[i].setAccessible(true);

				// Use default getters and setters to clone
				String getterMethod = "get"
						+ fields[i].getName().substring(0, 1).toUpperCase()
						+ fields[i].getName().substring(1);
				String setterMethod = "set"
						+ fields[i].getName().substring(0, 1).toUpperCase()
						+ fields[i].getName().substring(1);
				Method getter = clazz.getMethod(getterMethod);
				Class returnType = getter.getReturnType();
				Method setter = clazz.getMethod(setterMethod, returnType);

				// Do not iterate lyers again for Grid/Gu
				if (clonedObject instanceof Grid
						&& (fields[i].getName().equals("layerList") || fields[i]
								.getName().equals("bd"))) {
					continue;
				}

				// If field is a list
				if (fields[i].getType().equals(List.class)) {

					// Use the default getter
					List<?> list = (List<?>) getter.invoke(originalObject);

					List clonedList = new ArrayList<>();

					((BaseModel) clonedObject).saveIt();

					for (int j = 0; j < list.size(); j++) {
						if (appModels.contains(list.get(j).getClass())) {
							// If the element is a model clone it, and add to
							// list
							BaseModel clonedElement = (BaseModel) CloneHelper
									.clone(list.get(j),
											(BaseModel) clonedObject,
											alreadyCloned);
							clonedList.add(clonedElement);
						} else {
							// If the element its a primitive set It.
							clonedList.add(list.get(j));
						}
					}

					setter.invoke(clonedObject, clonedList);

				} else if (appModels.contains(fields[i].getType())) {
					// If the field its an object clone It
					((BaseModel) clonedObject).saveIt();

					setter.invoke(clonedObject, CloneHelper.clone(
							getter.invoke(originalObject),
							(BaseModel) clonedObject, alreadyCloned));
				} else {
					// If the field its a primitive set It.
					setter.invoke(clonedObject, getter.invoke(originalObject));
				}

			}

			// If cloning a Grid... Keep GUs/Layers consistency
			if (clonedObject instanceof Grid
					&& ((Grid) clonedObject).getGuList() != null
					&& !((Grid) clonedObject).getGuList().isEmpty()
					&& ((Grid) clonedObject).getLayerList() != null
					&& !((Grid) clonedObject).getLayerList().isEmpty()) {
				((Grid) clonedObject).setBd((BaseData) parent);
				for (GeologicalUnit gu : ((Grid) clonedObject).getGuList()) {
					gu.setGrid((Grid) clonedObject);
					// trigger the layers update
					gu.setNumLayers(gu.getNumLayers());
				}
			}

			return clonedObject;
		}
		return null;
	}

	/**
	 * Private helper method to find the models in project
	 * 
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> getModelsList() {

		String packageName = "application\\models\\";
		String packagePath = System.getProperty("user.dir") + "\\src\\"
				+ packageName;
		File packageDirectory = new File(packagePath);

		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();

		if (packageDirectory.exists() && packageDirectory.isDirectory()) {
			final String[] files = packageDirectory.list();

			for (final String file : files) {
				if (file.endsWith(".java")) {
					try {
						classes.add(Class.forName(packageName
								.replace("\\", ".")
								+ file.substring(0, file.length() - 5)));
					} catch (final NoClassDefFoundError e) {
						// do nothing. this class hasn't been found by the
						// loader, and we don't care.
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return classes;
	}

	public static <T, E> T getKeyByValue(HashMap<T, E> map, E value) {
		for (Entry<T, E> entry : map.entrySet()) {
			if (value.equals(entry.getValue())) {
				return entry.getKey();
			}
		}
		return null;
	}
}
