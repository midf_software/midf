package application.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper para parsear los datos ingresados por el usuario en formularios donde
 * se definen propieades para los bloques de las Unidades geologicas.
 * El usuario selecciona una capa de la UG y para esta especifica el valor de la propieadad para uno
 * o mas bloques de la capa. Para abreviar se definen la siguiente notacion:
 * - Todos ( * ): Simboliza todos los bloques de una capa (layer)
 * - Bloque ([#bloque]): Simboliza un bloque individual de la capa especificado por su indice. 
 * - Rango ([Bi]:[Bf]): Simboliza un rango de bloques (ej: 1:10) . Bi <= Bf
 * - Coma ( , ) : Separador que separa las especificaciones de los bloques (ej: 1, 3, 1:10) 
 * @author jeyson
 */

public class PropertiesRulesParser {
	final static String SEPARATOR = ",";
	final static String RANGE = ":";
	final static String ALL = "*"; 
	
	/**
	 * Devuelve listado de indices de bloques a modificar segun los bloques especificados en propertieRule
	 * @param propertieRule
	 * @param layerNum
	 * @param blocksPerLayer
	 * @return
	 */
	public static List<Integer> parsePropertieRule(String propertieRule, int layerNum, int blocksPerLayer) {
		List<Integer> indices = new ArrayList<Integer>();
		int bi, bf;
		int prevBlocks = blocksPerLayer * (layerNum -1);
				
		String cur_rule;
		String[] rules = propertieRule.trim().split(SEPARATOR);
		for(int ind=0; ind < rules.length; ind++){
			cur_rule = rules[ind].trim();
			try {
				if (cur_rule.equals(ALL)) {
					bi = prevBlocks + 1;
					bf = prevBlocks + blocksPerLayer;
				}
				else if(cur_rule.indexOf(RANGE) > -1){
					int indof = cur_rule.indexOf(RANGE);
					bi = prevBlocks + Integer.parseInt(cur_rule.substring(0, indof-1));
					bf = prevBlocks + Integer.parseInt(cur_rule.substring(indof+1));
				}
				else {
					//Caso numero
					bi = Integer.parseInt(cur_rule);
					bf = bi;
				}
				//Agregar bloques
				for(int z=bi ; z<=bf; z++){
					indices.add(z-1);//-1 for array offset
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return indices;
	}
}
