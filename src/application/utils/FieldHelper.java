package application.utils;

public class FieldHelper {
	
	public static double getDouble(String text) {
		return Double.parseDouble(text.equals("") ? "0" : text);
	}
	
	public static int getInteger(String text) {
		return Integer.parseInt(text.equals("") ? "0" : text);
	}
	
	public static String getText(int value){
		return String.valueOf(value);
	}
	
	public static String getText(double value){
		return String.valueOf(value);
	}
}
