package application.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Ayuda a parsear archivos planos que contienen informacion tabulada en formato CSV
 * @author jeyson
 *
 */
public class FileTableParser {
	
/**
 * Devuelve una lista de los valores  leidos del archivo plano de informacion tabulada.
 * @param filename : Ruta absoluta del archivo
 * @return List de vectores double[]
 */
	public static List<double[]> parseFile(String filename){
		List <double[]> data = new ArrayList<double[]>();
		
		 try {
			CSVReader reader = new CSVReader(new FileReader(filename),'\t','"', 1); // Skip first line
			    String [] nextLine;
			    while ((nextLine = reader.readNext()) != null) {
			        // nextLine[] is an array of values from the line
			       // System.out.println(nextLine[0] + nextLine[1] + "etc...");
			    	double [] row = new double[nextLine.length];
			    	for(int j=0; j<nextLine.length; j++){
			    		// System.out.println("data " + nextLine[j]);
			    		double val = FieldHelper.getDouble(nextLine[j]);
			    		row[j] = val;
			    	}
			    	data.add(row);
			    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 finally {
			
		 }
		 return data;
	}
}
