package application.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 * Component used to relate a table with an object.
 * 
 * @author CV
 */
public class TableHelper<T> {

	private TableView<T> tableView;

	private ObservableList<TableColumn<T, ?>> columnsList;

	private List<String> attributeNames;

	public TableHelper(TableView<T> tableView,
			ObservableList<TableColumn<T, ?>> columnsList,
			List<String> attributeNames, List<Class> attributeTypes) {

		this.tableView = tableView;
		this.columnsList = columnsList;
		this.attributeNames = attributeNames;

		tableView.setEditable(true);

		for (int i = 0; i < columnsList.size(); i++) {

			final int index = i;

			TableColumn column = columnsList.get(i);

			Callback<TableColumn, TableCell> cellFactory;

			if (attributeTypes.get(i).equals(Integer.class) || attributeTypes.get(i).equals(int.class)) {
				column.setCellValueFactory(new PropertyValueFactory<T, Integer>(
						attributeNames.get(i)));
				cellFactory = new Callback<TableColumn, TableCell>() {
					public TableCell call(TableColumn p) {
						return new EditingCell<Integer>(Integer.class);
					}
				};
			} else if (attributeTypes.get(i).equals(String.class)) {
				column.setCellValueFactory(new PropertyValueFactory<T, String>(
						attributeNames.get(i)));
				cellFactory = new Callback<TableColumn, TableCell>() {
					public TableCell call(TableColumn p) {
						return new EditingCell<String>(String.class);
					}
				};
			} else {
				column.setCellValueFactory(new PropertyValueFactory<T, Double>(
						attributeNames.get(i)));
				cellFactory = new Callback<TableColumn, TableCell>() {
					public TableCell call(TableColumn p) {
						return new EditingCell<Double>(Double.class);
					}
				};
			}

			column.setCellFactory(cellFactory);
			column.setOnEditCommit(new EventHandler<CellEditEvent<T, ?>>() {
				@Override
				public void handle(CellEditEvent<T, ?> t) {

					Object setValue = (t.getNewValue() == null) ? t
							.getOldValue() : t.getNewValue();

					T object = (T) t.getTableView().getItems()
							.get(t.getTablePosition().getRow());

					String methodName = "set"
							+ attributeNames.get(index).substring(0, 1)
									.toUpperCase()
							+ attributeNames.get(index).substring(1);

					try {
						Method method = object.getClass().getMethod(methodName,
								attributeTypes.get(index));
						method.invoke(object, setValue);
					} catch (NoSuchMethodException | SecurityException
							| IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						// TODO: mostrar un cochino mensaje
						e.printStackTrace();
					}

				}
			});
		}

		tableView.getColumns().setAll(columnsList);
	}

	public TableView<T> getTableView() {
		return tableView;
	}

	public void setTableView(TableView<T> tableView) {
		this.tableView = tableView;
	}

	public ObservableList<TableColumn<T, ?>> getColumnsList() {
		return columnsList;
	}

	public void setColumnsList(ObservableList<TableColumn<T, ?>> columnsList) {
		this.columnsList = columnsList;
	}

	public List<String> getAttributeNames() {
		return attributeNames;
	}

	public void setAttributeNames(List<String> attributeNames) {
		this.attributeNames = attributeNames;
	}

public class EditingCell<B> extends TableCell<T, B> {

		private TextField textField;
		
		private Class<?> classType;

		public EditingCell() {
			
		}

		public EditingCell(Class<?> classType) {
			this.classType = classType;
		}

		@Override
		public void startEdit() {
			if (!isEmpty()) {
				super.startEdit();
				createTextField();
				setText(null);
				setGraphic(textField);
				textField.selectAll();
			}
		}

		@Override
		public void cancelEdit() {
			super.cancelEdit();

			setText(((B) getItem()).toString());
			setGraphic(null);
		}

		@Override
		public void updateItem(B item, boolean empty) {
			super.updateItem(item, empty);

			if (empty) {
				setText(null);
				setGraphic(null);
			} else {
				if (isEditing()) {
					if (textField != null) {
						textField.setText(getString());
					}
					setText(null);
					setGraphic(textField);
				} else {
					setText(getString());
					setGraphic(null);
				}
			}
		}

		private void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.focusedProperty().addListener(
					new ChangeListener<Boolean>() {
						@Override
						public void changed(
								ObservableValue<? extends Boolean> arg0,
								Boolean arg1, Boolean arg2) {
							
							//Este try hace de validador básico para las tablas, especialmente para campos numericos
							try {
								if (!arg2) {
									if(classType.equals(Integer.class)){
										commitEdit((B) Integer.valueOf(textField.getText()));
									} else if(classType.equals(String.class)) {
										commitEdit((B) textField.getText());
									} else if(classType.equals(Double.class)) {
										commitEdit((B) Double.valueOf(textField.getText()));
									}
									
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								//e.printStackTrace();
								//TODO mostrar modal?
							}
							
							
							
						}
					});
		}

		private String getString() {
			return getItem() == null ? "" : getItem().toString();
		}
		
		public TextField getTextField() {
			return this.textField;
		}
		
	}
}
